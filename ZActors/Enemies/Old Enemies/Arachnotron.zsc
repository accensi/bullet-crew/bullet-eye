class ArachnotronMM : BEEnemy
{
	Default
	{
		Health 500;
		BEEnemy.ScoreValue 350;
		Radius 35;
		Height 64;
		Mass 600;
		Speed 12;
		PainChance 128;
		+FLOORCLIP
		SeeSound "baby/sight";
		PainSound "baby/pain";
		DeathSound "baby/death";
		ActiveSound "baby/active";
		Obituary "$OB_BABY";
		Tag "$FN_ARACH";
		Species "BEMonster";

		StencilColor "Red";

		BloodType "BEPatentedFakeBlood";
		
		DamageFactor "ArachnotronDamage", 15.0;
		
		DropItem "EnemyDropHighPool", 40;
		DropItem "StrikeOrbPool", 10;
	}

	States
	{
		Spawn:
			BSPI AB 10 A_Look;
			Loop;
		See:
			BSPI A 20;
			BSPI A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			BSPI A 0 A_JumpIfInventory("MovementSpeedSlowEffect", 1, "SeeSlow");
			BSPI A 0 A_JumpIfInventory("LobolashiHowlBuff", 1, "SeeHowled");			
			BSPI A 3 A_BabyMetal;
			BSPI ABBCC 3 A_Chase;
			BSPI D 3 A_BabyMetal;
			BSPI DEEFF 3 A_Chase;
			Goto See + 1;
		SeeSlow:
			BSPI A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			BSPI A 6 A_BabyMetal;
			BSPI ABBCCD 6 A_Chase;
			BSPI D 6 A_BabyMetal;
			BSPI EEFF 6 A_Chase;
			Loop;
		SeeHowled:
			BSPI A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			BSPI A 0 A_JumpIfInventory("MovementSpeedSlowEffect", 1, "SeeSlow");
			BSPI A 1 A_BabyMetal;
			BSPI ABBCCD 1 A_Chase;
			BSPI D 1 A_BabyMetal;
			BSPI EEFF 1 A_Chase;
			Loop;
		SeeFear:
			BSPI A 0 {bFRIGHTENED = true;}
			BSPI A 0 A_GiveInventory("MetalMonstrosityFearDebuff", 1);
			BSPI A 0 A_TakeInventory("MetalMonstrosityFearCounterA", 2);
			BSPI ABBCCD 1 A_Wander;
			BSPI D 1 A_BabyMetal;
			BSPI EEFF 1 A_Wander;
			BSPI A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, 4);
			BSPI A 0 {bFRIGHTENED = false;}
			BSPI A 0 A_TakeInventory("MetalMonstrosityFearDebuff", 1);
			BSPI A 0 A_TakeInventory("MetalMonstrosityDebuff", 1);
			Goto See;
			BSPI A 0;
			Goto SeeFear + 2;
		Missile:
			BSPI A 20 Bright A_FaceTarget;
			BSPI G 4 Bright A_SpawnProjectile("ArachnotronPlasmaMM", 35, 0, 0);
			BSPI H 4 Bright;
			BSPI H 1 Bright A_SpidRefire;
			Goto Missile + 1;
		Stunned:
			BSPI I 30;
			Goto See;
		ShockwaveStunned:
			BSPI I 0;
			Goto Super::ShockwaveStunned;
		Pain:
			BSPI I 3;
			BSPI A 0 SecondaryPainSound();
			BSPI I 3 A_Pain;
			BSPI A 0
			{
				let plr = BEPlayer(Target);
				if (plr && plr.CheckInventory("ShouldersRetaliationPauldrons", 0) && bHASRESURRECTED)
				{
					A_DamageSelf(100);
				}
			}
			BSPI A 0 A_TakeInventory("LobolashiHowlBuff", 1);
			Goto See + 1;
		Death:
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI J 8;
			BSPI A 0 A_NoBlocking;
			BSPI AA 0 A_DropItem("ScorePoolMedium", 1, 30);
			BSPI A 0 A_DropAmmoForWeapons((level.total_monsters >= 600 ? 15 : 5) + BECore.GetActivePlayerCount() * 2);
			BSPI A 0 A_DropItem("AmmoLockboxSmall", 1, 6);
			BSPI A 0 A_DropItem("AmmoLockboxLarge", 1, 3);
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI AAAAA 0 SpawnDeathExplosion(Exp_Extra);
			BSPI A 0 SpawnDeathExplosion(Exp_Big);
			BSPI K 8 A_Scream;
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI A 0 SpawnDeathExplosion(Exp_Big);
			BSPI L 6;
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI A 0 SpawnDeathExplosion(Exp_Big);
			BSPI M 1;
			TNT1 A 2;
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI A 0 SpawnDeathExplosion(Exp_Big);
			BSPI M 2;
			TNT1 A 2;
			BSPI AA 0 SpawnDeathExplosion(Exp_Small);
			BSPI A 0 SpawnDeathExplosion(Exp_Big);
			BSPI M 2;
			TNT1 A 1 A_BossDeath;
			Stop;
		FreezeDeathDrop:
			BSPI A 0 A_DropItem("EnemyDropHighPool", 1, 40);
			BSPI A 0 A_DropItem("StrikeOrbPool", 1, 10);
			BSPI A 0 A_DropItem("ScorePoolMedium", 1, 15);
			BSPI A 0 A_DropItem("ScorePoolLarge", 1, 15);
			BSPI A 0 A_DropAmmoForWeapons((level.total_monsters >= 600 ? 15 : 5) + BECore.GetActivePlayerCount() * 2);
			BSPI A 0 A_DropItem("AmmoLockboxSmall", 1, 6);
			BSPI A 0 A_DropItem("AmmoLockboxLarge", 1, 3);
			TNT1 A 1;
			Stop;
		Raise:
			BSPI A 0
			{
				bNOGRAVITY = false;
				bTHRUACTORS = false;
				bDONTSPLASH = false;
			}
			BSPI A 0 A_SpawnItemEx("ArchvileRessurectionFX", flags: SXF_NOCHECKPOSITION);
			BSPI A 0 {bHASRESURRECTED = true;}
			BSPI A 45;
			Goto See;
	}
}

class ArachnotronPlasmaMM : ArachnotronPlasma
{
	mixin BEEnemyProjectile;

	Default
	{
		DamageFunction (4 + (BEGlobalStats.Get().LevelsCompleted / 5));
		DamageType "ArachnotronDamage";
	}
}
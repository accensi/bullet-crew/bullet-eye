class ZombiemanMM : BEEnemy
{
	Default
	{
		Health 20;
		Radius 20;
		Height 56;
		Speed 8;
		PainChance 200;
		Monster;
		+FLOORCLIP
		+DOHARMSPECIES
		SeeSound "grunt/sight";
		AttackSound "grunt/attack";
		PainSound "grunt/pain";
		DeathSound "grunt/death";
		ActiveSound "grunt/active";
		Obituary "$OB_ZOMBIE";
		Tag "$FN_ZOMBIE";
		Species "BEMonster";

		StencilColor "Red";

		BloodType "BEPatentedFakeBlood";

		DropItem "EnemyDropLowPool", 45;
		DropItem "StrikeOrbPool", 5;
	}

	States
	{
		Spawn:
			POSS A 0 NoDelay
			{
				if ((GetCvar("bulleteye_oldenemyhealthboost") == 1))
				{
					A_SetHealth(Health + 30);
				}
				else
				{
					return;
				}
			}
		SpawnFall:
			POSS AB 10 A_Look;
			Loop;
		See:
			POSS A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			POSS A 0 A_JumpIfInventory("MovementSpeedSlowEffect", 1, "SeeSlow");
			POSS A 0 A_JumpIfInventory("LobolashiHowlBuff", 1, "SeeHowled");
			POSS AABBCCDD 4 A_Chase;
			Loop;
		SeeSlow:
			POSS A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			POSS AABBCCDD 8 A_Chase;
			Loop;
		SeeHowled:
			POSS A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, "SeeFear");
			POSS A 0 A_JumpIfInventory("MovementSpeedSlowEffect", 1, "SeeSlow");
			POSS AABBCCDD 2 A_Chase;
			Loop;
		SeeFear:
			POSS A 0 {bFRIGHTENED = true;}
			POSS A 0 A_GiveInventory("MetalMonstrosityFearDebuff", 1);
			POSS A 0 A_TakeInventory("MetalMonstrosityFearCounterA", 2);
			POSS AABBCCDD 2 A_Wander;
			POSS A 0 A_JumpIfInventory("MetalMonstrosityFearCounterA", 1, 4);
			POSS A 0 {bFRIGHTENED = false;}
			POSS A 0 A_TakeInventory("MetalMonstrosityFearDebuff", 1);
			POSS A 0 A_TakeInventory("MetalMonstrosityDebuff", 1);
			Goto See;
			POSS A 0;
			Goto SeeFear + 2;
		Missile:
			POSS E 10 A_FaceTarget;
			POSS F 8 A_CustomBulletAttack (22.5, 0, 1, 2, "ZombiemanPuff", 0, CBAF_NORANDOM);
			POSS E 8;
			Goto See;
		Stunned:
			POSS G 30;
			Goto See;
		ShockwaveStunned:
			POSS G 0;
			Goto Super::ShockwaveStunned;
		Pain:
			POSS G 3 A_Pain;
			POSS A 0 SecondaryPainSound();
			POSS G 3 Bright;
			POSS A 0
			{
				let plr = BEPlayer(Target);
				if (plr && plr.CheckInventory("ShouldersRetaliationPauldrons", 0) && bHASRESURRECTED)
				{
					A_DamageSelf(100);
				}
			}
			POSS A 0 A_TakeInventory("LobolashiHowlBuff", 1);
			Goto See;
		Death:
			POSS A 0 {bHASRESURRECTED = false;}
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS H 8;
			POSS A 0 A_NoBlocking;
			POSS AA 0 A_DropItem("ScorePoolSmallEnemy", 1, 15);
			POSS A 0 A_DropAmmoForWeapons((level.total_monsters >= 600 ? 8 : 2) + BECore.GetActivePlayerCount() * 2);
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS AAAAA 0 SpawnDeathExplosion(Exp_Extra);
			POSS I 8 A_Scream;
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS J 6;
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS J 1;
			TNT1 A 2;
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS J 2;
			TNT1 A 2;
			POSS AA 0 SpawnDeathExplosion(Exp_Small);
			POSS K 2;
			POSS A 0
			{
				bNOGRAVITY = false;
				bDONTSPLASH = true;
			}
			TNT1 A 0 A_CheckProximity("CleanUp", "ArchvileMM", 26000, 0, CPXF_ANCESTOR | CPXF_LESSOREQUAL);
			TNT1 A -1;
			Stop;
		FreezeDeathDrop:
			POSS A 0 A_DropItem("EnemyDropLowPool", 1, 45);
			POSS A 0 A_DropItem("StrikeOrbPool", 1, 5);
			POSS AA 0 A_DropItem("ScorePoolSmallEnemy", 1, 15);
			POSS A 0 A_DropAmmoForWeapons((level.total_monsters >= 600 ? 8 : 2) + BECore.GetActivePlayerCount() * 2);
			TNT1 A 1;
			Stop;
		Raise:
			POSS A 0
			{
				bNOGRAVITY = false;
				bTHRUACTORS = false;
				bDONTSPLASH = false;
			}
			POSS A 0 A_SpawnItemEx("ArchvileRessurectionFX", flags: SXF_NOCHECKPOSITION);
			POSS A 0 {bHASRESURRECTED = true;}
			POSS A 45;
			Goto See;
	}
}

class ZombiemanPuff : Actor //Invisible puff for enemy melee
{
	Default
	{
		+NOBLOCKMAP
		+NOGRAVITY
		+ALLOWPARTICLES
		+RANDOMIZE
		+ZDOOMTRANS
		RenderStyle "Translucent";
		Alpha 0.5;
		VSpeed 1;
		Mass 5;
		DamageType "ZombiemanDamage";
	}

	States
	{
		Spawn:
			PUFF A 4 Bright;
			PUFF B 4;
		Melee:
			PUFF CD 4;
			Stop;
	}
}
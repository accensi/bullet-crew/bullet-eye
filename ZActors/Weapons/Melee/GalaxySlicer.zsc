class GalaxySlicerWeapon : BEWeapon
{
	override void DrawHUDStuff(BulletEyeHUD hud, BEPlayer plr, Vector2 pos, int flags)
	{
		hud.DrawImage("WPNGAUGE", pos, flags);
		hud.DrawBar("GLXSLCBR", "ASVLCNBE", Energy, MaxEnergy, hud.AddOffset(flags, pos, (6, 1)), 0, 0, flags);
	}

	private action void A_TakeEnergy(int amount)
	{
		invoker.Energy = max(invoker.Energy - Amount, 0);
	}
	
	action void A_GiveEnergy(int amount)
	{
		invoker.Energy = max(invoker.Energy + Amount, 0);
	}
	
	override void DoEffect()
	{
		AllHoming = (A_GetWeaponLevel() > 2 && ++HomingTicker >= 35 * 6);
		if (Energy < MaxEnergy && level.time % 5 == 0) // && Owner.CountInv("GalaxySlicerAntiSpam") < 10)
		{
			Energy = min(Energy + 1 + Owner.CountInv("ShouldersAssaultPauldrons"), MaxEnergy);
		}

		Super.DoEffect();
	}

	override void AbsorbDamage(int damage, Name damageType, out int newDamage, Actor inflictor, Actor source, int flags)
	{
		if (A_GetWeaponLevel() < 2 && Owner.FindInventory("ShouldersAssaultPauldrons"))
		{
			Energy = min(Energy + 4, MaxEnergy);
			return;
		}
		else if (A_GetWeaponLevel() >= 2 && Owner.FindInventory("ShouldersAssaultPauldrons"))
		{
			Energy = min(Energy + 4, MaxEnergy);
		}
		else if (A_GetWeaponLevel() < 2)
		{
			return;
		}

		Energy = min(Energy + 10, MaxEnergy);
		HomingTicker = 0;
	}

	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "A large, heavy blade. Final slash releases a large wave at full power. Power regenerates even when not using the weapon. Press alt-fire the moment after the first slash to release a small wave at the cost of power.";
			case 2: return "Energy is now also gained while taking or dealing damage.";
			case 3: return "After taking no damage for 6 seconds, every slash will release a homing sword beam. Taking damage resets the effect.";
		}
		return Super.GetEffectText(lvl);
	}

	const MaxEnergy = 100;
	int Energy;
	private bool AllHoming;
	private int HomingTicker;

	Default
	{
		Inventory.Icon "WEAP25";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.UpSound "beplayer/weapswitchgalaxyslicer";
		BEWeapon.Sprite "WEAPH25";
		BEWeapon.Type WType_Melee;
		BEWeapon.Element WElement_Void;
		BEWeapon.Attributes 5, 1, 6, 2;
		BEWeapon.FlavorText "This hefty sword can mow down foes with ease, but the drawback is that its weight means very slow swings. Energy blades can be launched from the sword after a brief charging period, which only adds to the destruction this weapon can bring. There are rumors of a legendary hunter using this sword, but those rumors never state the name of this supposed hunter. In any case, using this weapon, perhaps you can create your own legends for people to tell around the campfire or local bars.";
		BEWeapon.CraftingCost 1200;
		+WEAPON.ALT_AMMO_OPTIONAL
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.NOAUTOFIRE
		+WEAPON.CHEATNOTWEAPON
		+WEAPON.MELEEWEAPON
		+BEWEAPON.ELITEWEAPON
		Tag "\cl*\c[TalayPeaks] Galaxy Slicer\c-";
		//Tag "\cl*\cx Galaxy Slicer";
		BEWeapon.Tier 5;
		BEWeapon.DynamicDamage 300, 0.30;
	}

	States
	{
		Spawn:
			PGXS A 0;
			Goto Super::Spawn;
		Ready:
			_WGS A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			_WGS A 0 A_TakeInventory("GalaxySlicerAttackHeld", 2);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKL", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKR", 1);
			_WGS A 0 A_GiveInventory("GalaxySlicerAltPrevent", 1);
			//_WGS A 0 A_TakeInventory("GalaxySlicerAntiSpam", 10);
			Loop;
		DeselectReal:
			TNT1 AA 0 A_Lower;
			_WGS A 1 A_Lower;
			Loop;
		SelectReal:
			TNT1 AA 0 A_Raise;
			_WGS A 1 A_Raise;
			Loop;
		Reload:
			Stop;
		Fire: //Right2Left
			_WGS A 0 A_CheckWeaponFire(0);
			_WGS A 0 A_StopSound(10);
			_WGS A 0 A_JumpIfInventory("GalaxySlicerAttackHeld", 1, 5);
			_WGS G 2 A_WeaponOffset(280, 36);
			_WGS G 2 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS A 0 A_TakeInventory("GalaxySlicerAttackHeld", 1);
			TNT1 A 3 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS B 2 A_WeaponOffset(300, 72);
			_WGS B 2 A_WeaponOffset(265, 72);
			_WGS B 2 A_WeaponOffset(230, 72);
			_WGS C 1 A_WeaponOffset(185, 56);
			_WGS C 1 A_WeaponOffset(130, 57);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetenerB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_StartSound("weapons/galaxyslicerswingpool", CHAN_WEAPON, CHANF_OVERLAP, 1);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtra", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraSweetener", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS C 1 A_WeaponOffset(75, 71);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 Radius_Quake(5, 4, 0, 1, 0);
			_WGS A 0 A_FireBullets(0, 0, 1, 110, "GalaxySlicerMPuff", FBF_EXPLICITANGLE | FBF_NORANDOM, 180 + (CountInv("GlovesExtendingGauntlets") * 50));
			_WGS A 0
			{
				if (invoker.AllHoming)
				{
					A_FireBEProjectile("GalaxySlicerSwordBeam", 0, 0, 0, 1, 0, 0);
					A_StartSound("weapons/galaxyslicerswordbeam", CHAN_AUTO, CHANF_OVERLAP, 1);
				}
			}
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS D 1 A_WeaponOffset(103, 82);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener2B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener3B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS D 1 A_WeaponOffset(60, 100);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS E 1 A_WeaponOffset(-35, 115);
			_WGS A 0 A_TakeInventory("GalaxySlicerAltPrevent", 1);
			_WGS A 0 A_GiveInventory("GalaxySlicerSmallBeamOKL", 1);
			_WGS E 1 A_WeaponOffset(-84, 131);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS E 1 A_WeaponOffset(-125, 141);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS F 1 A_WeaponOffset(-166, 157);
			TNT1 A 8 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			TNT1 A 0 A_Refire("Hold");
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
			Goto Ready;
		Hold: //Left2Right
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKL", 1);
			_WGS A 0 A_StopSound(10);
			_WGS A 0 A_GiveInventory("GalaxySlicerAttackHeld", 1);
			TNT1 A 1 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS H 1 Offset(-166, 72);
			_WGS H 1 Offset(-125, 72);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS H 1 Offset(-84, 72);
			_WGS A 0 A_StartSound("weapons/galaxyslicerswingpool", CHAN_WEAPON, CHANF_OVERLAP, 1);
			_WGS I 1 Offset(-35, 56);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipe", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraB", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraSweetenerB", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS I 1 Offset(60, 57);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 Radius_Quake(5, 4, 0, 1, 0);
			_WGS A 0 A_FireBullets(0, 0, 1, 110, "GalaxySlicerMPuff", FBF_EXPLICITANGLE | FBF_NORANDOM, 180 + (CountInv("GlovesExtendingGauntlets") * 50));
			_WGS A 0
			{
				if (invoker.AllHoming)
				{
					A_FireBEProjectile("GalaxySlicerSwordBeam", 0, 0, 0, 1, 0, 0);
					A_StartSound("weapons/galaxyslicerswordbeam", CHAN_AUTO, CHANF_OVERLAP, 1);
				}
			}
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener2", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener3", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS I 1 Offset(103, 71);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS J 1 Offset(75, 82);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS J 1 Offset(130, 100);
			_WGS K 1 Offset(185, 115);
			_WGS A 0 A_TakeInventory("GalaxySlicerAltPrevent", 1);
			_WGS A 0 A_GiveInventory("GalaxySlicerSmallBeamOKR", 1);
			_WGS K 1 Offset(230, 131);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS K 1 Offset(265, 141);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS L 1 Offset(300, 157);
			TNT1 A 8 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			TNT1 A 0 A_Refire("HoldSlash2");
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
			Goto Ready;
		HoldSlash2: //Right2Left Third Attack
			_WGS A 0 A_CheckWeaponFire(0);
			_WGS A 0 A_TakeInventory("GalaxySlicerAttackHeld", 2);
			_WGS A 0 A_StopSound(10);
			_WGS A 0
			{
				if (random (1, 100) <= 60)
				{
					A_StartSound("beplayer/quickkickwinduppool", CHAN_AUTO, 0, 1);
				}
			}
			TNT1 A 3 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS B 2 A_WeaponOffset(300, 72);
			_WGS B 2 A_WeaponOffset(265, 72);
			_WGS B 2 A_WeaponOffset(230, 72);
			_WGS C 1 A_WeaponOffset(185, 56);
			_WGS C 1 A_WeaponOffset(130, 57);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetenerB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_StartSound("weapons/galaxyslicerswingpool", CHAN_WEAPON, CHANF_OVERLAP, 1);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtra", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraSweetener", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS C 1 A_WeaponOffset(75, 71);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 Radius_Quake(5, 4, 0, 1, 0);
			_WGS A 0 A_FireBullets(0, 0, 1, 110, "GalaxySlicerMPuff", FBF_EXPLICITANGLE | FBF_NORANDOM, 180 + (CountInv("GlovesExtendingGauntlets") * 50));
			_WGS A 0
			{
				if (invoker.AllHoming)
				{
					A_FireBEProjectile("GalaxySlicerSwordBeam", 0, 0, 0, 1, 0, 0);
					A_StartSound("weapons/galaxyslicerswordbeam", CHAN_AUTO, CHANF_OVERLAP, 1);
				}
			}
			_WGS A 0
			{
				if (invoker.Energy >= invoker.MaxEnergy)
				{
					A_SpawnItemEx("GalaxySlicerBlastShine", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
					A_StartSound("weapons/galaxyslicerswordwave", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("GalaxySlicerSwordWave", 0, 0, 0, 1, 0, 0);
					Radius_Quake(8, 8, 0, 1, 0);
					A_TakeEnergy(100);
				}
				else
				{
					A_SpawnItemEx("GalaxySlicerBlastShineSmall", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
					A_StartSound("weapons/galaxyslicerswordwave", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("GalaxySlicerSwordWaveSmallShort", 0, 0, 0, 1, 0, 0);
				}	
			}
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS D 1 A_WeaponOffset(103, 82);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener2B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener3B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS D 1 A_WeaponOffset(60, 100);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS E 1 A_WeaponOffset(-35, 115);
			_WGS A 0 A_GiveInventory("GalaxySlicerAttackHeld", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerAltPrevent", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKL", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKR", 1);
			//_WGS A 0 A_TakeInventory("GalaxySlicerAntiSpam", 5);
			_WGS E 1 A_WeaponOffset(-84, 131);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS E 1 A_WeaponOffset(-125, 141);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS F 1 A_WeaponOffset(-166, 157);
			_WGS A 0
			{
				if (random (1, 100) <= 60)
				{
					A_StartSound("beplayer/quickkickendpool", CHAN_AUTO, 0, 1);
				}
			}
			_WGS A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("AccessoryMeteoriteWhetstone", 1) && random(1, 100) <= 20)
				{
					return ResolveState("HoldSlash2");
				}
				return ResolveState(null);
			}
			TNT1 A 12 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			TNT1 A 0 A_Refire("Fire");
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
			Goto Ready;
		Altfire: //Right2Left Small Beam - After Second Slash
			//_WGS A 0 A_JumpIfInventory("GalaxySlicerAntiSpam", 10, "Fire");
			_WGS A 0 A_JumpIfInventory("GalaxySlicerAltPrevent", 1, "Fire");
			_WGS A 0 A_JumpIfInventory("GalaxySlicerSmallBeamOKL", 1, "Altfire2");
			_WGS A 0 A_JumpIfInventory("GalaxySlicerSmallBeamOKR", 1, 1);
			Goto Hold + 37;
			_WGS A 0 A_CheckWeaponFire(0);
			_WGS A 0 A_TakeInventory("GalaxySlicerAttackHeld", 2);
			_WGS A 0 A_StopSound(10);
			TNT1 A 2 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS B 2 A_WeaponOffset(300, 72);
			_WGS B 2 A_WeaponOffset(265, 72);
			_WGS B 2 A_WeaponOffset(230, 72);
			_WGS C 1 A_WeaponOffset(185, 56);
			_WGS C 1 A_WeaponOffset(130, 57);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetenerB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_StartSound("weapons/galaxyslicerswingpool", CHAN_WEAPON, CHANF_OVERLAP, 1);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtra", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraSweetener", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS C 1 A_WeaponOffset(75, 71);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeB", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 Radius_Quake(5, 4, 0, 1, 0);
			_WGS A 0 A_FireBullets(0, 0, 1, 60, "GalaxySlicerMPuff", FBF_EXPLICITANGLE | FBF_NORANDOM, 180 + (CountInv("GlovesExtendingGauntlets") * 50));
			_WGS A 0
			{
				if (invoker.AllHoming)
				{
					A_FireBEProjectile("GalaxySlicerSwordBeam", 0, 0, 0, 1, 0, 0);
					A_StartSound("weapons/galaxyslicerswordbeam", CHAN_AUTO, CHANF_OVERLAP, 1);
				}
			}
			_WGS A 0
			{
				if (invoker.Energy >= 15)
				{
					A_SpawnItemEx("GalaxySlicerBlastShineSmall", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
					A_StartSound("weapons/galaxyslicerswordwave", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("GalaxySlicerSwordWaveSmall", 0, 0, 0, 1, 0, 0);
					Radius_Quake(8, 8, 0, 1, 0);
					A_TakeEnergy(25);
				}
			}
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS D 1 A_WeaponOffset(103, 82);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener2B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener3B", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY | SXF_NOCHECKPOSITION );
			_WGS D 1 A_WeaponOffset(60, 100);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS E 1 A_WeaponOffset(-35, 115);
			_WGS E 1 A_WeaponOffset(-84, 131);
			_WGS E 1 A_WeaponOffset(-125, 141);
			_WGS F 1 A_WeaponOffset(-166, 157);
			_WGS A 0 A_GiveInventory("GalaxySlicerAttackHeld", 1);
			_WGS A 0 A_GiveInventory("GalaxySlicerSmallBeamOKL", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKR", 1);
			//_WGS A 0 A_GiveInventory("GalaxySlicerAntiSpam", 1);
			TNT1 A 12 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			TNT1 A 2;
			TNT1 A 0 A_Refire("AltHold");
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
			Goto Ready;
		Altfire2: //Left2Right Small Beam - After First Slash
			//_WGS A 0 A_JumpIfInventory("GalaxySlicerAntiSpam", 10, "Fire");
			_WGS A 0 A_JumpIfInventory("GalaxySlicerAltPrevent", 1, "Fire");
			_WGS A 0 A_JumpIfInventory("GalaxySlicerSmallBeamOKL", 1, 1);
			Goto Fire + 41;
			_WGS A 0 A_StopSound(10);
			_WGS A 0 A_GiveInventory("GalaxySlicerAttackHeld", 1);
			TNT1 A 1 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WGS H 1 Offset(-166, 72);
			_WGS H 1 Offset(-125, 72);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS H 1 Offset(-84, 72);
			_WGS A 0 A_StartSound("weapons/galaxyslicerswingpool", CHAN_WEAPON, CHANF_OVERLAP, 1);
			_WGS I 1 Offset(-35, 56);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipe", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraB", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeExtraSweetenerB", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS I 1 Offset(60, 57);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 Radius_Quake(5, 4, 0, 1, 0);
			_WGS A 0 A_FireBullets(0, 0, 1, 60, "GalaxySlicerMPuff", FBF_EXPLICITANGLE | FBF_NORANDOM, 180 + (CountInv("GlovesExtendingGauntlets") * 50));
			_WGS A 0
			{
				if (invoker.AllHoming)
				{
					A_FireBEProjectile("GalaxySlicerSwordBeam", 0, 0, 0, 1, 0, 0);
					A_StartSound("weapons/galaxyslicerswordbeam", CHAN_AUTO, CHANF_OVERLAP, 1);
				}
			}
			_WGS A 0
			{
				if (invoker.Energy >= 15)
				{
					A_SpawnItemEx("GalaxySlicerBlastShineSmall", 45 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
					A_StartSound("weapons/galaxyslicerswordwave", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("GalaxySlicerSwordWaveSmall", 0, 0, 0, 1, 0, 0);
					Radius_Quake(8, 8, 0, 1, 0);
					A_TakeEnergy(25);
				}
			}
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener2", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS A 0 A_SpawnItemEx("GalaxySlicerSwipeSweetener3", 0 * cos(pitch), 0, 35 - (40 * sin(pitch)), vel.x, vel.y, vel.z, 0, SXF_ABSOLUTEVELOCITY|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH);
			_WGS I 1 Offset(103, 71);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS J 1 Offset(75, 82);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS J 1 Offset(130, 100);
			_WGS A 0 A_SwipeAttack(5, 'GalaxySlicer');
			_WGS K 1 Offset(185, 115);
			_WGS K 1 Offset(230, 131);
			_WGS K 1 Offset(265, 141);
			_WGS L 1 Offset(300, 157);
			_WGS A 0 A_GiveInventory("GalaxySlicerSmallBeamOKR", 1);
			_WGS A 0 A_TakeInventory("GalaxySlicerSmallBeamOKL", 1);
			//_WGS A 0 A_GiveInventory("GalaxySlicerAntiSpam", 1);
			TNT1 A 12 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			TNT1 A 2;
			TNT1 A 0 A_Refire("AltHold");
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
			Goto Ready;
		AltHold:
			_WGS A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			_WGS G 1 A_WeaponOffset(340, 75);
			_WGS G 1 A_WeaponOffset(320, 65);
			_WGS G 1 A_WeaponOffset(300, 48);
			_WGS G 1 A_WeaponOffset(280, 36);
			_WGS A 1 A_WeaponOffset(0, 32);
		AltHoldRaised:
			_WGS A 1 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY);
			TNT1 A 0 A_Refire("AltHoldRaised");
			Goto Ready;
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}

// [Ace] Melee attack puff.
class GalaxySlicerMPuff : BEPlayerPuff
{
	override int DoSpecialDamage(Actor target, int damage, Name damagetype)
	{
		let plr = BEPlayer(self.Target);
		if (plr)
		{
			let GalaxySlicer = GalaxySlicerWeapon(plr.FindInventory("GalaxySlicerWeapon"));
			if (GalaxySlicer)
			{
				if (plr.GetWeaponLevel("GalaxySlicerWeapon") >= 2)
				{
					GalaxySlicer.A_GiveEnergy(3 + plr.CountInv('ShouldersAssaultPauldrons'));
				}
			}
		}
		return Super.DoSpecialDamage(target, damage, damagetype);
	}
	
	Default
	{
		//+NOINTERACTION
		+PUFFONACTORS
		+PUFFGETSOWNER
		+ROLLSPRITE
		+BRIGHT
		+FORCEPAIN
		+THRUSPECIES
		+ALLOWTHRUFLAGS
		RenderStyle "Add";
		Alpha 0.75;
		Scale 0.8;
		DamageType "GalaxySlicer";
		Species "Player";
		SeeSound "weapons/galaxyslicerenemyhitpool";
	}

	States
	{
		Spawn:
			DRPM A 0 NoDelay
			{
				A_SpawnItemEx("GalaxySlicerMPuffClone", flags: SXF_TRANSFERPOINTERS);
				A_StartSound("weapons/galaxyslicerwallhitpool", CHAN_BODY, 0, 1.0);
				A_SetRoll(roll + random(0, 359));
			}
			DRHI ABCDEF 1;
			Stop;
		XDeath:
			DRHI A 0
			{
				A_SetRoll(roll + random(0, 359));
				A_SetScale(0.6);
				A_SpawnItemEx("GalaxySlicerSweepFX", 0, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
			}
			DRPM ABCDEF 1;
		XDeathFall:
			DRPM F 1 A_FadeOut(0.2);
			Loop;
	}
}

class GalaxySlicerMPuffClone : GalaxySlicerMPuff
{
	Default
	{
		Alpha 0.25;
		Scale 2.0;
	}

	States
	{
		Spawn:
			DRHI A 0 NoDelay A_SetRoll(random(0, 359));
			DRHI ABC 2;
			DRHI DEF 2;
			Stop;
		Death:
			DRHI F 1 A_FadeOut(0.05);
			Loop;
	}
}

class GalaxySlicerSweepFX : Actor
{
	Default
	{
		Radius 10;
		Height 5;
		RenderStyle "Add";
		Alpha 0.5;
		+BRIGHT
		+THRUSPECIES
		+ROLLSPRITE  
		XScale 0.8;
		YScale 2.5;
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_SetRoll(roll+random(0, 359));
		SpawnFall:
			GSSL C 1 A_FadeOut(0.1);
			Loop;
		Death:
			TNT1 A 1;
			Stop;
		}
}

class GalaxySlicerSwipe : Actor
{
	Default
	{
		RenderStyle "Add";
		+BRIGHT
		+NOINTERACTION
		+FLATSPRITE
		+ROLLSPRITE
		Alpha 0.8;
		Scale 1.5;
		ReactionTime 10;
	}

   States
	{
		Spawn:
			GSSL A 0 NoDelay
			{
				A_SetPitch(pitch - 10);
			}
		SpawnFall:
			GSSL A 1
			{
				A_SetRoll(roll - 20);
				A_Countdown();
			}
			Loop;
		Death:
			GSSL A 1 A_FadeOut(0.1);
			Loop;
	} 
}

class GalaxySlicerSwipeB : GalaxySlicerSwipe // [Mor] Left Swings
{
	States
	{
		Spawn:
			GSSL A 0 NoDelay 
			{
				A_SetPitch(pitch + 10);
				A_SetAngle(angle + 200);
			}
		SpawnFall:
			GSSL A 1
			{
				A_SetRoll(roll + 20);
				A_Countdown();
			}
			Loop;
	} 
}

class GalaxySlicerSwipeSweetener : GalaxySlicerSwipe
{
	Default
	{
		RenderStyle "Add";
		Alpha 0.12;
		Scale 1.8;
	}
}

class GalaxySlicerSwipeSweetenerB : GalaxySlicerSwipeB
{
	Default
	{
		RenderStyle "Add";
		Alpha 0.12;
		Scale 1.8;
	}
}

class GalaxySlicerSwipeSweetener2 : GalaxySlicerSwipeSweetener
{
	Default
	{
		Alpha 0.08;
		Scale 2.4;
		ReactionTime 6;
	}
	States
	{
	   SpawnFall:
			  GSSL B 1;
			  GSSL A 0 A_SetRoll(roll-20);
			  GSSL A 0 A_CountDown;
			  Loop;
	}
}

class GalaxySlicerSwipeSweetener2B : GalaxySlicerSwipeSweetenerB
{
	Default
	{
		Alpha 0.08;
		Scale 2.4;
		ReactionTime 6;
	}
	States
	{
	   SpawnFall:
			  GSSL B 1;
			  GSSL A 0 A_SetRoll(roll+20);
			  GSSL A 0 A_CountDown;
			  Loop;
	}
}

class GalaxySlicerSwipeSweetener3 : Actor //Mor'ladim - DON'T TOUCH THESE! Inheriting from other actors is just going to screw up the effects.
{
	Default
	{
		Radius 10;
		Height 5;
		RenderStyle "Add";
		Alpha 0.2;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		+SPRITEFLIP
		Scale 3.4;
		ReactionTime 4;
	}
	States
	{
		Spawn:
			_DRS A 0 NoDelay A_SetPitch(pitch-10);
		SpawnFall:
			_DRS A 1;
			_DRS A 0 A_SetRoll(roll-30);
			_DRS A 0 A_CountDown;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	} 
}

class GalaxySlicerSwipeSweetener3B : Actor
{
	Default
	{
		Radius 10;
		Height 5;
		RenderStyle "Add";
		Alpha 0.2;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		Scale 3.4;
		ReactionTime 4;
	}
	States
	{
		Spawn:
			_DRS A 0 NoDelay A_SetPitch(pitch-10);
		SpawnFall:
			_DRS A 1;
			_DRS A 0 A_SetRoll(roll+20);
			_DRS A 0 A_CountDown;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class GalaxySlicerSwipeExtra : Actor
{
	Default
	{
		Radius 10;
		Height 5;
		RenderStyle "Add";
		Alpha 0.3;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		Scale 1.2;
		ReactionTime 9;
	}
	States
	{
		Spawn:
			_GXS A 0 NoDelay A_SetPitch(pitch-12);
		SpawnFall:
			_GXS A 1;
			_GXS A 0 A_SetRoll(roll+38);
			_GXS A 0 A_CountDown;
			Loop;
		Death:
			_GXS A 1;
			_GXS A 0 A_FadeOut(0.1);
			Loop;  
   } 
}

class GalaxySlicerSwipeExtraB : Actor
{
	Default
	{
		Radius 10;
		Height 5;
		RenderStyle "Add";
		Alpha 0.3;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		+SPRITEFLIP
		Scale 1.2;
		ReactionTime 9;
	}
	States
	{
		Spawn:
			_GXS A 0 NoDelay A_SetPitch(pitch-12);
		SpawnFall:
			_GXS A 1;
			_GXS A 0 A_SetRoll(roll-38);
			_GXS A 0 A_CountDown;
			Loop;
		Death:
			_GXS A 1;
			_GXS A 0 A_FadeOut(0.1);
			Loop;
	} 
}

class GalaxySlicerSwipeExtraSweetener : GalaxySlicerSwipeExtra
{
	Default
	{
		RenderStyle "Stencil";
		StencilColor "Purple";
		Alpha 0.08;
		Scale 1.3;
	}
	States
	{
		Spawn:
			_GXS A 0 NoDelay A_SetPitch(pitch-15);
		SpawnFall:
			_GXS A 1;
			_GXS A 0 A_SetRoll(roll+45);
			_GXS A 0 A_CountDown;
			Loop;
   } 
}

class GalaxySlicerSwipeExtraSweetenerB : GalaxySlicerSwipeExtraB
{
	Default
	{
		RenderStyle "Stencil";
		StencilColor "Purple";
		Alpha 0.08;
		Scale 1.3;
	}
	States
	{
		Spawn:
			_GXS A 0 NoDelay A_SetPitch(pitch-15);
		SpawnFall:
			_GXS A 1;
			_GXS A 0 A_SetRoll(roll-45);
			_GXS A 0 A_CountDown;
			Loop;
	} 
}

class GalaxySlicerSwordBeam : BEPlayerFastProjectile //L3 Homing Effect
{
	Default
	{
		Radius 2;
		Height 2;
		Speed 45;
		RenderStyle "Add";
		Alpha 0.3;
		Scale 0.8;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		+SEEKERMISSILE
		PROJECTILE;
		DamageType "GalaxySlicer";
		ReactionTime 40;
		MissileType "GalaxySlicerSwordBeamTrail";
		MissileHeight 8;
	}
	States
	{
		Spawn:
			_GXS B 0 NoDelay A_SetPitch(pitch-5);
			_GXS B 0 A_SpawnItemEx("GalaxySlicerSwordBeamShine", flags: SXF_SETMASTER | SXF_ORIGINATOR);
		SpawnFall:
			_GXS B 1 A_Explode(25, 135, XF_THRUSTLESS, 135);
			TNT1 A 0 A_SeekerMissile(12, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			TNT1 A 1 A_Explode(25, 135, XF_THRUSTLESS, 135);
			TNT1 A 0 A_SeekerMissile(12, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			TNT1 A 0 A_CountDown;
			Loop;
		Death:
			_GXS B 1;
			_GXS B 0 A_FadeOut(0.1);
			Loop;
   } 
}

class GalaxySlicerSwordBeamTrail : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		Speed 45;
		RenderStyle "Stencil";
		StencilColor "Purple";
		Alpha 0.01;
		Scale 0.5;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		+NOGRAVITY
	}
	States
	{
		Spawn:
			_GXS B 0 NoDelay A_SetPitch(pitch-5);
		SpawnFall:
			_GXS B 1;
			TNT1 A 1;
			_GXS B 1;
			Goto Death;
		Death:
			_GXS B 1;
			_GXS B 0 A_FadeOut(0.01);
			Loop;  
   } 
}

class GalaxySlicerSwordWave : BEPlayerFastProjectile
{
	Default
	{
		Radius 2;
		Height 2;
		Speed 45;
		RenderStyle "Add";
		Alpha 0.3;
		XScale 2.5; //0.6
		YScale 1.2;
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+DONTSPLASH
		PROJECTILE;
		DamageType "GalaxySlicer";
		ReactionTime 16;
		MissileType "GalaxySlicerSwordWaveTrail";
		MissileHeight 8;
	}
	States
	{
		Spawn:
			_GXS C 0 NoDelay A_SetPitch(pitch-12);
			_GXS C 0 A_SpawnItemEx("GalaxySlicerWaveSweetener", flags: SXF_SETMASTER | SXF_ORIGINATOR);
		SpawnFall:
			_GXS C 1 A_Explode(90, 225, XF_THRUSTLESS, 225);
			TNT1 A 1 A_Explode(90, 225, XF_THRUSTLESS, 225);
			TNT1 A 0 A_CountDown;
			Loop;
		Death:
			_GXS C 1;
			_GXS C 0 A_FadeOut(0.1);
			Loop;
   } 
}

class GalaxySlicerSwordWaveSmall : GalaxySlicerSwordWave
{
	Default
	{
		Speed 35;
		XScale 0.5; //0.6
		YScale 0.5;
		ReactionTime 12;
		MissileType "GalaxySlicerSwordWaveTrailSmall";
	}
	States
	{
		Spawn:
			_GXS C 0 NoDelay A_SetPitch(pitch-12);
			_GXS C 0 A_SpawnItemEx("GalaxySlicerWaveSweetenerSmall", flags: SXF_SETMASTER | SXF_ORIGINATOR);
		SpawnFall:
			_GXS C 1 A_Explode(35, 125, XF_THRUSTLESS, 125);
			TNT1 A 1 A_Explode(35, 125, XF_THRUSTLESS, 125);
			TNT1 A 0 A_CountDown;
			Loop;
		Death:
			_GXS C 1;
			_GXS C 0 A_FadeOut(0.1);
			Loop;
   } 
}

class GalaxySlicerSwordWaveSmallShort : GalaxySlicerSwordWave //Short Lifespan.
{
	Default
	{
		Speed 35;
		XScale 0.3;
		YScale 0.3;
		ReactionTime 4;
		MissileType "GalaxySlicerSwordWaveTrailSmallShort";
	}
	States
	{
		Spawn:
			_GXS C 0 NoDelay A_SetPitch(pitch-12);
			_GXS C 0 A_SpawnItemEx("GalaxySlicerWaveSweetenerSmallShort", flags: SXF_SETMASTER | SXF_ORIGINATOR);
		SpawnFall:
			_GXS C 1 A_Explode(25, 125, XF_THRUSTLESS, 125);
			TNT1 A 1 A_Explode(25, 125, XF_THRUSTLESS, 125);
			TNT1 A 0 A_CountDown;
			Loop;
		Death:
			_GXS C 1;
			_GXS C 0 A_FadeOut(0.1);
			Loop;
   } 
}

class GalaxySlicerSwordWaveTrail : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		Speed 45;
		RenderStyle "Stencil";
		StencilColor "Pink";
		Alpha 0.01;
		XScale 1.6;
		YScale 1.0; //1.2
		+BRIGHT
		+THRUACTORS
		+FLATSPRITE
		+ROLLSPRITE
		+DONTSPLASH
		+NOGRAVITY
		ReactionTime 3;
	}
	States
	{
		Spawn:
			_GXS C 0 NoDelay A_SetPitch(pitch-12);
		SpawnFall:
			_GXS E 1;
			TNT1 A 1;
			_GXS E 1 SetShade("eb6bff");
			TNT1 A 1;
			_GXS E 1 SetShade("ce49e3");
			TNT1 A 1;
			_GXS E 1 SetShade("b02fc4");
			TNT1 A 1;
			_GXS E 1 SetShade("8a179c");
			TNT1 A 1;
			_GXS E 1 SetShade("6b077a");
		Travel:
			_GXS E 1;
			TNT1 A 1;
			TNT1 A 0
			{
				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			_GXS C 1;
			_GXS C 0 A_FadeOut(0.1);
			Loop;  
   } 
}

class GalaxySlicerSwordWaveTrailSmall : GalaxySlicerSwordWaveTrail
{
	Default
	{
		Speed 25;
		XScale 1.0;
		YScale 0.5;
   } 
}

class GalaxySlicerSwordWaveTrailSmallShort : GalaxySlicerSwordWaveTrail
{
	Default
	{
		Speed 25;
		XScale 0.5;
		YScale 0.5;
   } 
}

class GalaxySlicerWaveSweetener : Actor
{
	override void Tick()
	{
		if (!Master)
		{
			Destroy();
			return;
		}

		Super.Tick();
	}
	
	Default
	{
		+NOINTERACTION
		+WALLSPRITE
		+BRIGHT
		Radius 2;
		Height 2;
		Renderstyle "Add";
		Alpha 0.3;
		XScale 3.2;
		YScale 0.7;
	}
	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_Warp(AAPTR_MASTER, flags: WARPF_NOCHECKPOSITION);
			_GXS E 1;
			TNT1 A 1;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class GalaxySlicerWaveSweetenerSmall : GalaxySlicerWaveSweetener
{
	Default
	{
		XScale 1.0;
		YScale 0.4;
	}
}

class GalaxySlicerWaveSweetenerSmallShort : GalaxySlicerWaveSweetener
{
	Default
	{
		XScale 0.5;
		YScale 0.4;
	}
}

class GalaxySlicerBlastShine : Actor
{
	Default
	{
		+NOINTERACTION
		+NOCLIP
		+BRIGHT
		+ROLLSPRITE
		Radius 2;
		Height 2;
		Renderstyle "Add";
		Alpha 0.3;
		Scale 1.2;
	}
	States
	{
		Spawn:
			CBXP A 0 NoDelay A_SetRoll(frandom(-100, 100), 0, 0);
		SpawnFall:
			CBXP B 1 A_SetRoll(roll+10, SPF_INTERPOLATE, 0);
			TNT1 A 1;
			CGPT A 0 A_SetScale(1.0);
			CBXP B 1 A_SetRoll(roll+10, SPF_INTERPOLATE, 0);
			TNT1 A 1;
			CGPT A 0 A_SetScale(1.2);
			CBXP B 1 A_SetRoll(roll+10, SPF_INTERPOLATE, 0);
			TNT1 A 1;
			CGPT A 0 A_SetScale(1.4);
			Stop;
	}
}

class GalaxySlicerBlastShineSmall : GalaxySlicerBlastShine
{
	Default
	{
		Scale 0.4;
	}
}

class GalaxySlicerSwordBeamShine : Actor
{
	override void Tick()
	{
		if (!Master)
		{
			Destroy();
			return;
		}

		Super.Tick();
	}
	
	Default
	{
		+NOINTERACTION
		+BRIGHT
		+ROLLSPRITE
		Radius 2;
		Height 2;
		Renderstyle "Add";
		Alpha 0.2;
		Scale 0.45;
	}
	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_Warp(AAPTR_MASTER, flags: WARPF_NOCHECKPOSITION);
			_GXS D 1 A_SetRoll(random(0, 359));
			TNT1 A 1;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class GalaxySlicerAttackHeld : Inventory
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 2;
	}
}

class GalaxySlicerAltPrevent : Inventory //Prevents alt-fire state when player hasn't attacked yet - leading to easy waves otherwise.
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 1;
	}
}

class GalaxySlicerAntiSpam : Inventory //Prevents constant pressing of alt-fire to easily achieve beams. (May not be relevant anymore with tic adjustments.)
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 10;
	}
}

class GalaxySlicerSmallBeamOKR : Inventory //Right to left
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 1;
	}
}

class GalaxySlicerSmallBeamOKL : Inventory //Left to right
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 1;
	}
}
class BulletEyeWeapon : BEUltimateWeapon
{
	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "A heavy rapid fire weapon that can easily take down targets.";
		}
		return Super.GetEffectText(lvl);
	}

	Default
	{
		Radius 20;
		Height 16;
		Inventory.PickupSound "misc/w_pkup";
		Weapon.UpSound "ironannihilator/weaponup";
		Weapon.Kickback 100;
		Weapon.AmmoUse 1;
		Weapon.AmmoType1 "UltimateWeaponEnergy";
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		BEWeapon.Sprite "WEAPHU01";
		BEWeapon.Type WType_Ultimate;
		BEWeapon.Element WElement_Solid;
		BEWeapon.Attributes 5, 5, 5, 5;
		BEWeapon.FlavorText "Bullet-Eye: A monstrous weapon which fires a volley of high velocity rounds. The hand crank mechanism may seem outdated, but was added for a more personal flair. A setting allows the weapon to be perfectly capable of firing without utilizing the crank, but where is the fun in that?";
		Inventory.Icon "WEAPU01";
		+WEAPON.AMMO_OPTIONAL;
		//+WEAPON.NOAUTOAIM;
		Tag "\cg*\cx Bullet-Eye";
	}

	States
	{
		Ready:
			IANI A 0 A_JumpIfInventory("VeteranSpun", 1, "ReadyVeteranIdle");
			IANI A 0 A_JumpIfInventory("BeltBeltOfTheVeterans", 1, "ReadyVeteran");
			IANI A 0
			{
				GiveBody(CheckInventory("GlovesVitalGloves", 1) ? 8 : 4);

				if (CheckInventory("ArmorCombatArmorThorn", 1))
				{
					A_GiveInventory("CombatArmorThornReflectX", 1);
				}

				A_RadiusThrust(5000, 600, RTF_NOIMPACTDAMAGE | RTF_NOTMISSILE, 3000);
				A_AlertMonsters();
			}
			IANI A 0 { ShotwaveWeapon.SpawnShockwaveRing(self); }
			IANI A 0 A_StartSound("ironannihilator/spinup", CHAN_WEAPON, 0, 1);
			IANI ABCDE 2;
			IANA A 0
			{
				invoker.bNOAUTOAIM = !GetCvar('bulleteye_beautoaim');
			}
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 5);
			IANI FGHABCDE 2;
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 5);
			IANI FGH 2;
			IANI A 0 A_StopSound(5);
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY | WRF_NOSECONDARY | WRF_NOSWITCH);
			Goto Fire;
		ReadyVeteran:
			IANI A 0
			{
				GiveBody(CheckInventory("GlovesVitalGloves", 1) ? 10 : 4);

				if (CheckInventory("ArmorCombatArmorThorn", 1))
				{
					A_GiveInventory("CombatArmorThornReflectX", 1);
				}
			}
			IANI A 0 A_RadiusThrust(5000, 600, RTF_NOIMPACTDAMAGE | RTF_NOTMISSILE, 3000);
			IANI A 0 { ShotwaveWeapon.SpawnShockwaveRing(self); }
			IANI A 0 A_AlertMonsters;
			IANI A 5;
			IANI ABC 2;
			IANI DEFGH 2;
			IANI A 1 A_WeaponReady(WRF_NOSWITCH);
			IANI A 0
			{
				if (CountInv("VeteranSpun") == 0)
				{
					A_GiveInventory("VeteranSpun", 1);
				}
			}
		ReadyVeteranIdle:
			IANI A 1 A_WeaponReady(WRF_NOSWITCH | WRF_ALLOWZOOM);
			Loop;
		DeselectReal:
			IANA A 0 A_TakeInventory("VeteranSpun", 1);		
			IANI AAAAA 1 A_Lower(12);
			Loop;
		Select:
			TNT1 A 0
			{
				if (CheckInventory("HelmGuardVisor", 1))
				{
					A_GiveInventory("UltimateWeaponInvincibility");
				}
			}
			TNT1 AA 0 A_Raise;
			IANI A 1 A_Raise;
			Loop;
		Reload: // [Ace] Disables reloading
			Stop;
		Fire:
			IANI A 0 A_CheckWeaponFire(0);
			IANI A 0 A_JumpIfInventory("BeltBeltOfTheVeterans", 1, "FireVeteran");
			IANI A 0 A_JumpIfInventory("UltimateWeaponEnergy", 1, 1);
			Goto SpindownStop;
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI A 0 A_StartSound("ironannihilator/fire", 460, CHANF_LOOPING, 0.7);
			IANI A 0 A_AlertMonsters();
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			IANI AAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI I 2 Bright;
			IANI AAA 0 A_FireBullets(15.5, 4.5, -1, 100, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4); //1.5, 2.5
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, -12, 30, frandom(1.0, 2.0), frandom(-5.0, -9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0
			{	
				let plr = BEPlayer(self);
				if (plr.CheckInventory("ArmorSolarChargedMail", 1))
				{
					A_TakeInventory("UltimateWeaponEnergy", 2);
				}
				else
				{
					A_TakeInventory("UltimateWeaponEnergy", 4);
				}
			}
			IANI B 2 A_WeaponOffSet(3, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI AA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI J 2 Bright;
			IANI AAA 0 A_FireBullets(15.5, 4.5, -1, 100, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, 12, 30, frandom(1.0, 2.0), frandom(5.0, 9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 2);
			IANI D 2 A_WeaponOffSet(1, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 0.7);
			IANI AAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-2, 32, WOF_INTERPOLATE);
			IANI K 2 Bright;
			IANI AAA 0 A_FireBullets(15.5, 4.5, -1, 100, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, -12, 30, frandom(1.0, 2.0), frandom(-5.0, -9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0
			{	
				let plr = BEPlayer(self);
				if (plr.CheckInventory("ArmorSolarChargedMail", 1))
				{
					A_TakeInventory("UltimateWeaponEnergy", 2);
				}
				else
				{
					A_TakeInventory("UltimateWeaponEnergy", 4);
				}
			}
			IANI F 2 A_WeaponOffSet(1, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI AA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI L 2 Bright;
			IANI AAA 0 A_FireBullets(15.5, 4.5, -1, 100, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, 12, 30, frandom(1.0, 2.0), frandom(5.0, 9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
					A_FireBullets(35.5, 15.5, -1, 30, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 2);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProj", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI H 2 A_WeaponOffSet(2, 33, WOF_INTERPOLATE);
			IANA A 0
			{
				if ((GetCvar("bulleteye_beautoaim") == 1))
				{
					invoker.bNOAUTOAIM = false;
				}
				else
				{
					invoker.bNOAUTOAIM = true;
				}
			}
			IANI A 0 A_Refire;
			IANI A 0 A_StopSound(5);
			Loop;
		FireVeteran:
			IANI A 0 A_CheckWeaponFire(0);
			IANI A 0 A_JumpIfInventory("UltimateWeaponEnergy", 1, 1);
			Goto SpindownStop;
			IANI A 0 A_StartSound("ironannihilator/spinup", CHAN_WEAPON, 0, 1);
			IANI ABCDE 2;
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 1);
			IANI FGHABCDE 2;
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 1);
			IANI FGH 2;
			IANI A 0 A_Refire;
			IANI A 0 A_StartSound("ironannihilator/spindown", CHAN_WEAPON, 0, 1);
			IANI A 0 A_StopSound(5);
			IANI MNOA 1;
			IANI A 0 A_WeaponReady(WRF_NOSWITCH | WRF_NOBOB | WRF_NOFIRE);
			IANI MNOA 1;
			IANI A 0 A_WeaponReady(WRF_NOSWITCH | WRF_NOBOB | WRF_NOFIRE);
			IANI A 0 A_Refire;
			IANI MNOA 2;
			IANI A 0 A_WeaponReady(WRF_NOSWITCH | WRF_NOBOB | WRF_NOFIRE);
			IANI A 0 A_Refire;
			IANI MNO 2;
			IANI A 0 A_WeaponReady(WRF_NOSWITCH | WRF_NOBOB | WRF_NOFIRE);
			IANI A 0 A_Refire("Hold");
			IANI A 3;
			Goto ReadyVeteranIdle;
		Hold:
			IANI A 0 A_JumpIfInventory("UltimateWeaponEnergy", 1, 1);
			Goto SpindownStop;
			TNT1 A 0 A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI A 0 A_StartSound("ironannihilator/fire", 460, CHANF_LOOPING, 0.7);
			IANI A 0 A_AlertMonsters();
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			IANI AAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI I 2 Bright;
			IANI AA 0 A_FireBullets(1.5, 2.5, -1, 4, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 2, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, -12, 30, frandom(1.0, 2.0), frandom(-5.0, -9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 1);
			IANI B 2 A_WeaponOffSet(3, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI AA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI J 2 Bright;
			IANI AA 0 A_FireBullets(1.5, 2.5, -1, 4, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 2, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, 12, 30, frandom(1.0, 2.0), frandom(5.0, 9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 1);
			IANI D 2 A_WeaponOffSet(1, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI A 0 A_StartSound("ironannihilator/wind", CHAN_AUTO, 0, 0.7);
			IANI AAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-2, 32, WOF_INTERPOLATE);
			IANI K 2 Bright;
			IANI AA 0 A_FireBullets(1.5, 2.5, -1, 4, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 2, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, -12, 30, frandom(1.0, 2.0), frandom(-5.0, -9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 1);
			IANI F 2 A_WeaponOffSet(1, 33, WOF_INTERPOLATE);
			IANI A 0 A_GunFlash;
			IANI A 0 Bright Radius_Quake(3, 2, 0, 1, 0);
			TNT1 A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("BeltBeltOfTheVeterans", 1))
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOSWITCH);
				}
					else
				{
					A_WeaponReady(WRF_NOPRIMARY | WRF_NOBOB | WRF_NOSECONDARY | WRF_NOSWITCH);
				}
			}
			IANI AA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 3, random(4, 6) * cos(pitch), 0, random(6, 8) * sin(pitch), random(-3, 3));
			IANI A 0 A_WeaponOffSet(-1, 32, WOF_INTERPOLATE);
			IANI L 2 Bright;
			IANI AA 0 A_FireBullets(1.5, 2.5, -1, 4, "IAPuff", FBF_PUFFTARGET | FBF_NORANDOM, 8192, "BulletEyePierce", 4);
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 2)
				{
					A_FireBullets(35.5, 15.5, -1, 2, "IAPuffNoSparks", FBF_PUFFTARGET | FBF_NORANDOM, 8192, null, 4);
				}
			}
			IANI A 0 A_SpawnItemEx("BulletEyeCasing", 25, 12, 30, frandom(1.0, 2.0), frandom(5.0, 9.0), frandom(2.0, 3.0), 0, SXF_NOCHECKPOSITION);
			IANI A 0 A_JumpIfInventory("UltWeaponInfiniteAmmo", 1, 2);
			IANI A 0
			{	
				let plr = BEPlayer(self);
				if (plr.CheckInventory("ArmorSolarChargedMail", 1))
				{
					return;
				}
				else
				{
					A_TakeInventory("UltimateWeaponEnergy", 1);
				}
			}
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CountInv("BulletEyeModeLevel") >= 3)
				{
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
					A_FireBEProjectile("BulletEyeMissileProjVet", random(-10, 10), 0, 0, 0, 0, random(-5, 5));
				}
			}
			IANI H 2 A_WeaponOffSet(2, 33, WOF_INTERPOLATE);
			IANA A 0
			{
				if ((GetCvar("bulleteye_beautoaim") == 1))
				{
					invoker.bNOAUTOAIM = false;
				}
				else
				{
					invoker.bNOAUTOAIM = true;
				}
			}
			IANI A 0 A_Refire;
			IANI A 0 A_StopSound(5);
			Goto SpindownVeteran;
		AltFire:
			IANI A 0 A_CheckWeaponFire(0);
			IANI A 0 A_JumpIfInventory("BeltBeltOfTheVeterans", 1, 1);
			Goto Fire;
			IANA A 0 A_TakeInventory("VeteranSpun", 1);
			IANI A 0 A_TakeInventory("UltimateWeaponEnergy", 40);
			Goto SpindownStop;
		SpindownStop:
			IANI A 0 A_StopAllSounds();
			IANI A 0 A_StartSound("ironannihilator/spindown", CHAN_AUTO, 0, 1);
			IANI A 0 A_RadiusThrust(3000, 600, RTF_NOIMPACTDAMAGE | RTF_NOTMISSILE, 1500);
			IANI A 0 { ShotwaveWeapon.SpawnShockwaveRing(self); }
			IANI MNOAMNOAMNOAMNOA 1;
			/*
			IANI A 0
			{
				let plr = BEPlayer(self);
				if (plr.CheckInventory("HelmCDTest", 1))
				{
					plr.InventoryCooldown = 0;
				}
			}
			*/
			IANI A 1 A_DeselectUltimate();
			Stop;
		SpindownVeteran:
			IANI A 0 A_StopAllSounds();
			IANI A 0 A_StartSound("ironannihilator/spindown", CHAN_AUTO, 0, 1);
			IANI A 0 { ShotwaveWeapon.SpawnShockwaveRing(self); }
			IANI MNOAMNOAMNOAMNOA 1;
			IANI A 1;
			Goto ReadyVeteranIdle;
		Flash:
			TNT1 A 2 Bright A_Light1;
			TNT1 A 2 Bright A_Light2;
			TNT1 A 0 Bright A_Light0;
			Stop;
		Spawn:
			IAPK A -1;
			Stop;
	}
}

class IronAnnihilatorShotSparkFX : Actor
{
	Default
	{
		Scale 0.06;
		Gravity 0.3;
		-NOGRAVITY
		+NOINTERACTION
		+DONTSPLASH
		RenderStyle "Add";
		ReactionTime 3;
	}

	States
	{
		Spawn:
			TNT1 A 1
			{
				A_SpawnItemEx("IronABulletGlow");
				A_SpawnItemEx("IronABulletTrail");

				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			TNT1 A 1 A_FadeOut(0.1);
			Loop;
	}
}


class BulletEyeMissileProj : BEPlayerFastProjectile
{
	Default
	{
		PROJECTILE;
		+SEEKERMISSILE
		+SCREENSEEKER
		+THRUSPECIES
		+PAINLESS
		Species "Player";
		DamageType "BulletEye";
		Radius 4;
		Height 4;
		Speed 40;
		DamageFunction (35);
		Scale 0.4;
	}

	States
	{
		Spawn:
			CMSL A 4;
		SpawnFall:   
			CMSL A 1 Bright A_SeekerMissile(8, 8, SMF_CURSPEED|SMF_PRECISE|SMF_LOOK, 256);
			CMSL A 0 A_JumpIf(GetCVar("bulleteye_fxreduction") == 1, 2);
			CMSL A 0 A_SpawnItemEx("BEMissileSmokeFX", random(-2, -3), random(3, -3), random(2, -3), flags: SXF_NOCHECKPOSITION);
			CMSL A 1 Bright A_SeekerMissile(8, 8, SMF_CURSPEED|SMF_PRECISE|SMF_LOOK, 256);
			Loop;
		Death:
			CMSL A 0 A_CustomBulletAttack(0, 0, 1, 0, "BulletEyeActPuff", 0, CBAF_AIMFACING);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileExplosion", flags: SXF_NOCHECKPOSITION);
			Stop;
	}
}

class BEMissileSmokeFX : Actor
{
	Default
	{
		Height 2;
		Radius 2;
		+NOGRAVITY
		+NOINTERACTION
		+NOBLOCKMAP
		+DONTSPLASH
		+THRUACTORS
		+ROLLSPRITE
		RenderStyle "Add";
		Scale 0.5;
		Alpha 0.5;
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_SetRoll(roll + random(0, 359));
			CMSM A 1 A_SetTranslucent(0.45, 1);
			//CMSM B 1 A_SetTranslucent(0.4, 1);
			CMSM C 1 A_SetTranslucent(0.35, 1);
			//CMSM D 1 A_SetTranslucent(0.3, 1);
			CMSM E 1 A_SetTranslucent(0.25, 1);
			//CMSM F 1 A_SetTranslucent(0.2, 1);
			CMSM G 1 A_SetTranslucent(0.15, 1);
			//CMSM H 1 A_SetTranslucent(0.1, 1);
			CMSM I 1 A_SetTranslucent(0.09, 1);
			//CMSM J 1 A_SetTranslucent(0.08, 1);
			CMSM K 1 A_SetTranslucent(0.07, 1);
			//CMSM L 1 A_SetTranslucent(0.06, 1);
			CMSM M 1 A_SetTranslucent(0.05, 1);
			//CMSM N 1 A_SetTranslucent(0.04, 1);
			CMSM O 1 A_SetTranslucent(0.03, 1);
			//CMSM P 1 A_SetTranslucent(0.02, 1);
			Stop;
	}
}

class BulletEyeMissileProjVet : BulletEyeMissileProj
{
	Default
	{
		DamageFunction 3 * random(4, 8);
	}
}

class IronABulletGlow : Actor
{
	Default
	{
		+NOINTERACTION
		+THRUACTORS
		+BRIGHT
		+DONTSPLASH
		RenderStyle "Add";
		Alpha 0.06;
		Scale 0.15;
	}

	States
	{
		Spawn:
			SPGL A 1 Bright;
			Stop;
	}
}

class IronABulletTrail : Actor
{
	Default
	{
		Radius 6;
		Height 8;
		Scale 0.01;
		RenderStyle "Add";
		Alpha 0.4;
		Translation "80:111=160:167";
		+NOINTERACTION
		+BRIGHT
		+DONTSPLASH
	}

	States
	{
		Spawn:
			IAGL A 0 NoDelay A_JumpIf(bulleteye_fxreduction == 1, "Death");
			IAGL A 1 A_FadeOut(0.1);
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class IAPuff : BEPlayerPuff
{
	Default
	{
		DamageType "BulletEye";
		Species "Player";
		RenderStyle "Add";
		Alpha 0.8;
		Scale 1.6;
		+NOBLOCKMAP
		+NOGRAVITY
		+NOEXTREMEDEATH
		+PUFFONACTORS
		+BLOODLESSIMPACT
		+MTHRUSPECIES
		+BRIGHT
	}

	States
	{
		Spawn:
			RPP2 A 0 NoDelay A_StartSound("weapons/bulleteyericochetpool", CHAN_AUTO, 0, 1.0);
			RPP2 AB 2;
			RPP2 A 0 A_SpawnItemEx("IAPuffB");
			RPP2 A 0 A_SpawnItemEx("IAPuffRotated");
			RPP2 C 1;
			Stop;
	}
}

class IAPuffNoSparks : IAPuff //Similar to original, except it won't spawn extra sparks which reduce framerate.
{
	States
	{
		Spawn:
			RPP2 A 0 NoDelay A_StartSound("weapons/bulleteyericochetpool", CHAN_AUTO, 0, 1.0);
			RPP2 AB 2;
			RPP2 A 0 A_SpawnItemEx("IAPuffRotatedShort");
			RPP2 C 1;
			Stop;
	}
}

class IAPuffRotated : Actor
{
	Default
	{
		RenderStyle "Add";
		Alpha 0.25;
		Scale 2.8;
		+ROLLSPRITE
		+THRUACTORS
		+BRIGHT
	}

	States
	{
		Spawn:
			RPP2 A 0 NoDelay A_SetRoll(random(0, 359));
			RPP2 ABC 2;
			Stop;
	}
}

class IAPuffRotatedShort : IAPuffRotated //Single frame of animation that spawns from IAPuffNoSparks to alleviate FPS issues.
{
	States
	{
		Spawn:
			RPP2 A 0 NoDelay A_SetRoll(random(0, 359));
			RPP2 A 2;
			Stop;
	}
}

class IAPuffB : Actor
{
	Default
	{
		Scale 1.5;
		Renderstyle "Add";
		Alpha 0.8;
		+NOBLOCKMAP
		+NOGRAVITY
		+PUFFONACTORS
		+ROLLSPRITE
		+BRIGHT
	}

	States
	{
		Spawn:
			RPPF A 0 NoDelay A_SetRoll(random(0, 359));
			RPPF A 0 A_JumpIf(bulleteye_fxreduction == 1, "Death");
			RPPF AAAAAAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", random(-4, 4), random(-4, 4), random(-4, 4));
			RPPF AAAAAA 0 A_SpawnItemEx("BulletEyePuffSparkYellow", 0, 0, 8, random(-4, 4), random(-4, 4), random(3, 7), random(0, 359));
			RPPF AAAAAA 0 A_SpawnItemEx("BulletEyePuffSparkOrange", 0, 0, 8, random(-4, 4), random(-4, 4), random(3, 7), random(0, 359));
			RPPF ABCDE 1 BRIGHT;
			Stop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class BulletEyePuffSparkYellow : Actor
{
	Default
	{
		Speed 10;
		Radius 4;
		Height 4;
		Scale 0.4;
		Gravity 1.2;
		Projectile;
		+BRIGHT
		+THRUACTORS
		-NOGRAVITY
		+DONTSPLASH
		RenderStyle "AddStencil";
		StencilColor "Yellow";
		ReactionTime 12;
	}

	States
	{
		Spawn:
			SPTL A 3;
			SPTL A 0 A_CountDown;
			Loop;
		Death:
			TNT1 A 1;
			TNT1 A 0 A_FadeOut(0.15, 1);
			Loop;
	}
}

class BulletEyePuffSparkOrange : BulletEyePuffSparkYellow
{
	Default
	{
		StencilColor "Orange";
	}
}

class BulletEyePierce : BEPlayerFastProjectile
{
	Default
	{
		Radius 2;
		Height 2;
		Speed 75;
		DamageType "BulletEye";
		Projectile;
		+BRIGHT
		+THRUACTORS
		+BLOODLESSIMPACT
		+THRUSPECIES
		+PAINLESS
		+DONTSPLASH
		Species "Player";
		Scale 0.2;
		DeathSound "weapons/scraplauncherprojpool";
		MissileHeight 8;
		MissileType "ScrapTrail";
		ReactionTime 15;
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_SpawnItemEx("BulletEyePierceDMG");
			//TNT1 A 0 A_Explode(4, 20, 0, 0, 6);
			TNT1 A 1;
			//TNT1 A 0 A_Explode(4, 20, 0, 0, 6);
			TNT1 A 0 A_SpawnItemEx("BulletEyePierceDMG");
			TNT1 A 0 A_CountDown;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	} 
}

class BulletEyePierceDMG : Actor
{
	Default
	{
		Speed 2;
		Radius 10;
		Height 5;
		DamageFunction (15);
		DamageType "BulletEye";
		Projectile;
		+BLOODLESSIMPACT
		+NODAMAGETHRUST
		+DONTSPLASH
		+DONTBLAST
		+THRUSPECIES
		+PAINLESS
		Species "Player";
	}

	States
	{
		Spawn:
			TNT1 A 2;
			Stop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class BulletEyeCasing : Actor
{
	Default
	{
		Speed 1;
		Scale 0.3;
		Radius 2;
		Height 2;
		Projectile;
		BounceType "Doom";
		BounceCount 3;
		BounceFactor 0.5;
		BounceSound "weapons/bulleteyecasingpool";
		- NOGRAVITY
		+DONTSPLASH
		-NOBLOCKMAP
		-SOLID
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			_BEC A 1 NoDelay A_SetRoll(roll + 40, SPF_INTERPOLATE, 0);
			TNT1 A 0 A_JumpIf(bulleteye_fxreduction == 1, "Spawn");
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (5 * vel.x) / -35.0, -(5 * vel.y) / -35.0, 2 + (5 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (10 * vel.x) / -35.0, -(10 * vel.y) / -35.0, 2 + (10 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (15 * vel.x) / -35.0, -(15 * vel.y) / -35.0, 2 + (15 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (20 * vel.x) / -35.0, -(20 * vel.y) / -35.0, 2 + (20 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (25 * vel.x) / -35.0, -(25 * vel.y) / -35.0, 2 + (25 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			TNT1 A 0 A_SpawnItemEx("BulletEyeCasingTrail", (30 * vel.x) / -35.0, -(30 * vel.y) / -35.0, 2 + (30 * vel.z) / -35.0, flags: SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION);
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}


class BulletEyeCasingTrail : Actor
{
	Default
	{
		RenderStyle "Add";
		Scale 0.2;
		Alpha 0.02;
		+NOINTERACTION
		+THRUACTORS
		+ROLLSPRITE
		+DONTSPLASH
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_SetRoll(roll + random(0, 359));
			_BCT A 12 Bright;
			_BCT AAAA 2 Bright A_FadeOut(0.1);
			Stop;
	}
}

class BulletEyeActPuff : BulletPuff //Allows projectiles to activate certain linedefs/switches that only allow activation with a hitscan.
{
	Default
	{
		+BLOODLESSIMPACT
		+PUFFONACTORS
	}

	States
	{
		Spawn:
			TNT1 A 8;
		Melee:
			TNT1 A 8;
			Stop;
	}
}

class VeteranSpun : Inventory //Checks if the Bullet-Eye ready spin animation has already played or not. This is so that it won't keep triggering in the status panel.
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 1;
		Inventory.Icon "TNT1A0";
	}
}
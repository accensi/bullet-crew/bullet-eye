class RepeatersWeapon : BEWeapon
{
	override void DrawHUDStuff(BulletEyeHUD hud, BEPlayer plr, Vector2 pos, int flags)
	{
		hud.DrawImage("WPNGAUGE", pos, flags);
		for (int i = 0; i < plr.CountInv("RepeatersStreamCharge"); ++i)
		{
			hud.DrawImage("REPEATCR", hud.AddOffset(flags, pos, (46 - 8 * i, 1)), flags);
		}
		
		hud.DrawBar("REPT20BR", "REPTBLBR", plr.CountInv("RepeatersL2ChargeStack20"), hud.GetMaxAmount("RepeatersL2ChargeStack20"), hud.AddOffset(flags, pos, (6, 9)), 0, 0, flags);
		hud.DrawBar("REPT40BR", "REPTBLBR", plr.CountInv("RepeatersL2ChargeStack40"), hud.GetMaxAmount("RepeatersL2ChargeStack40"), hud.AddOffset(flags, pos, (6, 9)), 0, 0, flags);
	}

	override void AbsorbDamage(int damage, Name damageType, out int newDamage, Actor inflictor, Actor source, int flags)
	{
		if (Owner.FindInventory("ShouldersAssaultPauldrons") && A_GetWeaponLevel() >= 2 && Owner.CountInv("RepeatersL2ChargeStack20") < 20)
		{
			Owner.A_GiveInventory("RepeatersL2ChargeStack20", 4);
		}
	}

	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "Fire two energy pistols rapidly. Has a higher ammo save chance. A charge is gained whenever the free ammo effect occurs. At six charges, a stream of explosive plasma is launched when firing ends.";
			case 2: return "After firing at least 20 shots in a row, fire large energy blasts for a short time once firing next begins. The power of the explosive stream is increased after at least 40 shots have been fired in a row.";
			case 3: return "When the free ammo effect occurs, you will also gain power shot stacks. Shots no longer need to be fired in a row for the power shots to trigger. Magnitude of stacks gained is increased depending on how empty the magazine is.";
		}
		return Super.GetEffectText(lvl);
	}

	Default
	{
		Inventory.Icon "WEAP14";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoRepeaters";
		Weapon.AmmoType2 "AmmoRapidEnergy";
		Weapon.UpSound "beplayer/weapswitchrepeater";
		BEWeapon.ReloadProperties "weapons/repeatersreloadstage1", "weapons/repeatersreloadstage2", "weapons/repeatersreloadstage3";
		BEWeapon.Sprite "WEAPH14";
		BEWeapon.Type WType_Rapid;
		BEWeapon.SubType WType_Tech;
		BEWeapon.Element WElement_Energy;
		BEWeapon.Attributes 2, 5, 4, 3;
		BEWeapon.FlavorText "A simple civilian-class plasma side-arm, rapid-fire and with multitude levels of output. A reliable self-defense weapon, perfect for keeping sustained fire on an intruder or stalker or whatever. Celebrities carry these all the time. Still, they're reliable, and their size means you can hold two at once.";
		BEWeapon.CraftingCost 105;
		+WEAPON.ALT_AMMO_OPTIONAL
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.CHEATNOTWEAPON
		Tag "Repeaters";
		BEWeapon.Tier 2;
		BEWeapon.DynamicDamage 400, 0.10;
	}

	States
	{
		Spawn:
			PPRE A 0;
			Goto Super::Spawn;
		Ready:
			_PRP A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Loop;
		DeselectReal:
			TNT1 AA 0 A_Lower;
			_PRP A 1 A_Lower;
			Loop;
		SelectReal:
			TNT1 AA 0 A_Raise;
			_PRP A 1 A_Raise;
			Loop;
		Fire:
			_PRP A 0 A_CheckWeaponFire;
			_PRP A 0 A_GunFlash;
			_PRP B 1 Bright
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters20FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/firelarge", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaLargeShot20", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, 9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
					A_TakeInventory("Repeaters20FireOK", 1);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaShot", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, 9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
				}
			}
			_PRP A 0
			{
				if (random (1, 256) <= 60 + (CheckInventory("BeltBeltOfBullets", 1 * 20)))
				{
					A_GiveInventory("RepeatersStreamCharge", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					
					if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") < 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					else if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") >= 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					
					if ((GetCvar("bulleteye_rapidfreesound") == 1))
					{
						A_StartSound("weapons/rapidweaponfreeammo", CHAN_UI, CHANF_LOCAL, bulleteye_rapidfreesoundvolume, 0);
					}
					return;
				}
				else
				{
					A_TakeAmmo(1);
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") < 20) && (CountInv("RepeatersL2ChargeStack40") == 0) && (CountInv("Repeaters20FireOK") == 0) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
				else if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") >= 20) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 3)
				{
					A_GiveInventory("RepeatersL3MagEmpty", 1);
				}
			}
			_PRP A 0 A_CheckWeaponFire;
			_PRP CD 1 A_SetTics(1 - CheckInventory("HelmBlitzSight", 1) * 1);
			_PRP A 0 A_GunFlash;
			_PRP E 1 Bright 
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters20FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/firelarge", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaLargeShot20", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, -9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
					A_TakeInventory("Repeaters20FireOK", 1);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaShot", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, -9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
				}
			}
			_PRP A 0
			{
				if (random (1, 256) <= 60 + (CheckInventory("BeltBeltOfBullets", 1 * 20)))
				{
					A_GiveInventory("RepeatersStreamCharge", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					
					if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") < 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					else if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") >= 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					
					if ((GetCvar("bulleteye_rapidfreesound") == 1))
					{
						A_StartSound("weapons/rapidweaponfreeammo", CHAN_UI, CHANF_LOCAL, bulleteye_rapidfreesoundvolume, 0);
					}
					return;
				}
				else
				{
					A_TakeAmmo(1);
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") < 20) && (CountInv("RepeatersL2ChargeStack40") == 0) && (CountInv("Repeaters20FireOK") == 0) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
				else if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") >= 20) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 3)
				{
					A_GiveInventory("RepeatersL3MagEmpty", 1);
				}
			}
			_PRP FG 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			//_PRP A 1;
			_PRP A 1 A_ReFire;
			Goto Ready;
		Hold:
			_PRP A 0 A_CheckWeaponFire;
			_PRP A 0 A_GunFlash;
			_PRP B 1 Bright
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters20FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/firelarge", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaLargeShot20", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, 9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
					A_TakeInventory("Repeaters20FireOK", 1);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaShot", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, 9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
				}
			}
			_PRP A 0
			{
				if (random (1, 256) <= 60 + (CheckInventory("BeltBeltOfBullets", 1 * 20)))
				{
					A_GiveInventory("RepeatersStreamCharge", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					
					if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") < 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					else if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") >= 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					
					if ((GetCvar("bulleteye_rapidfreesound") == 1))
					{
						A_StartSound("weapons/rapidweaponfreeammo", CHAN_UI, CHANF_LOCAL, bulleteye_rapidfreesoundvolume, 0);
					}
					return;
				}
				else
				{
					A_TakeAmmo(1);
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") < 20) && (CountInv("RepeatersL2ChargeStack40") == 0) && (CountInv("Repeaters20FireOK") == 0) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
				else if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") >= 20) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 3)
				{
					A_GiveInventory("RepeatersL3MagEmpty", 1);
				}
			}
			_PRP A 0 A_CheckWeaponFire;
			_PRP CD 1 A_SetTics(1 - CheckInventory("HelmBlitzSight", 1) * 1);
			_PRP A 0 A_GunFlash;  
			_PRP E 1 Bright 
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters20FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/firelarge", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaLargeShot20", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, -9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
					A_TakeInventory("Repeaters20FireOK", 1);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, CHANF_OVERLAP, 1);
					A_FireBEProjectile("RepeatersPlasmaShot", frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")), 1, -9, 3, 0, frandom(-1.5 + CountInv("GlovesSteadyAimGloves"), 1.5 - CountInv("GlovesSteadyAimGloves")));
				}
			}
			_PRP A 0
			{
				if (random (1, 256) <= 60 + (CheckInventory("BeltBeltOfBullets", 1 * 20)))
				{
					A_GiveInventory("RepeatersStreamCharge", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					
					if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") < 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					else if (A_GetWeaponLevel() >= 3 && (CountInv("RepeatersL2ChargeStack20") >= 20))
					{
						A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CountInv("RepeatersL3MagEmpty") / 16) + (CheckInventory("ShouldersAssaultPauldrons", 0)));
					}
					
					if ((GetCvar("bulleteye_rapidfreesound") == 1))
					{
						A_StartSound("weapons/rapidweaponfreeammo", CHAN_UI, CHANF_LOCAL, bulleteye_rapidfreesoundvolume, 0);
					}
					return;
				}
				else
				{
					A_TakeAmmo(1);
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") < 20) && (CountInv("RepeatersL2ChargeStack40") == 0) && (CountInv("Repeaters20FireOK") == 0) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack20", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
				else if (A_GetWeaponLevel() >= 2 && (CountInv("RepeatersL2ChargeStack20") >= 20) && (CountInv("Repeaters40FireOK") == 0))
				{
					A_GiveInventory("RepeatersL2ChargeStack40", 1 + (CheckInventory("ShouldersAssaultPauldrons", 0)));
				}
			}
			_PRP A 0
			{
				if (A_GetWeaponLevel() >= 3)
				{
					A_GiveInventory("RepeatersL3MagEmpty", 1);
				}
			}
			_PRP FG 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			//_PRP A 1;
			_PRP A 1 A_ReFire;
			_PRP A 0 
			{
				if (CheckInventory("RepeatersL2ChargeStack20", 20))
				{
					//A_TakeInventory("RepeatersL2ChargeStack20", 20);
					A_GiveInventory("Repeaters20FireOK", 20);
				}
				else if (A_GetWeaponLevel() < 3)
				{
					A_TakeInventory("RepeatersL2ChargeStack20", 0);
					A_TakeInventory("RepeatersL2ChargeStack40", 0);
				}
			}
			_PRP A 0 A_JumpIfInventory("RepeatersStreamCharge", 6, 1);
			Goto Fire;
			_PRP A 0 A_GunFlash;
			_PRP A 0 
			{
				if (CheckInventory("RepeatersL2ChargeStack40", 20))
				{
					A_GiveInventory("Repeaters40FireOK", 1);
					A_TakeInventory("RepeatersL2ChargeStack40", 20);
					A_TakeInventory("RepeatersL2ChargeStack20", 20);
				}
				else if (A_GetWeaponLevel() < 3)
				{
					A_TakeInventory("RepeatersL2ChargeStack40", 20);
					A_TakeInventory("RepeatersL2ChargeStack20", 20);
				}
			}
			_PRP B 1 Bright
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters40FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, 0, 1);
					A_FireBEProjectile("EchoPlasmaProjPoweredShot", 0, 1, 9, 3, 0, 0);
					A_FireBEProjectile("EchoPlasmaProjPoweredShot", 15, 1, 9, 3, 0, 0);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, 0, 1);
					A_FireBEProjectile("EchoPlasmaProj", 0, 1, 9, 3, 0, 0);
				}
			}
			_PRP CD 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_PRP A 0 A_StartSound("plasmarepeaters/fire", CHAN_AUTO, 0, 1);
			_PRP A 0 A_GunFlash;
			_PRP E 1 Bright
			{
				if (A_GetWeaponLevel() >= 2 && (CheckInventory("Repeaters40FireOK", 1)))
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, 0, 1);
					A_FireBEProjectile("EchoPlasmaProjPoweredShot", 0, 1, -9, 3, 0, 0);
					A_FireBEProjectile("EchoPlasmaProjPoweredShot", -15, 1, 9, 3, 0, 0);
					A_TakeInventory("RepeatersL2ChargeStack40", 0);
					A_TakeInventory("Repeaters40FireOK", 0);
				}
				else
				{
					A_StartSound("plasmarepeaters/fire", CHAN_AUTO, 0, 1);
					A_FireBEProjectile("EchoPlasmaProj", 0, 1, -9, 3, 0, 0);
				}
			}
			_PRP FG 1;
			_PRP A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			_PRP A 0 A_TakeInventory("RepeatersStreamCharge", 0);
			_PRP A 1 A_ReFire;
			Goto Ready;
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}


class RepeatersPlasmaShot : BEPlayerFastProjectile
{
	Default
	{
		Radius 7;
		Height 5;
		Speed 80;
		DamageFunction (12);
		DamageType "Repeaters";
		Alpha 0.5;
		Scale 0.1;
		Projectile;
		RenderStyle "Add";
		DeathSound "plasmarepeaters/impact";
		+BRIGHT
		+THRUSPECIES
		+NODAMAGETHRUST
		Species "Player";
		MissileHeight 8;
		MissileType "RepeatersPTrail";
	}

    States
    {
		Spawn:
			RPGL A 1;
			Loop;
		Death:
			PRBL AAAA 0 A_SpawnProjectile("PlasmaRProjSparkFX", 0, 0, random(0, 360), CMF_AIMDIRECTION, random(-12, 18));
			PRBL A 0 A_SetTranslucent(0.6);
			PRPF AABCDE 2 A_SetScale(0.3);
			Stop;
	}
}


class RepeatersPTrail : Actor
{
	Default
	{
		Radius 6;
		Height 8;
		Scale 0.07;
		RenderStyle "Add";
		Alpha 0.5;
		+NOINTERACTION
		+THRUACTORS
		+BRIGHT
	}

    States
    {
    Spawn:
	   RPGL A 1 A_FadeOut (0.1);
	   Loop;
	}
}

class EchoPlasmaProj : BEPlayerFastProjectile
{
	Default
	{
		Speed 40;
		Radius 3;
		Height 3;
		DamageFunction (12);
		DamageType "Repeaters";
		DeathSound "plasmarepeaters/impact";
		MissileType "EchoPlasmaFlyTrail";
		MissileHeight 8;
		Scale 0.6;
		Projectile;
		+BRIGHT
		+RIPPER
		+THRUSPECIES
		+NODAMAGETHRUST
		Species "Player"; 
		ReactionTime 6;
	}

    States
    {
		Spawn:
			PRBL ABCD 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosive", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL EFGH 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosive", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL IJKL 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosive", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL A 0 A_Countdown;
			Loop;
		Death: 
			PRBL AAAA 0 A_SpawnProjectile("PlasmaRProjSparkFX", 0, 0, random(0, 360), CMF_AIMDIRECTION, random(-12, 18));
			PRBL A 0 A_SetScale(0.4);
			PRBL A 0 A_SetTranslucent(0.8);
			PRPF AABCDE 1;
			Stop;
	}
}

class EchoPlasmaProjPoweredShot : EchoPlasmaProj
{
	Default
	{
		Speed 40;
		DamageFunction (16);
		DamageType "Repeaters";
		DeathSound "plasmarepeaters/impact";
		MissileType "EchoPlasmaFlyTrailPowered";
		MissileHeight 8;
		Scale 1.5;
		Projectile;
		+BRIGHT
		+RIPPER
		+THRUSPECIES
		+NODAMAGETHRUST
		Species "Player"; 
		ReactionTime 6;
	}

    States
    {
		Spawn:
			PRBL ABCD 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosivePowered", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL EFGH 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosivePowered", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL IJKL 1;
			PRBL A 0 A_SpawnItemEx("EchoPlasmaExplosivePowered", flags: SXF_NOCHECKPOSITION | SXF_SETMASTER);
			PRBL A 0 A_Countdown;
			Loop;
		Death: 
			PRBL AAAA 0 A_SpawnProjectile("PlasmaRProjSparkFX", 0, 0, random(0, 360), CMF_AIMDIRECTION, random(-12, 18));
			PRBL A 0 A_SetScale(0.4);
			PRBL A 0 A_SetTranslucent(0.8);
			PRPF AABCDE 1;
			Stop;
	}
}

class EchoPlasmaFlyTrail : Actor //Projectile flight effect.
{
	Default
	{
		+NOINTERACTION
		+NOCLIP
		+BRIGHT
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.2;
		Scale 0.3;
	}

    States
    {
		Spawn:
			RTRL ACDE 1;
			Stop;
	}
}

class EchoPlasmaFlyTrailPowered : EchoPlasmaFlyTrail
{
	Default
	{
		Scale 0.8;
	}
}

class EchoPlasmaExplosive : Actor
{
	Default
	{
		Projectile;
		Radius 6;
		Height 6;
		RenderStyle "Add";
		DamageType "Repeaters";
		Alpha 0.7;
		Scale 0.5;
		+NOCLIP
		+THRUACTORS
		+FORCERADIUSDMG
		+BRIGHT
		+NOGRAVITY
		+THRUSPECIES
		+ROLLSPRITE
		+NODAMAGETHRUST
		Species "Player";
		ReactionTime 12;
	}

    States
    {
		Spawn:
			TTRL A 0 NoDelay A_SetRoll(random(0, 359));
		SpawnFall:
			TTRL AB 1;
			TTRL A 0 A_Countdown;
			Loop;
		Death:
			TTRL A 0 A_SetScale(0.3);
			TTRL BCDE 1;
			2RXP A 1;
			TTRL A 0 A_StartSound("plasmarepeaters/altburst", CHAN_AUTO, 0, 1.0);
			2RXP AAAAAA 0 A_SpawnItemEx("PlasmaRProjSparkFX", random(-6, 6), random(-6, 6), random(-6, 6), random(2, 6), random(2, 6), random(2, 6), random(0, 359), SXF_NOCHECKPOSITION);
			2RXP AAA 0 A_SpawnItemEx("PlasmaRProjSparkFXLarge", random(-6, 6), random(-6, 6), random(-6, 6), random(2, 6), random(2, 6), random(2, 6), random(0, 359), SXF_NOCHECKPOSITION);
			TTRL A 0 A_Explode(35, 120, 0, 1, 60);
			TTRL A 0 A_SetScale(0.5);
			2RXP C 1;
			TTRL A 0 A_SetScale(0.6);
			2RXP D 1;
			Stop;
	}
}

class EchoPlasmaExplosivePowered : EchoPlasmaExplosive
{
	Default
	{
		+ROLLSPRITE
		Alpha 0.7;
		Scale 0.4;
	}

    States
    {
		Spawn:
			RTRL AC 1 NoDelay A_SetRoll(random(0, 359));
			RTRL A 0 A_Countdown;
			Loop;
		Death:
			RTRL A 0 A_SetScale(0.7);
			RTRL BCDE 1;
			2RXP A 1;
			RTRL A 0 A_StartSound("plasmarepeaters/altburst", CHAN_AUTO, 0, 1.0);
			2RXP AAAAAA 0 A_SpawnItemEx("PlasmaRProjSparkFX", random(-6, 6), random(-6, 6), random(-6, 6), random(2, 6), random(2, 6), random(2, 6), random(0, 359), SXF_NOCHECKPOSITION);
			2RXP AAA 0 A_SpawnItemEx("PlasmaRProjSparkFXLarge", random(-6, 6), random(-6, 6), random(-6, 6), random(2, 6), random(2, 6), random(2, 6), random(0, 359), SXF_NOCHECKPOSITION);
			RTRL A 0 A_Explode(60, 200, 0, 1, 100);
			RTRL A 0 A_SetScale(1.1);
			2RXP C 1;
			RTRL A 0 A_SetScale(1.2);
			2RXP D 1;
			Stop;
	}
}

class PlasmaRProjFX : Actor
{
	Default
	{
		Projectile;
		Radius 6;
		Height 6;
		RenderStyle "Add";
		Alpha 0.15;
		Scale 0.4;
		+NOCLIP
		+THRUACTORS
		+BRIGHT
	}

    States
    {
		Spawn:
			TSXP B 1;
			Stop;
	}
}

class PlasmaRProjFXB : PlasmaRProjFX
{
	Default
	{
		Alpha 0.25;
		Scale 0.2;
	}

    States
    {
		Spawn:
			TSXP C 1;
			Stop;
	}
}

class PlasmaRProjSparkFX : Actor
{
	Default
	{
	   Projectile;
	   Radius 2;
	   Height 2;
	   Speed 6;
	   RenderStyle "Add";
	   Alpha 0.8;
	   Scale 0.03;
	   +THRUACTORS
	   -NOGRAVITY
	   +DROPOFF
	   +DONTSPLASH
	   +BRIGHT
	}

    States
    {
		Spawn:
			TSXP B 1;
			Loop;
		Death:
			TSXP B 1 A_FadeOut(0.1);
			Loop;
	}
}

class PlasmaRProjSparkFXLarge : PlasmaRProjSparkFX
{
	Default
	{
		Scale 0.06;
	}
}

class RepeatersPlasmaLargeShot20 : BEPlayerFastProjectile
{
	Default
	{
		Radius 8;
		Height 6;
		Speed 80;
		DamageFunction (18);
		DamageType "Repeaters";
		Alpha 0.35;
		Scale 0.35;
		Projectile;
		RenderStyle "Add";
		+BRIGHT
		+FORCERADIUSDMG
		+THRUSPECIES
		+ROLLSPRITE
		+NODAMAGETHRUST
		Species "Player";
		MissileHeight 8;
		MissileType "RepeatersLargePlasmaTrail";
	}

    States
    {
		Spawn:
			BPRP A 0 NoDelay A_SetRoll(random(0, 359));
			BPRP ABC 2;
			Loop;
		Death:
			BPRX A 0 A_StartSound("plasmarepeaters/firelargeexp", CHAN_AUTO, 0, 1);
			BPRX A 0 A_Explode(12, 100, 0, 1, 50);
			BPRX A 0 A_SetTranslucent(0.75);
			BPRX A 0 A_SpawnItemEx("RepeatersLargeShotFX", flags: SXF_NOCHECKPOSITION);
		FadeFall:
			BPRX A 1 A_SetScale(scale.x + 0.2);
			BPRX A 0 A_FadeOut(0.08);
			Loop;
	}
}

class RepeatersLargePlasmaTrail : Actor
{
	Default
	{
		Projectile;
		Radius 2;
		Height 2;
		Speed 6;
		RenderStyle "Add";
		Alpha 0.25;
		Scale 0.25;
		+THRUACTORS
		+NOINTERACTION
		+BRIGHT
	}

    States
    {
		Spawn:
			BPRX A 1 A_FadeOut (0.08);
			Loop;
	}
}

class RepeatersLargeShotFX : Actor
{
	Default
	{
		Projectile;
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.3;
		Scale 0.4;
		+NOCLIP
		+THRUACTORS
		+BRIGHT
		ReactionTime 8;
	}

    States
    {
		Spawn:
			BPRX B 1 A_SetScale(scale.x + 0.15);
			BPRX B 1 A_CountDown;
			Loop;
		Death:
			BPRX B 1;
			BPRX B 0 A_FadeOut(0.1);
			Loop;
	}
}

class RepeatersStreamCharge : Inventory
{
	Default
	{
		Inventory.MaxAmount 6;
		Inventory.InterHubAmount 6;
		Inventory.Amount 1;
	}
}

class RepeatersL2ChargeStack20 : Inventory //After 20 shots.
{
	Default
	{
		Inventory.MaxAmount 20;
		Inventory.InterHubAmount 20;
		Inventory.Amount 1;
	}
}

class RepeatersL2ChargeStack40 : Inventory //After 40 shots.
{
	Default
	{
		Inventory.MaxAmount 20;
		Inventory.InterHubAmount 20;
		Inventory.Amount 1;
	}
}

class Repeaters20FireOK : Inventory //Empowered normal shot toggle. After 20 shots.
{
	Default
	{
		Inventory.MaxAmount 20;
		Inventory.InterHubAmount 20;
		Inventory.Amount 1;
	}
}

class Repeaters40FireOK : Inventory //Empowered explosive stream shot toggle. After 40 shots.
{
	Default
	{
		Inventory.MaxAmount 1;
		Inventory.InterHubAmount 1;
		Inventory.Amount 1;
	}
}

class RepeatersL3MagEmpty : Inventory //Affects L3 stack acquisition.
{
	Default
	{
		Inventory.MaxAmount 80;
		Inventory.InterHubAmount 80;
		Inventory.Amount 1;
	}
}

class AmmoRepeaters : Ammo
{
	override void DoEffect()
	{
		MaxAmount = default.MaxAmount + BEEquipmentBonus.GetTotalBonus(Owner, 'BonusRapidMagCap');
		Super.DoEffect();
	}

	Default
	{
		Inventory.Amount 80;
		Inventory.MaxAmount 80;
		Inventory.InterHubAmount 80;
	}
}
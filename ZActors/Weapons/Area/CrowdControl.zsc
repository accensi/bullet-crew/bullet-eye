class CrowdControlWeapon : BEWeapon //Weapon sprite by Sonik.O.Fan | Minor edits by Mor'ladim.
{
	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "Fires an array of high velocity pellets. If at least 12 pellets hit or if 2 enemies are defeated at the same time, the weapon fires an additional spread of weaker pellets for the next three shots. Reloads one shell at a time, but can be fired even during a reload.";
			case 2: return "Increases pellet count and slightly improves firing speed.";
			case 3: return "The large spread attack can now also trigger after every 10 enemies killed and from the weaker pellets.";
		}
		return Super.GetEffectText(lvl);
	}

	override void DoEffect()
	{
		if (A_GetWeaponLevel() >= 3)
		{
			if (EnemiesKilledCombo >= 2)
			{
				Owner.A_GiveInventory("CrowdControlSuperSpread", 3);
				Owner.A_GiveInventory("CrowdControlSuperPrevent", 1);
				A_StartSound("weapons/crowdcontrolsupertrigger");
				EnemiesKilledCombo = 0;
			}
			
			if (PelletsHitCombo >= 12)
			{
				Owner.A_GiveInventory("CrowdControlSuperSpread", 3);
				Owner.A_GiveInventory("CrowdControlSuperPrevent", 1);
				A_StartSound("weapons/crowdcontrolsupertrigger");
				PelletsHitCombo = 0;
			}
			
			if (EnemiesKilledL3 >= 10)
			{
				Owner.A_GiveInventory("CrowdControlSuperSpread", 3);
				Owner.A_GiveInventory("CrowdControlSuperPrevent", 1);
				A_StartSound("weapons/crowdcontrolsupertrigger");
				EnemiesKilledL3 = 0;
			}			
		}
		else if (A_GetWeaponLevel() <= 2)
		{
			if (EnemiesKilledCombo >= 2 && Owner.CountInv("CrowdControlSuperSpread") == 0)
			{
				Owner.A_GiveInventory("CrowdControlSuperSpread", 3);
				Owner.A_GiveInventory("CrowdControlSuperPrevent", 1);
				A_StartSound("weapons/crowdcontrolsupertrigger");
				EnemiesKilledCombo = 0;
			}
			
			if (PelletsHitCombo >= 12 && Owner.CountInv("CrowdControlSuperSpread") == 0)
			{
				Owner.A_GiveInventory("CrowdControlSuperSpread", 3);
				Owner.A_GiveInventory("CrowdControlSuperPrevent", 1);
				A_StartSound("weapons/crowdcontrolsupertrigger");
				PelletsHitCombo = 0;
			}
		}		
		
		Super.DoEffect();
	}
	
	override void OnEnemyKilled(BEEnemy enemy, BEPlayer plr, Name mod)
	{
		Super.OnEnemyKilled(enemy, plr, mod);

		if (plr.FindInventory('CrowdControlWeapon'))
		{
			EnemiesKilledCombo += 1;
		}
		
		if (plr.FindInventory('CrowdControlWeapon') && A_GetWeaponLevel() >= 3)
		{
			EnemiesKilledL3 += 1;
		}		
	}	
	
	int PelletsHitCombo;
	int EnemiesKilledCombo;
	int EnemiesKilledL3;
	int CrowdControlKillTimer;
	int KillCountdown;

	Default
	{
		Inventory.Icon "WEAP41";
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoCrowdControl";
		Weapon.AmmoType2 "AmmoAreaEnergy";
		Weapon.UpSound "beplayer/weapswitchcrowdcontrol";
		BEWeapon.ReloadProperties "weapons/crowdcontrolreloadstage1", "weapons/crowdcontrolreloadstage2", "weapons/crowdcontrolreloadstage3";
		BEWeapon.Sprite "WEAPH41";
		BEWeapon.Type WType_Area;
		BEWeapon.Element WElement_Solid;
		BEWeapon.Attributes 3, 3, 1, 1;
		BEWeapon.FlavorText "A standard weapon when compared to the tools that manufacturers put out these days, but still no less useful. When confronting a large group of hostiles, this firearm is far more effective while firing in between targets rather than directly at them. Despite its insane spread, it still packs a punch when attacking at near point blank range. As a safety feature, whenever ammunition is loaded, the weapon must be racked before it can fire. Due to being a basic weapon, it is very inexpensive to mass produce.";
		BEWeapon.CraftingCost 105;
		+WEAPON.ALT_AMMO_OPTIONAL
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.CHEATNOTWEAPON
		Tag "Crowd Control";
		BEWeapon.Tier 1;
		BEWeapon.DynamicDamage 400, 0.10;
	}

	States
	{
		Spawn:
			PCRD A 0;
			Goto Super::Spawn;
		Ready:
			1WCC A 1 A_WeaponReady(WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			Loop;
		DeselectReal:
			TNT1 AA 0 A_Lower;
			1WCC A 1 A_Lower;
			Loop;
		SelectReal:
			TNT1 AA 0 A_Raise;
			1WCC A 1 A_Raise;
			Loop;
		Fire:
			1WCC A 0 A_CheckWeaponFire(2);
			1WCC B 1 Bright 
			{
				if (A_GetWeaponLevel() >= 2)
				{
					A_FireBullets(9, 5, 16, 17, "CrowdControlPuff", FBF_NORANDOM); //23
				}
				else
				{
					A_FireBullets(9, 5, 12, 17, "CrowdControlPuff", FBF_NORANDOM); //23
				}
			}
			1WCC A 0
			{
				if (CountInv("CrowdControlSuperSpread") >= 1 && !FindInventory('CrowdControlSuperPrevent'))
				{
					if (A_GetWeaponLevel() >= 3)
					{
						A_FireBullets(18, 12, 25, 3, "CrowdControlPuffTinyL3", FBF_NORANDOM);
						A_StartSound("weapons/crowdcontrolsuper", CHAN_AUTO, CHANF_OVERLAP, 1.0);
						A_TakeInventory("CrowdControlSuperSpread", 1);
					}
					else
					{
						A_FireBullets(18, 12, 25, 3, "CrowdControlPuffTiny", FBF_NORANDOM);
						A_StartSound("weapons/crowdcontrolsuper", CHAN_AUTO, CHANF_OVERLAP, 1.0);
						A_TakeInventory("CrowdControlSuperSpread", 1);
					}
				}
			}
			1WCC A 0 Radius_Quake(2, 2, 0, 2, 0);
			1WCC A 0 A_AlertMonsters;
			1WCC A 0 A_TakeAmmo(2);
			1WCC C 1 A_StartSound("weapons/crowdcontrolfire", CHAN_WEAPON, 0, 0.75);
			1WCC I 2 Offset(0, 38);
			1WCC J 2 Offset(0, 40);
			1WCC I 2 Offset(0, 36);
			1WCC A 4
			{
				if (A_GetWeaponLevel() >= 2)
				{
					A_SetTics(2);
				}
				else
				{
					A_SetTics(5);
				}
			}
			1WCC D 3 Offset(0, 34);
			1WCC E 3 Offset(0, 33);
			1WCC F 3 Offset(0, 32);
			1WCC G 3 Offset(0, 32);
			1WCC A 0 A_StartSound("weapons/crowdcontrolpump", CHAN_AUTO, 0, 1.0);
			1WCC A 0 A_SpawnItemEx("CrowdControlCasing", 25, -6, 25, frandom(1.0, 2.0), frandom(5.0, 9.0), frandom(2.0, 4.0), 0, SXF_NOCHECKPOSITION | SXF_TRANSFERPITCH);
			1WCC A 0
			{
				invoker.PelletsHitCombo = 0;
				invoker.EnemiesKilledCombo = 0;
			}
			1WCC HGF 2 Offset(0, 32);
			1WCC ED 2;
			1WCC A 1;
			1WCC A 0 A_ReFire;
			1WCC A 1;
			Goto Ready;
		Reload: //60 tics
			1WCC A 0
			{
				let plr = BEPlayer(self);
				int MagCapCheck = BEEquipmentBonus.GetTotalBonus(plr, 'BonusAreaMagCap');
				if (CountInv("AmmoAreaEnergy") <= 1 || CountInv("AmmoCrowdControl") >= 12 + MagCapCheck)
				{
					return ResolveState("Ready");
				}
				
				return ResolveState(null);
			}		
			1WCC A 0 { invoker.IsReloading = true; }
			1WCC A 0 A_StopSound(1);
			1WCC A 0 A_StopSound(230);
			1WCC A 1 A_WeaponOffset(0, 43);
			1WCC A 1 A_WeaponOffset(0, 60);
			1WCC A 1 A_WeaponOffset(0, 80);
			1WCC A 0 A_StartSound("weapons/crowdcontrolreloadstage1", CHAN_WEAPON, CHANF_OVERLAP);
			1WCC A 6 A_WeaponOffset(6, 83);
			1WCC A 7 A_WeaponOffset(3, 83);
		ReloadLoop:
			1WCC A 0
			{
				let plr = BEPlayer(self);
				int MagCapCheck = BEEquipmentBonus.GetTotalBonus(plr, 'BonusAreaMagCap');
				if (CountInv("AmmoCrowdControl") >= 12 + MagCapCheck)
				{
					return ResolveState("ReloadEnd");
				}

				return ResolveState(null);
			}
			1WCC A 0
			{
				if (CountInv("AmmoAreaEnergy") >= 2)
				{
					A_GiveInventory("AmmoCrowdControl", 2);
					A_TakeInventory("AmmoAreaEnergy", 2);
				}
				else if (CountInv("AmmoAreaEnergy") <= 1)
				{
					return ResolveState("ReloadEnd");
				}
				
				return ResolveState(null);
			}
			1WCC A 0 A_WeaponReady(WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			1WCC A 0 A_StartSound("weapons/crowdcontrolreloadsingle", CHAN_WEAPON, CHANF_OVERLAP);
			1WCC A 5 A_WeaponOffset(2, 84);
			1WCC A 0 A_WeaponReady(WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			1WCC A 5 A_WeaponOffset(1, 85);
			1WCC A 0 A_WeaponReady(WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			1WCC A 5 A_WeaponOffset(1, 84);
			Loop;
		ReloadEnd:
			1WCC A 0 A_StartSound("weapons/crowdcontrolreloadstage3", CHAN_WEAPON, CHANF_OVERLAP);
			1WCC A 4 A_WeaponOffset(0, 83);
			1WCC A 3 A_WeaponOffset(0, 80);
			1WCC A 2 A_WeaponOffset(0, 60);
			1WCC A 1 A_WeaponOffset(0, 43);
			1WCC A 0 { invoker.IsReloading = false; }
			Goto Ready;
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}

class CrowdControlPuff : Actor
{
	override int DoSpecialDamage(Actor target, int damage, Name damagetype)
	{
		let plr = self.Target;
		if (plr)
		{
			let crowdcontrol = CrowdControlWeapon(plr.FindInventory('CrowdControlWeapon'));
			if (crowdcontrol)
			{
				crowdcontrol.PelletsHitCombo += 1;
			}
		}
		return Super.DoSpecialDamage(target, damage, damagetype);
	}

	Default
	{
		Scale 0.25;
		+NOGRAVITY
		+BRIGHT
		+ROLLSPRITE
		+THRUSPECIES
		+ALLOWTHRUFLAGS
		+PUFFONACTORS
		+PUFFGETSOWNER		
		Species "Player";
		DamageType "CrowdControl";
	}

	States
	{
		Spawn:
			CCHS A 0 NoDelay A_SetRoll(roll + random(0,359));
			CCHS AA 0 A_SpawnItemEx("CrowdControlGlowFX");
			CCHS ABCDEF 1;
			Stop;
	}
}

class CrowdControlPuffTiny : Actor
{
	Default
	{
		Scale 0.1;
		RenderStyle "AddStencil";
		StencilColor "Red";		
		+NOGRAVITY
		+BRIGHT
		+ROLLSPRITE
		+THRUSPECIES
		+ALLOWTHRUFLAGS
		+PUFFONACTORS
		+PUFFGETSOWNER		
		Species "Player";
		DamageType "CrowdControl";
	}

	States
	{
		Spawn:
			CCHS A 0 NoDelay A_SetRoll(roll + random(0,359));
			CCHS AA 0 A_SpawnItemEx("CrowdControlGlowFXRed");
			CCHS ABCDEF 1;
			Stop;
	}
}

class CrowdControlPuffTinyL3 : CrowdControlPuff
{
	Default
	{
		Scale 0.1;
		RenderStyle "AddStencil";
		StencilColor "Red";
	}
}

class CrowdControlGlowFX : Actor
{
	Default
	{
		RenderStyle "Add";
		Alpha 0.4;
		Scale 0.5;
		+NOGRAVITY
		+BRIGHT
		+THRUACTORS
		+NOINTERACTION	
	}

	States
	{
		Spawn:
			CCHS B 1
			{
				A_SetScale(scale.x + 0.12);
				A_FadeOut(0.08);
			}
			Loop;
	}
}

class CrowdControlGlowFXRed : Actor
{
	Default
	{
		RenderStyle "AddStencil";
		StencilColor "Red";
		Alpha 0.4;
		Scale 0.5;
		+NOGRAVITY
		+BRIGHT
		+THRUACTORS
		+NOINTERACTION	
	}

	States
	{
		Spawn:
			CCHS B 1
			{
				A_SetScale(scale.x + 0.12);
				A_FadeOut(0.08);
			}
			Loop;
	}
}

class CrowdControlCasing : Actor
{
	bool FlipChecked;

	Default
	{
		Speed 1;
		Scale 0.15;
		Radius 12;
		Height 12;
		Projectile;
		BounceType "Doom";
		BounceCount 3;
		BounceFactor 0.5;
		BounceSound "weapons/crowdcontrolshellpool";
		-NOGRAVITY
		+DONTSPLASH
		+THRUACTORS
		+NOTELEPORT
		//-NOBLOCKMAP
		//-SOLID
		+ROLLSPRITE
	}

	States
	{
		Spawn:
			1WCS A 1 A_SetRoll(roll + 8, SPF_INTERPOLATE, 0);
			Loop;
		Death:
			1WCS B 0 
			{
				if (random(1, 100) <= 50 && !FlipChecked)
				{
					//bSPRITEFLIP = true;
					bXFLIP = true;
				}
				FlipChecked = true;
				A_SetRoll(0);
			}
			1WCS B 1;
			1WCS B 0 A_FadeOut(0.01);
			Loop;
	}
}

class CrowdControlSuperSpread : Inventory
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 3;
	}
}

class CrowdControlSuperPrevent : PowerDamage //Prevents the super spread from immediately firing on the shot that meets requirements.
{
	Default
	{
		+INVENTORY.PERSISTENTPOWER
		Powerup.Duration 32;
		DamageFactor "CrowdControlSuperSpread", 0;
	}
}

class AmmoCrowdControl : Ammo
{
	override void DoEffect()
	{
		MaxAmount = default.MaxAmount + BEEquipmentBonus.GetTotalBonus(Owner, 'BonusAreaMagCap');
		Super.DoEffect();
	}

	Default
	{
		Inventory.Amount 12;
		Inventory.MaxAmount 12;
		Inventory.InterHubAmount 12;
	}
}
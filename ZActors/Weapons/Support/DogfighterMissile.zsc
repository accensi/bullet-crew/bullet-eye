class DogfighterMissileWeapon : BEWeapon
{
	override void OnEnemyKilled(BEEnemy enemy, BEPlayer plr, Name mod)
	{
		Super.OnEnemyKilled(enemy, plr, mod);

		//Support weapon feature: Chance to drop health augments and tiny ammo pick ups on kills.
		if (plr.FindInventory('DogfighterMissileWeapon'))
		{
			enemy.A_SpawnItemEx("HealthAugmentDropped", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 215 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
		}
	}

	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "Launches small homing missiles that fire at enemies and redirect fire to them instead of you. Deals extra damage to airborne enemies.";
			case 2: return "Missiles release a cluster of bomblets on destruction.";
			case 3: return "When a missile sustains moderate damage or its fuel depletes, it will charge into an enemy and cause area damage.";
		}
		return Super.GetEffectText(lvl);
	}

	Default
	{
		Inventory.Icon "WEAP05";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoDogfighterMissile";
		Weapon.AmmoType2 "AmmoSupportEnergy";
		Weapon.UpSound "beplayer/weapswitchDogfighterMissile";
		BEWeapon.ReloadProperties "weapons/dogfightermissilereloadstage1", "weapons/dogfightermissilereloadstage2", "weapons/dogfightermissilereloadstage3";
		BEWeapon.Sprite "WEAPH05";
		BEWeapon.Type WTYPE_Support;
		BEWeapon.Element WElement_Solid;
		BEWeapon.Attributes 1, 3, 1, 4;
		BEWeapon.FlavorText "The military uses this to flush enemies out of hiding, or to cause a distraction while a squad moves in. However, each missile only carries a tiny load of explosives, in order to make room for the ammo magazine. This in turn, means that direct missile impacts will be less than stellar. Still, it's great for getting enemies that just can't be reached or for countering enemy snipers. Just be sure to fire in big open spaces in order to utilize the extra firepower.";
		BEWeapon.CraftingCost 90;
		+WEAPON.ALT_AMMO_OPTIONAL 
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.CHEATNOTWEAPON
		Tag "Dogfighter Missile";
		BEWeapon.Tier 2;
		//BEWeapon.DamageBonus 0.15, 0; // [Mor] Higher boost because it's normally weaker than other Support weapons.
		//BEWeapon.DamagePerType 0.30; // [Mor] Higher boost due to the overall low power of Support weapons.
		BEWeapon.DynamicDamage 300, 0.20;
	}

	States
	{
		Spawn:
			PCSM A 0;
			Goto Super::Spawn;
		Ready:
			_CHM A 1 A_WeaponReady(WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			Loop;
		DeselectReal:
			TNT1 A 0 A_Lower(32);
			_CHM A 1 A_Lower;
			Loop;
		SelectReal:
			TNT1 A 0 A_InitOverlays;
			TNT1 A 0 A_Raise(32);
			_CHM A 1 A_Raise;
			Loop;
		Fire:
			_CHM A 0 A_CheckWeaponFire(4);
			_CHM A 1 Bright;
			_CHM A 0 A_AlertMonsters;
			_CHM A 0 A_StartSound("weapons/DogfighterMissilelaunch");
			_CHM A 0
			{
				if (A_GetWeaponLevel() >= 3)
				{
					A_FireBEProjectile("DogfighterMissileProjXX", 0, 1, 0, 0, 0, 0);
				}
				else if (A_GetWeaponLevel() >= 2)
				{
					A_FireBEProjectile("DogfighterMissileProjX", 0, 1, 0, 0, 0, 0);
				}
				else
				{
					A_FireBEProjectile("DogfighterMissileProj", 0, 1, 0, 0, 0, 0);
				}
			}
			_CHM A 0 A_TakeAmmo(6);
			_CHM A 0 A_GunFlash;
			_CHM A 1 Offset( 0, 36);
			_CHM A 1 Offset( 0, 45);
			_CHM A 1 Offset( 0, 50);
			_CHM A 3 Offset( 0, 45);
			_CHM A 2 Offset( 0, 40);
			_CHM A 1 Offset( 0, 32);
			_CHM A 1 A_WeaponReady(WRF_NOPRIMARY | WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4 | WRF_NOBOB);
			_CHM A 8 A_WeaponReady(WRF_NOPRIMARY | WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);
			_CHM A 1 A_ReFire;
			Goto Ready;
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}

class DogfighterMissileShot : BEPlayerFastProjectile
{
/*
	override int DoSpecialDamage(Actor target, int damage, Name damagetype)
	{
		let mon = BEEnemy(Target);
		if (mon && target && self.Target.CheckInventory("BonusDogfighterMissileL1", 0) && !mon.CheckInventory("DogfighterDebuff", 0) && mon.bFLOAT)
		{
			mon.A_GiveInventory("DogfighterDebuff");
		}
		return damage;
	}
*/
	
	Default
	{
		Radius 7;
		Height 5;
		Speed 45;
		DamageFunction (7);
		DamageType "DogfighterMissile";
		Alpha 0.8;
		Scale 0.1;
		Projectile;
		RenderStyle "Stencil";
		StencilColor "Yellow";
		+MTHRUSPECIES
		+THRUSPECIES
		+NODAMAGETHRUST
		+PAINLESS
		+BRIGHT
		Species "Player";
		MissileHeight 8;
		MissileType "DogfighterMissileShotTrail";
		SeeSound "weapons/dogfightermissilefire";
	}

	States
	{
		Spawn:
			BSTP ABCDEFGHIJKL 1 A_RadiusGive("DogfighterMissileTaunter", 12, RGF_MONSTERS);
			Loop;
		Death:
			BSTS A 0 A_StartSound("enemy/dreadtrooperbulletimppool", CHAN_BODY, 0, 0.5);
			BSTS AAA 0 A_SpawnItemEx("IronAnnihilatorShotSparkFX", 0, 0, 0, frandom(-12, 12), frandom(-12, 12), frandom(-12, 12), frandom(0, 360));
			BSTC A 1 A_SetScale(0.2);
			Stop;
	}
}

class DogfighterMissileShotTrail : Actor
{
	Default
	{
		Radius 6;
		Height 8;
		Scale 0.04;
		RenderStyle "Stencil";
		StencilColor "Yellow";
		Alpha 0.5;
		+NOINTERACTION
		+THRUACTORS
		+BRIGHT
	}

	States
	{
		Spawn:
			DTTR A 1 A_FadeOut (0.1);
			Loop;
	}
}

class DogfighterMissileProj : BEPlayerProjectile
{
	Default
	{
		Projectile;
		+SEEKERMISSILE
		+SCREENSEEKER
		+THRUSPECIES
		+SHOOTABLE
		-SOLID
		+THRUACTORS
		+MISSILE
		//-ISMONSTER
		Species "Player";
		DamageType "DogfighterMissile";
		Health 50;
		Radius 4;
		Height 4;
		Speed 12;
		DamageFunction (25);
		Scale 0.7;
		BounceFactor 2.0;
		ReactionTime 80;
	}

	States
	{
		Spawn:
			CMSL A 0 NoDelay Thing_ChangeTID(0, 9120);
			CMSL A 0
			{
				let plr = BEPlayer(Target);
				if (plr.FindInventory('BonusDogfighterMissileL1B'))
				{
					bBOUNCEONWALLS = true;
					bBOUNCEONFLOORS = true;
					bBOUNCEONCEILINGS = true;
				}
			}
			CMSL A 0 A_StartSound("weapons/dogfightermissilemove", 210, CHANF_LOOPING | CHANF_OVERLAP, 1.5);
			CMSL A 4;
		SpawnFall:
			CMSL A 2 Bright A_SeekerMissile(0, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			CMSL A 0 A_SpawnProjectile("DogfighterMissileShot", 0, 0, random(-3, 3), CMF_OFFSETPITCH | CMF_TRACKOWNER, random(-3, 3), AAPTR_TRACER);
			CMSL A 2 Bright A_SeekerMissile(0, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			CMSL A 0 A_JumpIf(bulleteye_fxreduction == 1, 2);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileSmokeFX", random(-2, -3), random(3, -3), random(2, -3), flags: SXF_NOCHECKPOSITION);
			CMSL A 0 A_RadiusGive("DogfighterMissileTaunter", 50, RGF_MONSTERS);
			CMSL A 0 A_CountDown;
			Loop;
		Death:
			CMSL A 0 A_StopSound(210);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileExplosion", flags: SXF_NOCHECKPOSITION);
			Stop;
	}
}

class DogfighterMissileProjX : DogfighterMissileProj
{
	States
	{
		Death:
			CMSL A 0 A_StopSound(210);
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 8, random(-4, 4), random(-4, 4), random(3, 7), random(0, 359));
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 5, random(-4, 8), random(-4, 5), random(4, 12), random(0, 359));
			CMSL A 0 A_SpawnItemEx("DogfighterMissileExplosion");
			Stop;
	}
}

class DogfighterMissileExplosion : BEPlayerProjectile
{
	Default
	{
		Projectile;
		+BRIGHT
		+FORCERADIUSDMG
		+THRUSPECIES
		+THRUACTORS
		Species "Player";
		DamageType "DogfighterMissile";
		Radius 4;
		Height 4;
		RenderStyle "Add";
		Alpha 0.75;
		Scale 0.7;
	}

	States
	{
		Spawn:
			SHXP A 0;
			Goto Death;
		Death:
			SHXP A 0 A_StartSound("weapons/DogfighterMissileexplode", CHAN_AUTO, 0, 0.6);
			SHXP A 0 A_SpawnItemEx("DogfighterMissileExpAfterFX");
			SHXP A 0 A_Explode(15, 85, 0, 1, 0);
			SHXP ABC 1;
			SHXP D 2 A_SetScale(1.2);
			SHXP E 2 A_SetTranslucent(0.4);
			TNT1 A 0 A_SetScale(0.9);
			SHXP F 2 A_SetTranslucent(0.2);
			Stop;  
	}
}

class DogfighterMissileProjXX : DogfighterMissileProj
{
	override void PostBeginPlay()
	{
		ChargeAttackTimer = 35; // This is multiplied by 6.

		Super.PostBeginPlay();
	}
	
	override int SpecialMissileHit(Actor victim)
	{
		if (CanIgnite)
		{
			let plr = BEPlayer(Target);
			bool enhanced = plr.CheckInventory("AccessoryRingOfIntenseFlames", 1);
			ElementData data;
			data.Element = WElement_Fire;
			data.BurnDamage = enhanced ? 35 : 15;
			data.BurnTicker = 35 * 12;
			data.BurnFrequency = enhanced ? 12 : 20;
			BEWeapon.ApplyEffects(BEEnemy(victim), OwningWeapon, self, plr, data);
		}

		return Super.SpecialMissileHit(victim);
	}

	private bool CanIgnite;
	private int ChargeAttackTimer;

	Default
	{
		Health 50;
		+SHOOTABLE
		+NOPAIN
		+MISSILE
		+USEBOUNCESTATE
		
		BounceType "Hexen";
		BounceCount 2;
		ReactionTime 120;
		
		DamageFactor "EnemyMelee", 0.0;
		DamageFactor "DreadTrooperDamage", 1.0;
		DamageFactor "CrusherDamage", 1.0;
		DamageFactor "RivetingGrenadierDamage", 1.0;
		DamageFactor "InvadingSwarmerDamage", 1.0;
		DamageFactor "BounderDamage", 1.0;
		DamageFactor "AlphaBounderDamage", 1.0;
		DamageFactor "GargantDamage", 1.0;
		DamageFactor "DreadVisageDamage", 1.0;
		DamageFactor "PhaseReaperDamage", 1.0;
		DamageFactor "CyclopticBruteDamage", 1.0;
		DamageFactor "AberwraitherDamage", 1.0;
		DamageFactor "RadTerrorDamage", 1.0;
		DamageFactor "NeoSquidDamage", 1.0;
		DamageFactor "RadTerrorDamage", 1.0;
		DamageFactor "MagRoaderDamage", 1.0;
		DamageFactor "MagRoaderFireDamage", 1.0;
		DamageFactor "LobolashiDamage", 1.0;
		DamageFactor "AvatarOfAgonyDamage", 1.0;
		//Vanilla Enemy Damage
		DamageFactor "SpiderMastermindDamage", 1.0;
		DamageFactor "Explosive", 1.0;
	}

	States
	{
		Spawn:
			CMSL A 0 NoDelay Thing_ChangeTID(0, 9121);
			CMSL A 0 A_StartSound("weapons/dogfightermissilemove", 210, CHANF_LOOPING | CHANF_OVERLAP, 1.5);
			CMSL A 0
			{
				let plr = BEPlayer(Target);
				if (plr.FindInventory('BonusDogfighterMissileL1B'))
				{
					bBOUNCEONWALLS = true;
					bBOUNCEONFLOORS = true;
					bBOUNCEONCEILINGS = true;
				}
			}
			CMSL A 4;
		SpawnFall:
			CMSL A 3 Bright A_SeekerMissile(0, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			CMSL A 0 A_SpawnProjectile("DogfighterMissileShot", 0, 0, frandom(-3, 3), CMF_OFFSETPITCH | CMF_TRACKOWNER, frandom(-3, 3), AAPTR_TRACER);
			CMSL A 2 Bright A_SeekerMissile(0, 20, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			CMSL A 0 A_JumpIf(bulleteye_fxreduction == 1, 2);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileSmokeFX", random(-2, -3), random(3, -3), random(2, -3), flags: SXF_NOCHECKPOSITION);
			CMSL A 0 A_JumpIf(--ChargeAttackTimer <= 0 || Health <= 0, "Charge");
			CMSL A 0 A_RadiusGive("DogfighterMissileTaunterL3", 50, RGF_MONSTERS);
			CMSL A 0 A_JumpIfHealthLower(5, "Charge");
			Loop;
		Charge:
			CMSL A 0
			{
				A_FaceTracer();
				A_ChangeVelocity(12, 0, 0, CVF_RELATIVE);
				bTHRUACTORS = false;
				bMISSILE = true;
			}
			CMSL A 0
			{
				let plr = BEPlayer(Target);
				if (plr && plr.FindInventory('BonusDogfighterMissileL3'))
				{
					CanIgnite = true;
				}
			}
		ChargeLoop:
			CMSL A 1 A_SeekerMissile(0, 30, SMF_CURSPEED | SMF_PRECISE | SMF_LOOK, 256);
			CMSL AAA 0 A_SpawnItemEx("DogfighterL3Explosion", 0, 0, frandom(-4, 12), flags: SXF_NOCHECKPOSITION);
			CMSL A 0 A_CountDown;
			Loop;
		Bounce.Wall:
			CMSL A 0;
			Goto Charge;
		Bounce.Ceiling:
			CMSL A 0;
			Goto Death;
		Bounce.Floor:
			CMSL A 0;
			Goto Death;			
		Death:
			CMSL A 0 A_StopSound(210);
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 8, frandom(-4, 4), frandom(-4, 4), frandom(3, 7), frandom(0, 359), SXF_NOCHECKPOSITION);
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 5, frandom(-4, 8), frandom(-4, 5), frandom(4, 12), frandom(0, 359), SXF_NOCHECKPOSITION);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileExplosion", flags: SXF_NOCHECKPOSITION);
			Stop;
		XDeath:
			CMSL A 0 A_StopSound(210);
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 8, frandom(-4, 4), frandom(-4, 4), frandom(3, 7), frandom(0, 359), SXF_NOCHECKPOSITION);
			CMSL AAAAA 0 A_SpawnItemEx("DogfighterMissileClusterShot", 0, 0, 5, frandom(-4, 8), frandom(-4, 5), frandom(4, 12), frandom(0, 359), SXF_NOCHECKPOSITION);
			CMSL A 0 A_SpawnItemEx("DogfighterMissileExplosion", flags: SXF_NOCHECKPOSITION);
			CMSL A 0 A_Explode(100, 250, 0, 1, 250);
			Stop;
	}
}

class DogfighterMissileTaunter : CustomInventory
{
	Default
	{
		+INVENTORY.ALWAYSPICKUP
	}

	States
	{
		Pickup:
			TNT1 A 0
			{
				if (GetClass() is "DogfighterMissileProj")
				{
					SetStateLabel("End");
				}
			}
			TNT1 A 0
			{
				if (GetClass() is "DogfighterMissileProjX")
				{
					SetStateLabel("End");
				}
			}
			TNT1 A 0 A_JumpIf(tidtohate==9120, "End");
			TNT1 A 0 Thing_Hate(0, 9120, 0);
		End:
			TNT1 A 0;
			Stop;
	}
}

class DogfighterMissileTaunterL3 : CustomInventory
{
	Default
	{
		+INVENTORY.ALWAYSPICKUP
	}

	States
	{
		Pickup:
			TNT1 A 0
			{
				if (GetClass() is "DogfighterMissileProjXX")
				{
					SetStateLabel("End");
				}
			}
			TNT1 A 0 A_JumpIf(tidtohate==9121, "End");
			TNT1 A 0 Thing_Hate(0, 9121, 0);
		End:
			TNT1 A 0;
			Stop;
	}
}

class DogfighterDebuff : PowerProtection //Equipment Bonus Effect
{
	Default
	{
		Powerup.Duration BE_INFINITEDURATION;
		DamageFactor "DogfighterMissile", 4.0;
	}
}

class DogfighterMissileExpAfterFX : Actor
{
	Default
	{
		+BRIGHT
		+NOINTERACTION
		+THRUACTORS
		+NOGRAVITY
		Radius 4;
		Height 4;
		RenderStyle "Add";
		Alpha 0.75;
		Scale 1.5;
		VSpeed 5;
	}

	States
	{
		Spawn:
			CMCX A 1 NoDelay
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX B 1
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX C 1
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX D 1
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX E 1
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX F 1
			{
				A_SetScale(scale.x + 0.1);
				A_ChangeVelocity(0, 0, 6);
				A_FadeOut(0.1);
			}
			CMCX G 1;
			Stop;
		Death:
			CMCX G 1 A_FadeOut(0.1);
			Loop;
	}
}

class DogfighterMissileSmokeFX : Actor
{
	Default
	{
		Height 2;
		Radius 2;
		+NOGRAVITY
		+NOINTERACTION
		+NOBLOCKMAP
		+DONTSPLASH
		+THRUACTORS
		+ROLLSPRITE
		RenderStyle "Add";
		Scale 0.5;
		Alpha 0.5;
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_SetRoll(roll + random(0, 359));
			CMSM A 2 A_SetTranslucent(0.45, 1);
			CMSM B 1 A_SetTranslucent(0.4, 1);
			CMSM C 1 A_SetTranslucent(0.35, 1);
			CMSM D 1 A_SetTranslucent(0.3, 1);
			CMSM E 2 A_SetTranslucent(0.25, 1);
			CMSM F 1 A_SetTranslucent(0.2, 1);
			CMSM G 1 A_SetTranslucent(0.15, 1);
			CMSM H 1 A_SetTranslucent(0.1, 1);
			CMSM I 2 A_SetTranslucent(0.09, 1);
			CMSM J 1 A_SetTranslucent(0.08, 1);
			CMSM K 1 A_SetTranslucent(0.07, 1);
			CMSM L 1 A_SetTranslucent(0.06, 1);
			CMSM M 1 A_SetTranslucent(0.05, 1);
			CMSM N 1 A_SetTranslucent(0.04, 1);
			CMSM O 1 A_SetTranslucent(0.03, 1);
			CMSM P 1 A_SetTranslucent(0.02, 1);
			Stop;
	}
}

class DogfighterMissileClusterShot : Actor
{
	Default
	{
		Speed 15;
		Radius 2;
		Height 2;
		Projectile;
		+CANBOUNCEWATER
		+BOUNCEONCEILINGS
		-NOGRAVITY
		+THRUACTORS
		+THRUSPECIES
		+BRIGHT
		Species "Player"; 
		DamageType "DogfighterMissile";
		Gravity 0.6;
		RenderStyle "Add";
		Alpha 0.8;
		Scale 0.3;
		Args 50;
	}

	States
	{
		Spawn:   
			CLSP A 8;
			CLSP A 0 A_Jump(256, "Left", "Right");
			Loop;
		Right:
			CLSP B 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP C 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP D 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP E 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP F 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP G 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP H 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
		RightFall:
			CLSP I 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP A 0 A_CountDownArg(0, "Death");
			Loop;
		Left:
			CLSP J 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP K 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP L 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP M 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP N 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP O 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP P 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
		LeftFall:
			CLSP I 1;
			CLSP A 0 A_SpawnItemEx("ScatterShotGlow");
			CLSP A 0 A_SpawnItemEx("ClusterShotDrag");
			CLSP A 0 A_CountDownArg(0, "Death");
			Loop;
		Death:
			CMCX A 0 A_StartSound("weapons/DogfighterMissilecluster", CHAN_AUTO, 0, 0.6);
			CMCX A 0 A_SetScale(0.55);
			CMCX A 0 {bNOGRAVITY = true;} 
			CMCX ABCDEFG 1;
			Stop;
	}
}

class ClusterShotDrag : Actor
{
	Default
	{
		Radius 15;
		Height 15;
		DamageFunction (8);
		DamageType "DogfighterMissile";
		Projectile;
		+THRUSPECIES
		Species "Player";
	}

	States
	{
		Spawn:
			TNT1 A 2;
			Stop;
	}
}

class DogfighterL3Explosion : Actor
{
	Default
	{
		Projectile;
		+BRIGHT
		+FLOORCLIP
		+THRUACTORS
		+NOGRAVITY
		+DONTBLAST
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.75;
		Scale 0.7;
	}

	States
	{
		Spawn:
			BXP1 A 0 NoDelay A_StartSound("powerup/itemcapsuleexplode", CHAN_AUTO, 0, 0.15);
			BXP1 AB 2;
			BSTC AA 0 A_SpawnItemEx("BEGenExplosionSmall", random(-8, 8), random(-4, 4), random(0, 14), flags: SXF_NOCHECKPOSITION);
			BXP1 CD 2;
			BSTC AA 0 A_SpawnItemEx("BEGenExplosionSmall", random(-12, 12), random(-10, 10), random(0, 10), flags: SXF_NOCHECKPOSITION);
			BXP1 EF 2;
			BSTC AA 0 A_SpawnItemEx("BEGenExplosionSmall", random(-6, 6), random(-15, 15), random(0, 12), flags: SXF_NOCHECKPOSITION);
			BXP1 G 2;
			Stop;
	}
}

class AmmoDogfighterMissile : Ammo
{
	override void DoEffect()
	{
		MaxAmount = default.MaxAmount + BEEquipmentBonus.GetTotalBonus(Owner, 'BonusSupportMagCap');
		Super.DoEffect();
	}

	Default
	{
		Inventory.Amount 36;
		Inventory.MaxAmount 36;
		Inventory.InterHubAmount 36;
	}
}
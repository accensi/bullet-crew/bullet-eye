class PlasmaSpidersWeapon : BEWeapon
{
	override void DrawHUDStuff(BulletEyeHUD hud, BEPlayer plr, Vector2 pos, int flags)
	{
		hud.DrawImage("WPNGAUGE", pos, flags);
		int SpiderCount = DeployedSpidersCount();
		for (int i = 0; i < SpiderCount; ++i)
		{
			hud.DrawImage("PLSPIDBR", hud.AddOffset(flags, pos, (46 - 8 * i, 1)), flags);
		}

		if (Owner.FindInventory('PlasmaSpidersRoughDuration'))
		{		
			hud.DrawBar("GRNHLHBR", "GRNHLHBB", plr.CountInv("PlasmaSpidersRoughDuration"), hud.GetMaxAmount("PlasmaSpidersRoughDuration"), hud.AddOffset(flags, pos, (6, -5)), 0, 0, flags);
		}		
	}
	
	override void DoEffect()
	{
		if (Owner)
		{
			if (Owner.CheckInventory("PlasmaSpidersRoughDuration", 1))
			{
				if (level.time % 35 == 0)
				{
					Owner.A_TakeInventory("PlasmaSpidersRoughDuration", 1);
				}
			}
			
			if (DeployedSpidersCount() == 0 && Owner.CheckInventory("PlasmaSpidersRoughDuration", 1) && level.time % 35 * 3 == 0)
			{
				Owner.A_TakeInventory("PlasmaSpidersRoughDuration", 0);
			}
		}

		Super.DoEffect();
	}	
	
	override void OnEnemyKilled(BEEnemy enemy, BEPlayer plr, Name mod)
	{
		Super.OnEnemyKilled(enemy, plr, mod);

		//Support weapon feature: Chance to drop health augments and tiny ammo pick ups on kills.
		if (plr.FindInventory('PlasmaSpidersWeapon'))
		{
			enemy.A_SpawnItemEx("HealthAugmentDropped", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 215 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
			enemy.A_SpawnItemEx("WeaponTinyAmmoPool", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION, failchance: 180 - (plr.CheckInventory("BeltReplenishmentCord", 1) * 30));
		}
	}

	private action void A_DeploySpider()
	{
		let plr = BEPlayer(invoker.Owner);
		Actor a; bool success;
		[success, a] = A_SpawnItemEx("PlasmaSpiderActive", 0, 0, 35, 15, 0, 6, 0, SXF_SETMASTER | SXF_TRANSFERPITCH | SXF_NOCHECKPOSITION);
		if (success)
		{
			int Size = plr.DeployedSpiders.Size();
			for (int i = 0; i < Size; ++i)
			{
				if (!plr.DeployedSpiders[i])
				{
					plr.DeployedSpiders[i] = PlasmaSpiderActive(a);
					return;
				}
			}

			plr.DeployedSpiders.Push(PlasmaSpiderActive(a));
		}
	}

	clearscope int DeployedSpidersCount()
	{
		let plr = BEPlayer(Owner);
		int Total = 0;
		for (int i = 0; i < plr.DeployedSpiders.Size(); ++i)
		{
			if (plr.DeployedSpiders[i])
			{
				Total++;
			}
		}
		return Total;
	}

	override string GetEffectText(int lvl)
	{
		switch (lvl)
		{
			case 1: return "Deploy up to 6 spider drones to assault enemies. Spiders pulse healing energy after every 10 successful hits. Pressing alt-fire on a target will cause all active spiders to focus on that Target. If using primary fire again at the maximum deploy count, the spiders will instead have their duration refreshed. The timer displayed provides a rough estimate on the duration of the first spider deployed.";
			case 2: return "Spiders now create barriers which block shots. Holding alt-fire will set the spiders to Guardian mode, halting their movement and increasing the size of the barriers.";
			case 3: return "When spiders pulse energy, they now leave behind webs which stun enemies. Active time of spiders is now increased.";
		}
		return Super.GetEffectText(lvl);
	}

	Default
	{
		Inventory.Icon "WEAP26";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoPlasmaSpiders";
		Weapon.AmmoType2 "AmmoSupportEnergy";
		Weapon.UpSound "beplayer/weapswitchplasmaspiders";
		BEWeapon.ReloadProperties "weapons/plasmaspidersreloadstage1", "weapons/plasmaspidersreloadstage2", "weapons/plasmaspidersreloadstage3";
		BEWeapon.Sprite "WEAPH26";
		BEWeapon.Type WTYPE_Support;
		BEWeapon.Element WElement_Energy;
		BEWeapon.Attributes 2, 2, 6, 4;
		BEWeapon.FlavorText "Although the firepower of these spider-like drones leave much to be desired, they can easily overwhelm enemies and provide much needed fire support. Each of their connecting shots charges energy cells within the spider, which in turn allows them to emit a radius of restorative ether energy. Experts can even deploy a few spiders and utilize another weapon in their arsenal while the spiders provide cover fire.";
		BEWeapon.CraftingCost 75;
		+WEAPON.ALT_AMMO_OPTIONAL
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.NOALERT
		+WEAPON.CHEATNOTWEAPON
		+BEWEAPON.NOMAGAZINE
		Tag "Plasma Spiders";
		BEWeapon.Tier 3;
		BEWeapon.TechnicalInfo WTIF_BURST;
		BEWeapon.DamagePerType 0.30; // [Mor] Higher boost due to the overall low power of Support weapons.
		BEWeapon.DynamicDamage 300, 0.20;
	}
	
	private int AltHoldCounter;

	States
	{
		Spawn:
			PPSP A 0;
			Goto Super::Spawn;
		Ready:
			_WPS A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Loop;
		DeselectReal:
			TNT1 A 0 A_Lower(32);
			_WPS A 1 A_Lower;
			Loop;
		SelectReal:
			TNT1 A 0 A_Raise(32);
			_WPS A 1 A_Raise;
			Loop;
		AltFire:
			_WPS A 0
			{
				if (invoker.DeployedSpidersCount() == 0 && CheckInventory("BonusPlasmaSpidersL1E", 1))
				{
					return ResolveState("ActualFireBonus");
				}

				return ResolveState(null);
			}
			_WPS A 1
			{
				FLineTraceData Data;
				LineTrace(angle, 2048 * 4, pitch, 0, Height / 2 + BEPlayer(self).AttackZOffset, 0, 0, Data);

				if (Data.HitActor is 'BEEnemy')
				{
					bool HasSpawnedIndiator;
					BlockThingsIterator it = BlockThingsIterator.Create(self, 2048);
					while (it.Next())
					{
						let Spider = PlasmaSpiderActive(it.thing);
						if (Spider && Spider.CheckSight(Data.HitActor) && Spider.HateTarget != Data.HitActor)
						{
							if (!HasSpawnedIndiator)
							{
								Actor a = Spawn("SpiderTargetIndicator", Data.HitActor.Pos + (0, 0, Data.HitActor.height + 8));
								a.Master = Data.HitActor;
								HasSpawnedIndiator = true;
							}
							Spider.HateTarget = BEEnemy(Data.HitActor);
							Spider.A_ClearTarget();
							Spider.SetStateLabel("SpawnFall");
						}
					}
					return ResolveState("Ready");
				}
				return ResolveState(null);
			}
		AltHold:
			_WPS A 1 A_JumpIf(A_GetWeaponLevel() < 2, 'Ready');
			_WPS A 0
			{
				let plr = BEPlayer(self);
				invoker.AltHoldCounter++;
				if (invoker.AltHoldCounter > 15)
				{
					invoker.AltHoldCounter = 0;
					for (int i = 0; i < plr.DeployedSpiders.Size(); ++i)
					{
						if (plr.DeployedSpiders[i] && !plr.DeployedSpiders[i].ReinforcePosition)
						{
							plr.DeployedSpiders[i].SetStateLabel('ReinforcePosition');
						}
					}
					return ResolveState("Ready");
				}

				return ResolveState(null);
			}
			_WPS A 0 A_Refire();
			_WPS A 0 { invoker.AltHoldCounter = 0; }
			Goto Ready;
		Fire:
			_WPS A 1
			{
				if (invoker.DeployedSpidersCount() == 6)
				{
					return ResolveState("FailThrow");
				}

				return ResolveState(null);
			}
			_WPS A 0 A_CheckWeaponFire(8, 'Ready');
			_WPS A 1 Offset(0, 35);
			_WPS A 1 Offset(0, 55);
			_WPS A 1 Offset(0, 75);
			_WPS A 1 Offset(0, 95);
			_WPS A 1 Offset(0, 115);
			_WPS A 1 Offset(0, 135);
		ActualFire:
			_WPS A 0 Bright A_WeaponReady(WRF_NOFIRE|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WPS A 0 A_StartSound("weapons/plasmaspidersthrow", 100, 0, 1);
			_WPS A 0
			{
				if (invoker.DeployedSpidersCount() == 0)
				{
					A_GiveInventory("PlasmaSpidersRoughDuration", 20);
				}
			}
			TNT1 A 3;			
			TNT1 A 0 
			{
				A_DeploySpider();
				A_TakeAmmo(8);
			}
			TNT1 A 8;
			_WPS A 1 Offset(0, 85);
			_WPS A 1 Offset(0, 65);
			_WPS A 1 Offset(0, 45);
			_WPS A 1 Offset(0, 32);
			_WPS A 2;
			_WPS A 0 A_ReFire;
			Goto Ready;
		ActualFireBonus:
			_WPS A 0 A_CheckWeaponFire(72, 'Ready');
			_WPS A 0 Bright A_WeaponReady(WRF_NOFIRE|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WPS A 0 A_StartSound("weapons/plasmaspidersthrow", 100, 0, 1);
			_WPS A 0
			{
				if (invoker.DeployedSpidersCount() == 0)
				{
					A_GiveInventory("PlasmaSpidersRoughDuration", 20);
				}
			}			
			TNT1 A 3;
			TNT1 A 0 
			{
				A_DeploySpider();
				A_DeploySpider();
				A_DeploySpider();
				A_DeploySpider();
				A_DeploySpider();
				A_DeploySpider();
				A_TakeAmmo(72);
			}
			TNT1 A 8;
			_WPS A 1 Offset(0, 85);
			_WPS A 1 Offset(0, 65);
			_WPS A 1 Offset(0, 45);
			_WPS A 1 Offset(0, 32);
			_WPS A 2;
			_WPS A 0 A_ReFire;
			Goto Ready;
		FailThrow: //This now resets the duration on spiders instead of just failing.
			_WPS A 0 A_CheckWeaponFire(8, 'Ready');
			_WPS A 0
			{
				if (CountInv("PlasmaSpidersRoughDuration") <= 15)
				{
					return ResolveState(null);
				}

				return ResolveState("Ready");
			}			
			_WPS A 0 A_StartSound("powerup/weaponenhanceruse", CHAN_AUTO);
			_WPS A 0 A_GiveToChildren("SpiderDurationReset", 1);
			_WPS A 0 A_TakeAmmo(8);
			_WPS A 0
			{
				if (invoker.DeployedSpidersCount() >= 6) //Should be impossible to get to this state without 6, but just in case.
				{
					A_GiveInventory("PlasmaSpidersRoughDuration", 20);
				}
			}
			_WPS A 5;
			Goto Ready;
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}

class PlasmaSpiderActive : Actor
{
	Default
	{
		Health 60;
		Radius 15;
		Height 15;
		Mass 50;
		Speed 15;
		Scale 1.0;
		Monster;
		+AVOIDMELEE
		+NOBLOOD
		+DONTGIB
		+DONTBLAST
		+DONTHARMSPECIES
		+THRUSPECIES
		+FRIENDLY
		+INVULNERABLE
		+NEVERTARGET
		+NOTAUTOAIMED
		+NODAMAGETHRUST
		+MISSILEMORE
		+MISSILEEVENMORE
		+LOOKALLAROUND
		+NOBLOCKMONST
		+ROLLSPRITE
		-SHOOTABLE
		Species "Player";
	}

	BEEnemy HateTarget;
	bool ReinforcePosition;
	private int Duration, Cooldown;

	States
	{
		Spawn:
			PLSP A 1 NoDelay A_SetRoll(roll+35);
			PLSP A 0 A_JumpIf(pos.Z - floorz <= 5, "SpawnSetup");
			Loop;
		ReinforcePosition:
			PLSP A 0
			{
				ReinforcePosition = true;
			}
		SpawnSetup:
			PLSP A 0
			{
				A_StartSound("weapons/plasmaspidersimpact");
				bROLLSPRITE = false;
			}
			PLSP B 0
			{
				let plr = BEPlayer(Master);
				if (plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 3)
				{
					Duration = 20; // [Ace] It's 20 because it normally goes into the negatives. Too lazy to initialize it and count to zero, so we count from zero downwards.
				}
			}
			PLSP B 0
			{
				let plr = BEPlayer(Master);
				if (plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 2)
				{
					// [Ace] Destroy the previous shield.
					if (Tracer)
					{
						Tracer.Destroy();
					}
					A_SpawnItemEx(ReinforcePosition ? "PlasmaSpiderBarrierL2" : "PlasmaSpiderBarrier", 0, 0, height, flags: SXF_SETMASTER | SXF_ORIGINATOR | SXF_ISTRACER);
				}
			}
			PLSP B 0
			{
				BEWeaponUpgrade.TransferUpgrades(Master, self);
			}
			PLSP B 0
			{
				let plr = BEPlayer(Master);
				if (plr.CheckInventory("BonusDamageSupport", 1))
				{
					A_GiveInventory("BonusDamageSupport", 1);
				}
			}
		SpawnFall:
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (FindInventory('SpiderDurationReset') && plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 3)
				{
					Duration = 20;
					A_TakeInventory("SpiderDurationReset");
				}
				else if (FindInventory('SpiderDurationReset'))
				{
					Duration = 0;
					A_TakeInventory("SpiderDurationReset");
				}
			}		
			PLSP B 0 A_JumpIf(!ReinforcePosition, 2);
			PLSP B 35 * 5;
			PLSP B 0
			{
				if (ReinforcePosition)
				{
					ReinforcePosition = false;
					SetStateLabel("SpawnSetup");
				}
			}
			PLSP BCDE 3 A_Wander;
			PLSP A 0 A_Look();
			PLSP B 0 A_StartSound("weapons/plasmaspidersmove", 102, CHANF_LOOPING | CHANF_OVERLAP, 0.2);
			PLSP B 0
			{
				if (HateTarget)
				{
					target = HateTarget;
					SetStateLabel("See");
				}
				else if (!Target)
				{
					A_Look();
				}
			}
			PLSP A 0 A_JumpIf(--Duration == -55, 'Death');
			Loop;
		See:
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (FindInventory('SpiderDurationReset') && plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 3)
				{
					Duration = 20;
					A_TakeInventory("SpiderDurationReset");
				}
				else if (FindInventory('SpiderDurationReset'))
				{
					Duration = 0;
					A_TakeInventory("SpiderDurationReset");
				}
			}
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (plr.CheckInventory("BonusPlasmaSpidersL1", 1) && CheckInventory("PlasmaSpidersHealingEnergy", 6))
				{
					SetStateLabel("HealPulseWait");
				}
			}
			PLSP A 0 A_JumpIfInventory("PlasmaSpidersHealingEnergy", 10, "HealPulseWait");
			PLSP B 0 A_StartSound("weapons/plasmaspidersmove", 102, CHANF_LOOPING | CHANF_OVERLAP, 0.2);
			PLSP BCDE 3 A_Chase();
			PLSP A 0 A_JumpIf(--Duration == -55, 'Death');
			Loop;
		Missile:
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (plr.CheckInventory("BonusPlasmaSpidersL1", 1) && CheckInventory("PlasmaSpidersHealingEnergy", 6))
				{
					SetStateLabel("HealPulseWait");
				}
			}
			PLSP A 0 A_JumpIfInventory("PlasmaSpidersHealingEnergy", 10, "HealPulseWait");
			PLSP B 2 A_FaceTarget;
			PLSP A 0 A_StartSound("weapons/plasmaspidersshoot");
			PLSP C 2
			{
				let plr = BEPlayer(Master);
				if (plr.CheckInventory("BonusPlasmaSpidersL1C", 1))
				{
					A_SpawnProjectile("PlasmaSpiderShotBurst", 25, flags: CMF_TRACKOWNER);
				}
				else
				{
					A_SpawnProjectile("PlasmaSpiderShot", 25, flags: CMF_TRACKOWNER);
				}
			}
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (plr.CheckInventory("BonusPlasmaSpidersL1C", 1))
				{
					SetStateLabel("MissileBonus");
				}
			}
			PLSP CDE 2;
			Goto See;
		MissileBonus:
			PLSP A 0 A_StartSound("weapons/plasmaspidersshoot");
			PLSP C 2 A_SpawnProjectile("PlasmaSpiderShotBurst", 25, flags: CMF_TRACKOWNER);
			PLSP A 0 A_StartSound("weapons/plasmaspidersshoot");
			PLSP C 2 A_SpawnProjectile("PlasmaSpiderShotBurst", 25, flags: CMF_TRACKOWNER);
			PLSP CDE 2;
			Goto See;
		HealPulseWait:
			PLSP A 0 A_StartSound("weapons/plasmaspidershealpulse");
			PLSP GHGHGHGHGHGH 3
			{
				let plr = BEPlayer(Master);
				if (CheckProximity((plr.CheckInventory("BonusPlasmaSpidersL1D", 1)) ? "BEEnemy" : "BEPlayer", (plr.CheckInventory("BonusPlasmaSpidersL1B", 1)) ? 500 : 300, 1, CPXF_ANCESTOR | CPXF_CHECKSIGHT) || --Cooldown == -50)
				{
					Cooldown = 0;
					SetStateLabel('HealPulse');
					return;
				}
			}
			Loop;
		HealPulse:
			PLSP B 0
			{
				let plr = BEPlayer(Master);
				if (plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 3)
				{
					A_SpawnItemEx("PlasmaSpiderWeb", 0, 0, 3, flags: SXF_NOCHECKPOSITION);
				}
			}
			PLSP A 0 
			{
				let plr = BEPlayer(Master);
				
				if (plr.CheckInventory("BonusPlasmaSpidersL1D", 1)) //Deals area damage instead with Venom Strike equipment bonus.
				{
					A_SpawnItemEx("PlasmaSpiderDamagePulseFX", flags: SXF_NOCHECKPOSITION);
					A_TakeInventory("PlasmaSpidersHealingEnergy", 10);
					A_StartSound("weapons/blazestreamfirenova");
					A_Explode(35, (plr.CheckInventory("BonusPlasmaSpidersL1B", 1)) ? 800 : 600, XF_EXPLICITDAMAGETYPE | XF_NOTMISSILE, 1, 128, 0, 0, null, "Riskbreaker");
					A_SetArg(1, 50);
				}
				else
				{
					A_SpawnItemEx("PlasmaSpiderHealPulseFX", flags: SXF_NOCHECKPOSITION); //Heal
					A_TakeInventory("PlasmaSpidersHealingEnergy", 10);
					A_StartSound("weapons/plasmaspidershealpulsesuccess");
					A_RadiusGive("PowerUpMediGemSmall", (plr.CheckInventory("BonusPlasmaSpidersL1B", 1)) ? 800 : 600, RGF_PLAYERS);
					A_SetArg(1, 50);
				}
			}
			Goto See;
		Pain:
			PLSP B 2;
			Goto See;
		Death:
			PLSP A 1
			{
				let plr = BEPlayer(Master);
				if (plr)
				{
					A_StopSound(102);
					A_SpawnItemEx("BEGenExplosionBig", 0, 0, 12, 0, 0, 0);
				}
			}
			PLSP A 0
			{
				let plr = BEPlayer(Master);
				if (plr.GetWeaponLevel("PlasmaSpidersWeapon") >= 3 && plr.CheckInventory("BonusPlasmaSpidersL3", 1))
				{
					A_SpawnItemEx("PlasmaSpiderWeb", 0, 0, 3, flags: SXF_NOCHECKPOSITION);
				}
			}
			Stop;
	}
}

class PlasmaSpiderShot : BEPlayerProjectile
{
	Default
	{
		Radius 3;
		Height 3;
		Speed 45;
		DamageFunction (30);
		RenderStyle "Add";
		Alpha 0.8;
		Scale 0.6;
		DamageType "PlasmaSpiders";
		Projectile;
		+DONTSPLASH
		+BRIGHT
		+THRUSPECIES
		+ROLLSPRITE
		Species "Player";
	}

	States
	{
		Spawn:
			PSDS A 1 NoDelay A_SetRoll(random(0, 359));
			TNT1 A 1;
			PSDS B 1 A_SetRoll(random(0, 359));
			Loop;
		Death:
			PSDS A 0 A_StartSound("weapons/plasmaspidersshotimpact", CHAN_AUTO, 0, 0.85);
		DeathFall:
			PSDS A 0 A_SetScale(scale.x + 0.2);
			PSDS B 1;
			PSDS A 0 A_FadeOut(0.1);
			Loop;
		XDeath:
			PSDS A 0 A_StartSound("weapons/plasmaspidersshotimpact", CHAN_AUTO, 0, 0.85);
			PSDS A 0 A_GiveInventory("PlasmaSpidersHealingEnergy", 1, AAPTR_TARGET);
			Goto DeathFall;
	}
}

class PlasmaSpiderShotBurst : PlasmaSpiderShot
{
	Default
	{
		DamageType "PlasmaSpidersBurst";
	}
}

class PlasmaSpiderHealPulseFX : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.4;
		Scale 0.4;
		+BRIGHT
		+FLATSPRITE
		+DONTBLAST
		+NOINTERACTION
		ReactionTime 14;
	}

	States
	{
		Spawn:
			PSDS C 1;
			PSDS C 0 A_SetScale(scale.x + 0.4);
			PSDS C 0
			{
				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			PSDS C 1 A_FadeOut(0.1);
			PSDS C 0 A_SetScale(scale.x + 0.1);
			Loop;
	}
}

class PlasmaSpiderDamagePulseFX : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.4;
		Scale 0.4;
		+BRIGHT
		+FLATSPRITE
		+DONTBLAST
		+NOINTERACTION
		ReactionTime 14;
	}

	States
	{
		Spawn:
			BSFV B 1;
			BSFV B 0 A_SetScale(scale.x + 0.4);
			BSFV B 0
			{
				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			BSFV B 1 A_FadeOut(0.1);
			BSFV B 0 A_SetScale(scale.x + 0.1);
			Loop;
	}
}

class PlasmaSpidersHealingEnergy : Inventory
{
	Default
	{
		Inventory.MaxAmount 10;
		Inventory.Icon "TNT1A0";
	}
}

class PlasmaSpiderBarrier : Actor
{
	override void PostBeginPlay()
	{
		A_SetSize(Radius * scale.x, Height * scale.y);

		Super.PostBeginPlay();
	}

	override void Tick()
	{
		if (!Master)
		{
			Destroy();
			return;
		}

		Super.Tick();
	}
	
	Default
	{
		Health 1;
		Radius 14;
		Height 60;
		+NOGRAVITY
		+SHOOTABLE
		+NOBLOOD
		+SOLID
		+DONTGIB
		+NOTAUTOAIMED
		+DONTTHRUST
		+THRUSPECIES
		+INVULNERABLE
		Species "Player";
		RenderStyle "Add";
		Alpha 0.45;
		Mass 10;
		Scale 0.6;
	}
	
	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_Warp(AAPTR_MASTER, 0, 0, Master.Height, flags: WARPF_NOCHECKPOSITION | WARPF_INTERPOLATE);
			PSDS D 1;
			TNT1 A 1;
			Loop;
		Death:
			PSDS D 1 A_FadeOut(0.1);
			Loop;
	}
}

class PlasmaSpiderBarrierL2 : PlasmaSpiderBarrier
{
	Default
	{
		Scale 1.0;
	}
}


class PlasmaSpiderWeb : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		RenderStyle "Add";
		Alpha 0.6;
		Scale 5.0;
		+BRIGHT
		+FLATSPRITE
		+DONTBLAST
		+THRUSPECIES
		Species "Player";
		DamageType "PlasmaSpidersWeb";
		ReactionTime 15;
	}

	States
	{
		Spawn:
			PSDS E 1;
			TNT1 A 0 A_Explode(1, 180, XF_NOSPLASH, 0, 180);
			TNT1 A 1;
			PSDS E 1;
			TNT1 A 0 A_Explode(1, 180, XF_NOSPLASH, 0, 180);
			TNT1 A 1;
			PSDS E 1;
			TNT1 A 0 A_Explode(1, 180, XF_NOSPLASH, 0, 180);
			TNT1 A 1;
		SpawnFall:
			PSDS E 1;
			TNT1 A 1;
			PSDS E 0 A_CountDown;
			Loop;
		Death:
			PSDS E 1 A_FadeOut(0.1);
			PSDS E 0 A_SetScale(scale.x + 0.1);
			Loop;
	}
}

class SpiderTargetIndicator : Actor
{
	override void Tick()
	{
		if (Master && Master.Health > 0)
	 	{
	 		Warp(Master, 0, 0, Master.height + 8, flags: WARPF_NOCHECKPOSITION | WARPF_INTERPOLATE);
	 	}
	 	else
	 	{
	 		Destroy();
	 		return;
	 	}

	 	Super.Tick();
	}

	Default
	{
		+NOINTERACTION
		RenderStyle "Translucent";
		Scale 2.0;
	}

	States
	{
		Spawn:
			 PLST GGGGGGGGGGGGG 1 NoDelay A_SetScale(scale.x - 0.1);
			 PLST GGGGGG 5 // [Ace] Has to be an even number of frames.
			 {
			 	alpha = alpha == 0 ? 1.0 : 0;
			 }
			 PLST G 1
			 {
			 	A_FadeOut(0.1);
			 }
			 Wait;
	}
}

class SpiderDurationReset : Inventory
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 1;
	}
}

class PlasmaSpidersRoughDuration : Inventory //Displays a rough estimate of the remaining time left on the FIRST spider spawned.
{
	Default
	{
		+INVENTORY.UNDROPPABLE
		Inventory.MaxAmount 20;
		Inventory.Icon "TNT1A0";
	}
}

class AmmoPlasmaSpiders : Ammo
{
	Default
	{
		Inventory.Amount 32;
		Inventory.MaxAmount 32;
		Inventory.InterHubAmount 32;
	}
}
class BEFoodItem : BEInventoryItem
{
	override void ActivateItem()
	{
		Heal();
		BEPlayer(Owner).Bonuses[BonusType].Insert(GetMapKey(), BonusAmount);
	}

	protected void Heal()
	{
		Owner.GiveBody(GetHealthAmount());

		let cls = GetRegenClass();
		if (cls)
		{
			Owner.A_GiveInventory(cls, 1);
			let regen = PowerRegeneration(Owner.FindInventory(cls));
			regen.EffectTics = regen.MaxEffectTics = TICRATE * RegenTime;
			regen.Strength = int(ceil(RegenAmount / double(RegenTime))); // [Ace] This will abort if you have a regen class with 0 regen time. Completely intentional because DON'T DO THAT.
		}
	}

	// [Ace] Synthesize the regen class's name from the item's name by establishing a convention, which is that [Item]Name becomes Name[Regen].
	// Saves us the effort of having to link the classes to the items manually via virtuals or properties.
	clearscope class<PowerRegeneration> GetRegenClass() const
	{
		string regenName = GetClassName();
		regenName.Replace("Item", "");
		regenName = regenName.."Regen";
		return (class<PowerRegeneration>)(regenName);
	}

	protected virtual clearscope int GetHealthAmount() const
	{
		int amt = HealthAmount;
		if (Owner.FindInventory('HelmProteinAnalyzer'))
		{
			double fac = BECore.GetHealthPercentage(Owner) <= 20 ? 0.5 : 0.3;
			amt += int(amt * fac);
		}
		return amt;
	}

	// [Ace] No modifications by default.
	protected virtual clearscope int, int GetRegenAmount()
	{
		return RegenAmount, RegenTime;
	}

	// [Ace] The name of the item will be used for the player's Bonuses dictionaries to permanently increase max health only once per item.
	protected clearscope string GetMapKey() const
	{
		// [Ace] MakeLower usage is because using Name directly with associative maps seems to be prone to issues between play sessions, likely due to how Name works.
		// Meanwhile strings are case-sensitive.
		string clsName = GetClassName();
		return clsName.MakeLower();
	}

	protected void SetFoodTag(BEPlayer plr)
	{
		if (!plr)
		{
			return;
		}

		int healthAmt = GetHealthAmount();
		let [regAmt, regTime] = GetRegenAmount();

		// [Ace] Tag strings are generated at runtime when you pick up the items. Until then they will only show their short names.
		// If regeneration is 0, it will only tell how much health it restores.
		string regStr = "";
		string totalStr = "";

		if (regAmt > 0 && regTime > 0)
		{
			regStr = String.Format(" and an additional %i \c[CottonCandy]health\cd over %i seconds", regAmt, regTime);
			totalStr = String.Format(" (%i \c[CottonCandy]HP\cd)", healthAmt + regAmt);
		}

		string tag = String.Format("\ct%s\n\cdUse: Restores %i \c[CottonCandy]health\cd%s.%s", default.GetTag(), healthAmt, regStr, totalStr);

		if (!plr.Bonuses[BonusType].CheckKey(GetMapKey())) // [Ace] Check if bonus hasn't been given before.
		{
			string type = "";
			switch (BonusType)
			{
				case BEPlayer.SBonus_Health: type = "health"; break;
				case BEPlayer.SBonus_Luck: type = "luck"; break;
			}

			//tag = String.Format("%s \c[Cyan]Max %s +%i\c-", tag, type, BonusAmount);
			tag = String.Format("%s \n\c[Purple]First time eating bonus: \c[Cyan]Max %s +%i\c-", tag, type, BonusAmount);
		}

		SetTag(tag);
	}

	override void DoEffect()
	{
		Super.DoEffect();

		// [Ace] Update periodically (currently 30 seconds) to reflect the real numbers depending on owned equipment items and such.
		// There is no point in updating on every tic because the player just isn't going to see it.
		// Using GetAge instead of Level.time to prevent potential framerate stuttering if a lot of items get updated on the same tic when playing on absolute shitcans.
		if (GetAge() % 1050 == 0)
		{
			SetFoodTag(BEPlayer(Owner));
		}
	}

	override bool TryPickup(in out Actor toucher)
	{
		bool result = Super.TryPickup(toucher);
		if (result)
		{
			SetFoodTag(BEPlayer(toucher));
		}
		return result;
	}

	meta int BonusType, BonusAmount;
	property Bonus: BonusType, BonusAmount;

	meta int HealthAmount, RegenAmount, RegenTime;
	property Healing: HealthAmount, RegenAmount, RegenTime;
}

// [Mor] Restores 5 health and an additional 15 health over 15 seconds. (20 HP)
class ItemJuicyRoast : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 10;
		BEInventoryItem.SellPrice 40;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 2;
		BEFoodItem.Healing 5, 15, 15;
		Inventory.PickupMessage "\ctItem - Juicy Roast";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM003";
		Tag "Juicy Roast";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I003 A -1;
			Stop;
	}
}

class JuicyRoastRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -15;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 3 health and an additional 6 health over 3 seconds. (9 HP)
class ItemTastyCheese : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 15;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 1;
		BEFoodItem.Healing 3, 6, 3;
		Inventory.PickupMessage "\ctItem - Tasty Cheese";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM008";
		Tag "Tasty Cheese";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I008 A -1;
			Stop;
	}
}

class TastyCheeseRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -3;
		Powerup.Strength 2;
	}
}

// [Mor] Restores 4 health and an additional 10 health over 10 seconds. (14 HP)
class ItemFriedBounderDrumstick : BEFoodItem
{
	override int GetHealthAmount()
	{
		return Super.GetHealthAmount() + (Owner.FindInventory("LegsHeavyPlateDreadseekers", 0) ? 3 : 0);
	}

	Default
	{
		BEInventoryItem.Cooldown 10;
		BEInventoryItem.SellPrice 30;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 1;
		BEFoodItem.Healing 4, 10, 10;
		Inventory.PickupMessage "\ctItem - Fried Bounder Drumstick";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM011";
		Tag "Fried Bounder Drumstick";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I011 A -1;
			Stop;
	}
}

class FriedBounderDrumstickRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -10;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 5 health and an additional 12 health over 10 seconds. (17 HP)
class ItemClassicBurger : BEFoodItem
{	
	Default
	{
		BEInventoryItem.Cooldown 10;
		BEInventoryItem.SellPrice 35;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 2;
		BEFoodItem.Healing 5, 12, 12;
		Inventory.PickupMessage "\ctItem - Classic Burger";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM020";
		Tag "Classic Burger";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I020 A -1;
			Stop;
	}
}

class ClassicBurgerRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -12;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 8 health and an additional 17 health over 15 seconds. (25 HP)
// [Ace] It can't restore it over 15 seconds because fractional health isn't a thing. It's 17 seconds.
class ItemBeefCurry : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 10;
		BEInventoryItem.SellPrice 45;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 3;
		BEFoodItem.Healing 8, 17, 17;
		Inventory.PickupMessage "\ctItem - Beef Curry";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM021";
		Tag "Beef Curry";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I021 A -1;
			Stop;
	}
}

class BeefCurryRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -17;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 5 health.
class ItemFieldRation : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 6;
		BEInventoryItem.SellPrice 15;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 1;
		BEFoodItem.Healing 5, 0, 0;
		Inventory.PickupMessage "\ctItem - Field Ration";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM044";
		Tag "Field Ration";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I044 A -1;
			Stop;
	}
}

// [Mor] Restores 8 health.
class ItemWaveCola : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 6;
		BEInventoryItem.SellPrice 40;
		BEFoodItem.Bonus BEPlayer.SBonus_Luck, 1;
		BEFoodItem.Healing 8, 0, 0; 
		Inventory.PickupMessage "\ctItem - Wave Cola";
		Inventory.UseSound "inventory/inventoryitemdrink";
		Inventory.PickUpSound "inventory/inventoryitemgetpotionmedium";
		Inventory.Icon "ITEM047";
		Tag "Wave Cola";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I047 A -1;
			Stop;
	}
}

// [Mor] Restores 4 health and an additional 8 health over 8 seconds. (12 HP)
class ItemThornTreeApple : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 35;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 1;
		BEFoodItem.Healing 4, 8, 8;
		Inventory.PickupMessage "\ctItem - Thorn Tree Apple";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM048";
		Tag "Thorn Tree Apple";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I048 A -1;
			Stop;
	}
}

class ThornTreeAppleRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -4;
		Powerup.Strength 2;
	}
}

// [Mor] Restores 5 health and an additional 13 health over 13 seconds. (18 HP)
class ItemBBQRibs : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 45;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 2;
		BEFoodItem.Healing 5, 13, 13;
		Inventory.PickupMessage "\ctItem - BBQ Ribs";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM049";
		Tag "BBQ Ribs";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I049 A -1;
			Stop;
	}
}

class BBQRibsRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -13;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 6 health and an additional 10 health over 10 seconds. (16 HP)
class ItemButteredRice : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 45;
		BEFoodItem.Bonus BEPlayer.SBonus_Health, 2;
		BEFoodItem.Healing 6, 10, 10;
		Inventory.PickupMessage "\ctItem - Buttered Rice";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM050";
		Tag "Buttered Rice";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I050 A -1;
			Stop;
	}
}

class ButteredRiceRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -10;
		Powerup.Strength 1;
	}
}

// [Mor] Restores 6 health.
class ItemStarDrops : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 125;
		BEFoodItem.Bonus BEPlayer.SBonus_Luck, 3;
		BEFoodItem.Healing 6, 0, 0;
		Inventory.PickupMessage "\ctItem - Star Drops";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM051";
		Tag "Star Drops";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I051 A -1;
			Stop;
	}
}

// [Mor] Restores 6 health and an additional 6 health over 3 seconds. (12 HP)
class ItemStrawberryShortcake : BEFoodItem //Sprite from Castlevania: Symphony of the Night (Konami)
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 125;
		BEFoodItem.Bonus BEPlayer.SBonus_Luck, 2;
		BEFoodItem.Healing 6, 6, 3;
		Inventory.PickupMessage "\ctItem - Strawberry Shortcake";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM061";
		Tag "Strawberry Shortcake";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I061 A -1;
			Stop;
	}
}

class StrawberryShortcakeRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -3;
		Powerup.Strength 2;
	}
}

// [Mor] Restores 8 health and an additional 6 health over 3 seconds. (14 HP)
class ItemTiramisu : BEFoodItem
{
	Default
	{
		BEInventoryItem.Cooldown 5;
		BEInventoryItem.SellPrice 125;
		BEFoodItem.Bonus BEPlayer.SBonus_Luck, 2;
		BEFoodItem.Healing 8, 6, 3;
		Inventory.PickupMessage "\ctItem - Tiramisu";
		Inventory.UseSound "inventory/inventoryitemfood";
		Inventory.PickUpSound "inventory/inventoryitemgetfood";
		Inventory.Icon "ITEM062";
		Tag "Tiramisu";
		-BEINVENTORYITEM.UNLIMITED
	}
	
	States
	{
		Spawn:
			I062 A -1;
			Stop;
	}
}

class TiramisuRegen : PowerRegeneration
{
	Default
	{
		Powerup.Duration -3;
		Powerup.Strength 2;
	}
}
//-------------------------------
// CORE
//-------------------------------

class BEArmor : BasicArmor
{
	override bool HandlePickup(Inventory item)
	{
		return item.GetClass() == 'BEArmor';
	}

	override void AbsorbDamage(int damage, Name damageType, out int newDamage, Actor inflictor, Actor source, int flags)
	{
		if (damage > 0 && Amount > 0 && (!inflictor || !inflictor.bPIERCEARMOR))
		{
			newDamage = 0;
			Amount += Owner.FindInventory('LegsGreavesOfWildShielding') ? randompick(-2, -2, -1, -1, -1, -1, -1, 1, 2, 3) : -1 - damage / (Owner.FindInventory('GlovesHyperWrist') ? 1 : 3); //damage / 7
			

			let plr = BEPlayer(Owner);
			plr.A_SetBlend("Blue", 0.5, 10);
			plr.A_GiveInventory("ShieldFlicker"..random(1, 2));
			plr.A_StartSound("powerup/energyscreendamagepool", CHAN_AUTO, 0, 1);
			plr.MakeInvulnerable((plr.CheckInventory("PowerStrikeOrbDefenseProt", 0) || plr.CheckInventory("PowerStrikeOrbDefenseProtX", 0) ? 45 : 35) + BEEquipmentBonus.GetTotalBonus(plr, 'BonusUnstoppable'));

			if ((plr.FindInventory('ArmorStrikerMail')) && plr.Player.ReadyWeapon)
			{
				BEWeapon(plr.Player.ReadyWeapon).GiveWeaponAmmo(BEWeapon.GWAF_FORCE | BEWeapon.GWAF_NORESERVE | BEWeapon.GWAF_PARTIAL);
			}
			
			Amount = clamp(Amount, 0, MaxAmount);
			
			// [Ace] Armor is depleted.
			if (Amount == 0)
			{
				let ShockwaveLegs = LegsShockForceLegArmor(plr.FindInventory('LegsShockForceLegArmor'));
				if (ShockwaveLegs)
				{
					ShockwaveLegs.DoShockwave();
				}
				
				plr.A_GiveInventory("ShieldGate", 1);
			}
		}
	}
}

class EnergyScreenBonus : BasicArmorBonus
{
	override bool Use(bool pickup)
	{
		let armor = BEArmor(Owner.FindInventory("BEArmor"));
		bool result = false;

		// [GZD] This should really never happen but let's be prepared for a broken inventory.
		if (armor == null)
		{
			armor = BEArmor(Spawn("BEArmor"));
			armor.BecomeItem();
			armor.Amount = 0;
			armor.MaxAmount = MaxSaveAmount;
			Owner.AddInventory(armor);
		}

		if (BonusCount > 0 && armor.BonusCount < BonusMax)
		{
			armor.BonusCount = min(armor.BonusCount + BonusCount, BonusMax);
			result = true;
		}

		int saveAmount = min(GetSaveAmount(), MaxSaveAmount);

		if (saveAmount <= 0)
		{
			// [GZD] If it can't give you anything, it's as good as used.
			return BonusCount > 0 ? result : true;
		}

		// [GZD] If you already have more armor than this item can give you, you can't use it.
		if (armor.Amount >= MaxSaveAmount + armor.BonusCount)
		{
			return result;
		}

		armor.Amount = min(armor.Amount + saveAmount, MaxSaveAmount + armor.BonusCount);
		armor.MaxAmount = max(armor.MaxAmount, MaxSaveAmount);
		return true;
	}


	Default
	{
		Armor.SavePercent 100;
		Armor.MaxSaveAmount 30; //6
	}
}

//-------------------------------
// ENERGY SCREEN
//-------------------------------

class EnergyScreen : CustomInventory replaces BlueArmor
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreen";
		Inventory.PickupMessage "Energy Screen - [+25 Shield Layers]"; //8
		Radius 20;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESCR A 1 Bright
			{
				if (level.total_monsters > 0 && level.killed_monsters == level.total_monsters)
				{
					SetStateLabel("Convert");
				}
			}			
			Loop;
		Convert:
			TNT1 A 0 A_SpawnItemEx("CrystallizedShield");
			Stop;
		Pickup:
			ESCR A 0 A_JumpIfInventory("BEArmor", 30, "FailPickup");
			ESCR A 0 A_GiveInventory(CheckInventory("HelmForgedHelmet", 0) ? "EnergyScreenShieldsX" : "EnergyScreenShields");
			Stop;
		FailPickup:
			ESCR A 0;
			Fail;
	}
}

class EnergyScreenShields : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 25; //5
	}
} 

class EnergyScreenShieldsX : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 30; //6
	}
}

//-------------------------------
// PARTIAL ENERGY SCREEN
//-------------------------------

class PartialEnergyScreen : CustomInventory replaces GreenArmor
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreen";
		Inventory.PickupMessage "Partial Energy Screen - [+15 Shield Layers]"; //3
		Radius 20;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESCR B 1 Bright
			{
				if (level.total_monsters > 0 && level.killed_monsters == level.total_monsters)
				{
					SetStateLabel("Convert");
				}
			}			
			Loop;
		Convert:
			TNT1 A 0 A_SpawnItemEx("CrystallizedShield");
			Stop;
		Pickup:
			ESCR A 0 A_JumpIfInventory("BEArmor", 30, "FailPickup");
			ESCR A 0 A_GiveInventory(CheckInventory("HelmForgedHelmet", 0) ? "PartialEnergyScreenShieldsX" : "PartialEnergyScreenShields");
			Stop;
		FailPickup:
			ESCR A 0;
			Fail;
	}
}

class PartialEnergyScreenShields : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 15; //3
	}
} 

class PartialEnergyScreenShieldsX : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 20; //4
	}
}

//-------------------------------
// SHIELD CAPSULES
//-------------------------------

class ShieldCapsuleS : CustomInventory
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard S - [+1 Energy Screen Shard]";
		Radius 15;
		ReactionTime 55;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRS AB 3 Bright;
			ESRS A 0
			{
				if (--ReactionTime <= 0)
				{
					ReactionTime = 16;
					SetStateLabel("FadingOut");
				}
			}
			Loop;
		Pickup:
			ESCR A 0 A_JumpIfInventory("BEArmor", 30, "CheckShards");
			ESRS A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 2 : 1);
			Stop;
		FailPickup:
			ESRS A 0;
			Fail;
		CheckShards:
			ESCR A 0 A_JumpIfInventory("ESShard", 9, "FailPickup");
			ESRS A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 2 : 1);
			Stop;
		FadingOut:
			ESRS A 1;
			TNT1 A 1;
			ESRS A 0
			{
				if (-- ReactionTime <= 0)
				{
					Destroy();
					return;
				}
			}
			Loop;
	}
}

class ShieldCapsuleSNoDrop : ShieldCapsuleS //Does not disappear. Spawns on Armor Bonus map spots.
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard S - [+1 Energy Screen Shard]";
		Radius 15;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRS AB 3 Bright;
			Loop;
		Pickup:
			ESCR A 0 A_JumpIfInventory("BEArmor", 30, "CheckShards");
			ESRS A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 2 : 1);
			Stop;
		FailPickup:
			ESRS A 0;
			Fail;
		CheckShards:
			ESCR A 0 A_JumpIfInventory("ESShard", 9, "FailPickup");
			ESRS A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 2 : 1);
			Stop;
	}
}

class ShieldCapsuleL : CustomInventory
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard L - [+3 Energy Screen Shards]";
		Radius 15;
		ReactionTime 55;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRL AB 3 Bright;
			ESRL A 0
			{
				if (--ReactionTime <= 0)
				{
					ReactionTime = 16;
					SetStateLabel("FadingOut");
				}
			}
			Loop;
		Pickup:
			ESRL A 0 A_JumpIfInventory("BEArmor", 30, "CheckShards");
			ESRL A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 4 : 3);
			Stop;
		FailPickup:
			ESRL A 0;
			Fail;
		CheckShards:
			ESRL A 0 A_JumpIfInventory("ESShard", 9, "FailPickup");
			ESRL A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 4 : 3);
			Stop;
		FadingOut:
			ESRL A 1;
			TNT1 A 1;
			ESRL A 0
			{
				if (-- ReactionTime <= 0)
				{
					Destroy();
					return;
				}
			}
			Loop;
	}
}

class ShieldCapsuleLNoDrop : ShieldCapsuleL
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard L - [+3 Energy Screen Shards]";
		Radius 15;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRL AB 3 Bright;
			Loop;
		Pickup:
			ESRL A 0 A_JumpIfInventory("BEArmor", 30, "CheckShards");
			ESRL A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 4 : 3);
			Stop;
		FailPickup:
			ESRL A 0;
			Fail;
		CheckShards:
			ESRL A 0 A_JumpIfInventory("ESShard", 9, "FailPickup");
			ESRL A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 4 : 3);
			Stop;
	}
}

class ShieldCapsuleXL : CustomInventory
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard XL - [+6 Energy Screen Shards]";
		Radius 15;
		ReactionTime 55;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRX AB 3 Bright;
			ESRX A 0
			{
				if (--ReactionTime <= 0)
				{
					ReactionTime = 16;
					SetStateLabel("FadingOut");
				}
			}
			Loop;
		Pickup:
			ESRX A 0 A_JumpIfInventory("BEArmor", 30, "CheckShards");
			ESRX A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 7 : 6);
			Stop;
		FailPickup:
			ESRX A 0;
			Fail;
		CheckShards:
			ESRX A 0 A_JumpIfInventory("ESShard", 9, "FailPickup");
			ESRX A 0 A_GiveInventory("ESShard", CheckInventory("HelmForgedHelmet", 0) ? 7 : 6);
			Stop;
		FadingOut:
			ESRX A 1;
			TNT1 A 1;
			ESRX A 0
			{
				if (-- ReactionTime <= 0)
				{
					Destroy();
					return;
				}
			}
			Loop;
	}
}

class EnergyShellArmorShield : CustomInventory
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Shard L - [+2 Shields]";
		Radius 15;
		ReactionTime 55;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRL AB 3 Bright;
			ESRL A 0
			{
				if (--ReactionTime <= 0)
				{
					ReactionTime = 16;
					SetStateLabel("FadingOut");
				}
			}
			Loop;
		Pickup:
			ESRL A 0 A_GiveInventory(CheckInventory("HelmForgedHelmet", 0) ? "EnergyShellShieldL" : "EnergyShellShieldS");
			Stop;
		FailPickup:
			ESRL A 0;
			Fail;
		FadingOut:
			ESRL A 1;
			TNT1 A 1;
			ESRL A 0
			{
				if (-- ReactionTime <= 0)
				{
					Destroy();
					return;
				}
			}
			Loop;
	}
}

class EnergyShellShieldS : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 2;
	}
}

class EnergyShellShieldL : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 3;
	}
}

class ShieldShardComplete : CustomInventory
{
	Default
	{
		Inventory.PickupSound "powerup/energyscreenrepair";
		Inventory.PickupMessage "Shield Charge - [+1 Shield]";
		Radius 15;
		ReactionTime 55;
		+DONTGIB
	}

	States
	{
		Spawn:
			ESRS AB 3 Bright;
			ESRS A 0
			{
				if (--ReactionTime <= 0)
				{
					ReactionTime = 16;
					SetStateLabel("FadingOut");
				}
			}
			Loop;
		Pickup:
			ESRS A 0 A_GiveInventory("ShieldShardCompleteDone");
			Stop;
		FailPickup:
			ESRS A 0;
			Fail;
		FadingOut:
			ESRS A 1;
			TNT1 A 1;
			ESRS A 0
			{
				if (-- ReactionTime <= 0)
				{
					Destroy();
					return;
				}
			}
			Loop;
	}
}

class ShieldShardCompleteDone : EnergyScreenBonus
{
	Default
	{
		Armor.SaveAmount 1;
	}
}

class ESShard : Inventory //Energy Screen Shard
{
	Default
	{
		Inventory.MaxAmount 20; //Count should "reset" at 10, but this is 20 to make spillover easier.
	}
}

class CrystallizedShield : Health //Sprite from "Bust-a-Move" - Taito
{								//This appears in place of Energy Screen items when all enemies in the map have been defeated.
	mixin Magnetism;

	override void Touch(Actor toucher)
	{
		let plr = BEPlayer(toucher);
		if (plr)
		{
			plr.GiveInventory("PartialEnergyScreen", 1);
			plr.GiveInventory("ESShard", 5);
			plr.Score += 2000;
		}

		Super.Touch(toucher);
	}

	Default
	{
		Inventory.Amount 5;
		Inventory.PickupMessage "Crystallized Shield [+3 Energy Screen Layers and +5 Energy Screen Shards] + 2000 Score";
		Inventory.PickupSound "pickup/crystallizedget";
		-SHOOTABLE
		+DONTGIB
		+INVENTORY.ALWAYSPICKUP
		Scale 0.4;
		Radius 20;
	}

	States
	{
		Spawn:
			_CSP B 0 NoDelay
			{
				if (bulleteye_crystallizedmarkers)
				{
					A_SpawnItemEx("CrystallizedShieldMarker", flags: SXF_SETMASTER);
				}
			}
		SpawnTrue:
			_CSP B 1;
			_CSP B 0
			{
				if (bulleteye_supercrystalmagnet)
				{
					SetStateLabel("Convert");
				}
			}
			_CSP B 1 Bright;
			Loop;
		Convert:
			_CSP A 0 A_SpawnItemEx("CrystallizedShieldSuperM");
			Stop;
	}
}

class CrystallizedShieldSuperM : Health //This appears in place of Energy Screen items when all enemies in the map have been defeated. Super magnetized.
{								
	mixin SuperMagnetism;

	override void Touch(Actor toucher)
	{
		let plr = BEPlayer(toucher);
		if (plr)
		{
			plr.GiveInventory("PartialEnergyScreen", 1);
			plr.GiveInventory("ESShard", 5);
			plr.Score += 2000;
		}

		Super.Touch(toucher);
	}

	Default
	{
		Inventory.Amount 5;
		Inventory.PickupMessage "Crystallized Shield [+3 Energy Screen Layers and +5 Energy Screen Shards] + 2000 Score";
		Inventory.PickupSound "pickup/crystallizedget";
		-SHOOTABLE
		+DONTGIB
		+INVENTORY.ALWAYSPICKUP
		+BRIGHT
		Scale 0.4;
		Radius 20;
	}

	States
	{
		Spawn:
			_CSP B 0 NoDelay
			{
				if (bulleteye_crystallizedmarkers)
				{
					A_SpawnItemEx("CrystallizedShieldMarker", flags: SXF_SETMASTER);
				}
			}
		SpawnTrue:
			_CSP B 1;
			_CSP B 0
			{
				if (!bulleteye_supercrystalmagnet)
				{
					SetStateLabel("Convert");
				}
			}
			Loop;
		Convert:
			_CSP A 0 A_SpawnItemEx("CrystallizedShield");
			Stop;
	}
}

class CrystallizedShieldMarker : MapMarker //Sprite from "Bust-a-Move" - Taito
{
	Default
	{
		+NOINTERACTION
		+INVISIBLE
		Scale 0.5;
		Args 0, 1;
	}

	States
	{
		Spawn:
			CRSD A 2
			{
				if (!Master)
				{
					Destroy();
				}
			}
			Loop;
	}
}

class ShieldGate : PowerInvulnerable //Brief invulnerabilty when shields deplete.
{
	Default
	{
		Powerup.Duration -2;
		Inventory.Icon "";
	}
}
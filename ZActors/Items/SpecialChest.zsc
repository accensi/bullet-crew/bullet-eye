class SpecialChest : Actor //These were originally supposed to be super secret chests you could find by chance in a map, but there were complaints they were hard to find, so a sound has been added when they spawn.
{
	static void TrySpawn(Actor other)
	{
		if (random(1, 100) <= 2) // 2% chance.
		{
			for (int i = 0; i < 300; ++i) // [Ace] Not 'while' so it doesn't hang up the game if it ends up being an infinite loop.
			{
				Vector3 spawnPos = (other.Pos.X - random(-512, 512), other.Pos.Y - random(-512, 512), other.Pos.Z);
				if (level.IsPointInLevel(spawnPos)) 
				{
					Actor.Spawn("SpecialChest", spawnPos);
					break;
				}
			}
		}
	}

	override void BeginPlay()
	{
		bCOUNTSECRET = bulleteye_counthiddenobjects;

		Super.BeginPlay();
	}

	override bool OnGiveSecret(bool printmsg, bool playsound)
	{
		if (printmsg)
		{
			A_Print("You discovered a hidden cache!");
		}

		return false; // [Ace] Just in case, although it doesn't play the default sound or show the default message regardless of this.
	}

	private bool SoundPlayed;
	private bool AppearanceFX;

	Default
	{
		Health 300;
		WoundHealth 80;
		Radius 15;
		Height 55;
		FloatBobStrength 0.2;
		PainChance 256;
		PainChance "SpecialChestDamage", 0;
		DamageFactor "QuickMelee", 15.0;
		DamageFactor "TheRadiatorDamage", 0;
		+SHOOTABLE
		+NOBLOOD
		-SOLID
		+DONTGIB
		+NOTAUTOAIMED
		+DONTTHRUST
		+NORADIUSDMG
		+FLOATBOB
	}

	States
	{
		Spawn:
			SBCH A 0 NoDelay
			{
				if (!AppearanceFX)
				{
					//A_SpawnItemEx("SpecialChestAppearanceFX", 0, 0, 5);
					A_StartSound("powerup/secretchestspawn", CHAN_AUTO, 0, 0.8, ATTN_NONE);
					AppearanceFX = true;
				}
			}
		SpawnLoop:
			TNT1 A 10; //A_SetTranslucent(1.0);
			TNT1 A 0 A_StartSound("intermission/tick", CHAN_AUTO, 0, 1, 5);
			TNT1 A 0 A_CheckProximity("Appear", "BEPlayer", 140, 1, CPXF_ANCESTOR);
			TNT1 A 0
			{
				if (BECore.CountPlayersInventory("PowerRevealer", 1))
				{
					A_SpawnItemEx("RevealerSPChestFlash", 0, 0, height / 2, flags: SXF_NOCHECKPOSITION);
				}
			}
			Loop;
		Appear:
			SBCH A 10 A_SetTranslucent(0.12);
			TNT1 A 0 A_StartSound("intermission/tick", CHAN_AUTO, 0, 1, 5);
			TNT1 A 0 A_CheckProximity("Spawn", "BEPlayer", 200, 0, CPXF_ANCESTOR | CPXF_EXACT);
			Loop;
			/*
		AppearRevealer:
			SBCH A 10 A_SetTranslucent(0.45);
			TNT1 A 0 A_StartSound("intermission/tick", CHAN_AUTO, 0, 1, 5);
			Loop;
			*/
		Pain:
			SBCH A 0 A_SetTranslucent(1.0);
			SBCH A 0 A_StartSound("powerup/secretchesthit", CHAN_AUTO, 0, 1, ATTN_NONE);
			SBCH ABC 1;
			/*
			TNT1 A 0
			{
				if (BECore.CountPlayersInventory("PowerRevealer", 1))
				{
					SetStateLabel("AppearRevealer");
				}
			}
			*/
			Goto Spawn;
		Wound:
			SBCH A 0
			{
				if (!SoundPlayed)
				{
					A_SetTranslucent(1.0);
					A_StartSound("powerup/secretchestappear", CHAN_AUTO, 0, 1, ATTN_NONE);
					SoundPlayed = true;
				}
			}
			SBCH DEFE 5;
			Loop;
		Death:
			SBCH A 0
			{
				if (!SoundPlayed) //This is here so that it will still play the secret tune when opened instantly with Quick Kick.
				{
					A_StartSound("powerup/secretchestappear", CHAN_AUTO, 0, 1, ATTN_NONE);
					SoundPlayed = true; //Just in case.
				}
			}
			TNT1 A 0
			{
				let plr = BEPlayer(Target);
				if (plr && plr.CheckInventory("LegsDemolishersLegPlates", 0))
				{
					A_GiveToTarget("PowerDemolisherDamage", 1);
				}
				BEGlobalStats.Get().TreasureLevel += 2;
			}
			
			ICPS A 0 A_SpawnItemEx("BEGenExplosionBig", 0, 0, 12, 0, 0, 0);
			SBCH A 0 A_SpawnItemEx("BasicGearPool", 0, 0, 6, random(4, 8), random(4, 8), random(6, 12), random(0, 359), SXF_NOCHECKPOSITION);
			SBCH AAAAAAAAAAAAAAAAAAAA 0 A_SpawnItemEx("ScorePoolLarge", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION);
			SBCH A 0 A_SpawnItemEx(BECollectible.PickRandomCollectible(BECollectible.CType_Standard), 0, 0, 6, random(4, 8), random(4, 8), random(6, 12), random(0, 359), SXF_NOCHECKPOSITION);
			SBCH A 0
			{
				if (bulleteye_counthiddenobjects)
				{
					GiveSecret();
				}
			}
			Stop;
	}
}

class SpecialChestSuper : Actor //The original intent and iteration of these chests. No visual/audio hints given for this one!
{
	private bool SoundPlayed;
	private bool AppearanceFX;

	Default
	{
		Health 300;
		WoundHealth 80;
		Radius 15;
		Height 55;
		FloatBobStrength 0.2;
		PainChance 256;
		PainChance "SpecialChestDamage", 0;
		DamageFactor "QuickMelee", 15.0;
		DamageFactor "TheRadiatorDamage", 0;
		+SHOOTABLE
		+NOBLOOD
		+NOGRAVITY
		-SOLID
		+DONTGIB
		+NOTAUTOAIMED
		+DONTTHRUST
		+NORADIUSDMG
		+FLOATBOB
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				if (!AppearanceFX)
				{
					AppearanceFX = true;
					bFLOORCLIP = true;
				}
			}
		SpawnSetup:
			TNT1 A 35;
			TNT1 A 0 { bFLOORCLIP = false; }
			TNT1 AA 2 A_ChangeVelocity(frandompick(-20.0, 20.0), frandompick(-20.0, 20.0), randompick(8, 15), CVF_RELATIVE);
			TNT1 A 0 A_Stop;
		SpawnLoop:
			TNT1 A 1;
			TNT1 A 0
			{
				if (BECore.CountPlayersInventory("PowerRevealer", 1))
				{
					A_SpawnItemEx("RevealerSPChestFlash", 0, 0, height / 2, flags: SXF_NOCHECKPOSITION);
				}
			}
			Loop;
		Appear:
			S2CH A 1 A_SetTranslucent(0.35);
			Loop;
		Pain:
			S2CH A 0 A_SetTranslucent(1.0);
			S2CH A 0 A_StartSound("powerup/secretchesthit", CHAN_AUTO, 0, 1, ATTN_NONE);
			S2CH ABC 1;
			/*
			TNT1 A 0
			{
				if (BECore.CountPlayersInventory("PowerRevealer", 1))
				{
					SetStateLabel("Appear");
				}
			}
			*/
			Goto SpawnLoop;
		Wound:
			S2CH A 0
			{
				if (!SoundPlayed)
				{
					A_SetTranslucent(1.0);
					A_StartSound("powerup/secretchestappear", CHAN_AUTO, 0, 1, ATTN_NONE);
					SoundPlayed = true;
				}
			}
			S2CH DEFE 5;
			Loop;
		Death:
			S2CH A 0
			{
				if (!SoundPlayed) //This is here so that it will still play the secret tune when opened instantly with Quick Kick.
				{
					A_StartSound("powerup/secretchestappear", CHAN_AUTO, 0, 1, ATTN_NONE);
					SoundPlayed = true; //Just in case.
				}
			}
			TNT1 A 0
			{
				let plr = BEPlayer(Target);
				if (plr && plr.CheckInventory("LegsDemolishersLegPlates", 0))
				{
					A_GiveToTarget("PowerDemolisherDamage", 1);
				}
				BEGlobalStats.Get().TreasureLevel += 8;
			}
			
			S2CH A 0 A_SpawnItemEx("BEGenExplosionBig", 0, 0, 12, 0, 0, 0);
			//S2CH AA 0 A_SpawnItemEx("WeaponUpgradeKitPool", 0, 0, 6, random(4, 8), random(4, 8), random(6, 12), random(0, 359), SXF_NOCHECKPOSITION);
			S2CH AA 0 A_SpawnItemEx("BasicGearPoolChest", 0, 0, 6, random(4, 8), random(4, 8), random(6, 12), random(0, 359), SXF_NOCHECKPOSITION);
			S2CH AAAAAAAAAAAAAAAAAAAA 0 A_SpawnItemEx("ScorePoolLarge", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION);
			S2CH AAAAA 0 A_SpawnItemEx("CoinBagSmall", 0, 0, 6, random(-2, 2), random(-2, 2), random(4, 9), random(0, 359), SXF_NOCHECKPOSITION);
			S2CH AA 0 A_SpawnItemEx(BECollectible.PickRandomCollectible(BECollectible.CType_Standard), 0, 0, 6, random(4, 8), random(4, 8), random(6, 12), random(0, 359), SXF_NOCHECKPOSITION);
			Stop;
	}
}

class SpecialChestAppearanceFX : Actor
{
	Default
	{
		Radius 2;
		Height 2;
		RenderStyle "AddStencil";
		StencilColor "Green";
		Alpha 0.6;
		+BRIGHT
		+NOINTERACTION
		+FLATSPRITE
		Scale 0.6;
		ReactionTime 35 * 3;
	}

	States
	{
		Spawn:
			LKOP A 1;
			LKOP A 0 A_SetScale(scale.x + 0.5);
			LKOP A 0
			{
				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			LKOP A 1 A_FadeOut(0.15);
			LKOP A 0 A_SetScale(scale.x + 0.2);
			Loop;
	} 
}
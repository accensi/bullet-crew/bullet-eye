enum EquipmentType
{
	EType_Helmet,
	EType_Armor,
	EType_Gloves,
	EType_Boots,
	EType_Shoulders,
	EType_Belt,
	EType_Legs,
	EType_Accessory
}

class BEEquipmentItem : Inventory abstract
{
	//--------------------------------------------------
	// STATIC
	//--------------------------------------------------

	static int CheckShieldBoosts(Actor other, int bon1 = 6, int bon2 = 15)
	{
		return other.CheckInventory("ArmorDualLayeredMail", 0) * bon1
		+ other.CheckInventory("PowerShieldPolish", 0) * bon2;
	}

	static class<BEEquipmentItem> PickRandomItem(EquipmentType slot = -1)
	{
		// [Ace] All numbers are 1-based.
		int totalScore = BECore.GetTotalScore();
		int currScoreTier = 0;
		foreach (sc : BEEquipmentItem.ScoreMin)
		{
			if (totalScore >= sc)
			{
				currScoreTier++;
			}
		}

		int chosenTiers = 0;
		for (int i = 1; i <= BEEquipmentItem.ScoreMin.Size(); ++i)
		{
			if (i < currScoreTier - 1 || i > currScoreTier + 1)
			{
				continue;
			}

			for (int j = 4; j > 0; --j)
			{ 
				if (random(1, 100) <= (i == currScoreTier ? 100 : 33) && currScoreTier >= j)
				{
					chosenTiers |= 1 << (i - random(1, j));
					break;
				}
			}
		}

		let data = BEGlobalStats.Get();
		Array<class<BEEquipmentItem> > pickedItems;
		foreach (cls : AllActorClasses)
		{
			if (!(cls is 'BEEquipmentItem') || cls.IsAbstract())
			{
				continue;
			}

			let item = BEEquipmentItem(GetDefaultByType(cls));
			bool eliteCheck = !item.bELITEEQUIPMENT || data.CollectedElites.Find(item.GetClass()) != data.CollectedElites.Size();
			if (!item.bENEMYDROP && eliteCheck && !item.bSHIELDEQUIPMENT && !item.bSPECIALDROP && (slot < 0 || item.Type == slot) && chosenTiers & (1 << item.Tier - 1))
			{
				pickedItems.Push(item.GetClass());
			}
		}

		return pickedItems[random(0, pickedItems.Size() - 1)];
	}

	// --------------------------------------------------
	// VIRTUALS
	// --------------------------------------------------

	// [Ace] This is used when you pick up an equipment for the first time or pick up a different one.
	override void AttachToOwner(Actor other)
	{
		Super.AttachToOwner(other);

		if (bELITEEQUIPMENT)
		{
			let data = BEGlobalStats.Get();
			if (data.CollectedElites.Find(self.GetClass()) == data.CollectedElites.Size())
			{
				data.CollectedElites.Push(self.GetClass());
			}
		}

		let plr = BEPlayer(other);
		if (plr)
		{
			// [Ace] If you currently have another equipment item in that slot, drop it.
			if (plr.EquipmentSlots[Type] && plr.EquipmentSlots[Type].GetClass() != GetClass())
			{
				plr.A_DropInventory(plr.EquipmentSlots[Type].GetClass());
			}

			// [Ace] Carry on the regular cycle.
			plr.EquipmentSlots[Type] = self;

			// [Ace] If the "new" item has any old bonuses on it, give them. 
			if (RolledBonuses.Size() > 0)
			{
				for (int i = 0; i < RolledBonuses.Size(); ++i)
				{
					plr.A_GiveInventory(RolledBonuses[i]);
				}
			}

			// [Ace] If not, roll new ones, but only if the level of the item is over 1. Unless it's a blank one. Those get a starting bonus.
			else
			{
				if (bBLANK)
				{
					RollNewRandomBonus();
				}
				if (EquipmentLevel > 1)
				{
					for (int i = 0; i < EquipmentLevel - 1; ++i)
					{
						RollNewRandomBonus();
					}
				}
			}
		}
	}

	override void BeginPlay()
	{
		EquipmentLevel = 1; // [Ace] Starts at 1 because it's more intuitive.
		Super.BeginPlay();
	}

	override void DetachFromOwner()
	{
		// [Ace] Remove all bonuses upon removing the item.
		let plr = BEPlayer(Owner);
		for (int i = 0; plr && i < RolledBonuses.Size(); ++i)
		{
			plr.A_TakeInventory(RolledBonuses[i], 1);
		}

		// [Ace] Gotta clear the pointer, too, because dropping the item doesn't get rid of it.
		plr.EquipmentSlots[Type] = null;

		LastOwner = plr;

		Super.DetachFromOwner();
	}

	// [Ace] This is used to level up your current equipment item.
	override bool HandlePickup(Inventory item)
	{
		let plr = BEPlayer(Owner);
		let newItem = BEEquipmentItem(item);
		

		if (newItem && GetClass() == newItem.GetClass())
		{
			// [Ace] Bump level and grant new bonus.
			if (EquipmentLevel + newItem.EquipmentLevel <= GetMaxEquipmentLevel())
			{
				int timesToLevelUp = newItem.EquipmentLevel;
				EquipmentLevel += timesToLevelUp;

				for (int i = 0; i < timesToLevelUp; ++i)
				{
					RollNewRandomBonus();
				}
			}
		}

		return Super.HandlePickup(item);
	}

	override void Tick()
	{
		if (Owner)
		{
			Target = Owner;
		}
		
		if (CheckProximity("BEEquipmentItem", 40, 1, CPXF_SETTRACER | CPXF_CLOSEST | CPXF_ANCESTOR)) //16
		{
			angle = AngleTo(Tracer);
			A_ChangeVelocity(-0.2, 0, 0, CVF_RELATIVE);
		}

		Super.Tick();
	}

	override void Touch(Actor toucher)
	{
		// [Ace] Prevent picking up equipment if equipment lock is active and you have something different in that slot.
		let plr = BEPlayer(toucher);
		if (plr)
		{
			if (plr.CheckInventory("EquipmentLockToggle", 0))
			{
				plr.PreviewEquipment = self;
				Target = plr;
				if (plr.EquipmentSlots[Type] && plr.EquipmentSlots[Type].GetClass() != GetClass())
				{
					return;
				}
			}
			
			let existing = plr.FindInventory(GetClass());
			let equip = BEEquipmentItem(plr.FindInventory(GetClass()));
			if (existing && equip.EquipmentLevel == equip.GetMaxEquipmentLevel())
			{
				return;
			}
		}

		Super.Touch(toucher);
	}

	virtual void OnPlayerRespawn() { }
	virtual void OnEnemyKilled(BEEnemy enemy, BEPlayer plr, BEWeapon wpn, Name mod) { }
	virtual ui void DrawHUDStuff(BulletEyeHUD hud, BEPlayer plr, int flags) { }
	
	// --------------------------------------------------
	// ACTIONS
	// --------------------------------------------------

	void SellItem(Actor seller)
	{
		if (!seller || !seller.CheckInventory("EquipmentLockToggle", 0))
		{
			return;
		}

		seller.A_StartSound("environment/vendingmachinesell", 20, CHANF_OVERLAP, 1.0, 2);
		seller.A_GiveInventory('CraftingMaterials', SellPrice * (EquipmentLevel * 4) + (BEEquipmentBonus.GetTotalBonus(seller, 'BonusBartering')));
		SellCoinSpawner.SpawnCoinEffect(self);
		Destroy();
		
		if (bELITEEQUIPMENT)
		{
			//seller.A_StartSound("environment/vendingmachinesell", 20, CHANF_OVERLAP, 1.0, 2);
			A_SpawnItemEx('SpecialItemAppearFX', 0, 20);
			A_SpawnItemEx('SpecialItemAppearFX', 0, -20);
			A_SpawnItemEx('ItemEliteSignet', 0, 20);
			A_SpawnItemEx('ItemEliteSignet', 0, -20);
			Destroy();		
		}
	}

	// [Ace] bonusIndex > -1 rerolls only the bonus at the index.
	void RerollBonuses(int bonusIndex = -1)
	{
		if (Owner)
		{
			if (bonusIndex == -1)
			{
				// [Ace] First clear all bonuses.
				for (int i = 0; i < RolledBonuses.Size(); ++i)
				{
					Owner.A_TakeInventory(RolledBonuses[i], 1);
				}

				RolledBonuses.Clear();

				// [Ace] Then re-roll.
				for (int i = 0; i < GetMaxEquipmentLevel() - 1; ++i)
				{
					RollNewRandomBonus();
				}
			}
			else
			{
				bonusIndex = clamp(bonusIndex, 0, RolledBonuses.Size() - 1);
				class<BEEquipmentBonus> oldBonus = RolledBonuses[bonusIndex];
				Owner.A_TakeInventory(oldBonus, 1);
				RolledBonuses.Delete(bonusIndex);
				RollNewRandomBonus(bonusIndex, oldBonus);
			}
		}
	}

	// [Ace] This should have returned the new bonus and not automatically push it, but oh well.
	void RollNewRandomBonus(int insertIndex = -1, class<BEEquipmentBonus> oldBonus = null)
	{
		Array<class<BEEquipmentBonus> > allBonuses;
		foreach (cls : AllActorClasses)
		{
			let bonCls = (class<BEEquipmentBonus>)(cls);
			if (bonCls && !bonCls.IsAbstract())
			{
				allBonuses.Push(bonCls);
			}
		}

		let plr = BEPlayer(Owner);
		if (plr)
		{
			Array<int> IteratedIndices; // [Ace] This prevents iterating over the same index.
			class<BEEquipmentBonus> PickedBonus = null;

			do
			{
				// [Ace] Pick a random bonus. Only skip if it's already been interated.
				int Index = random(0, allBonuses.Size() - 1);
				if (IteratedIndices.Size() > 0 && IteratedIndices.Find(Index) != IteratedIndices.Size())
				{
					continue;
				}

				IteratedIndices.Push(Index);
				PickedBonus = allBonuses[Index];

				
				/*
				bool dontHaveIt = Owner && !Owner.FindInventory(PickedBonus);
				bool stackableAndNotOnItem = RolledBonuses.Find(PickedBonus) == RolledBonuses.Size() && GetDefaultByType(PickedBonus).bSTACKABLE;

				if ((DontHaveIt || StackableAndNotOnItem) && (!oldBonus || oldBonus != PickedBonus))
				{
					break;
				}
				*/
				
				// [Ace] If you don't have the bonus or of it's stackable and this equipment item hasn't given it yet, give it.
				bool dontHaveIt = Owner && !Owner.FindInventory(PickedBonus) || GetDefaultByType(PickedBonus).bSTACKABLE;

				if ((dontHaveIt) && (!oldBonus || oldBonus != PickedBonus))
				{
					break;
				}
			} while (IteratedIndices.Size() < allBonuses.Size()); // [Ace] When this is exited normally, PickedBonus will likely be null.

			// [Ace] Give picked bonus and put it in the array.
			plr.A_GiveInventory(PickedBonus);

			if (insertIndex == -1)
			{
				RolledBonuses.Push(PickedBonus);
			}
			else
			{
				RolledBonuses.Insert(insertIndex, PickedBonus);
			}
		}
	}

	// --------------------------------------------------
	// INFORMATION
	// --------------------------------------------------

	clearscope bool CanReroll() const
	{
		return RolledBonuses.Size() > 0;
	}

	clearscope int GetMaxEquipmentLevel()
	{
		return 4 + (int(bBLANK) * 2);
		//return 4 + int(bBLANK);
	}

	clearscope int GetUpgradeCost()
	{
		int cost = 1;
		switch (EquipmentLevel)
		{
			case 1: cost = 300 * Tier / 2; break;
			case 2: cost = 800 * Tier / 2; break;
			case 3: cost = 1800 * Tier  / 2; break;
			case 4: cost = bBLANK ? 3500 : 250; break;
			case 5: cost = bBLANK ? 6000 : 250; break;
			case 6: cost = 250; break;
			
			/*
			case 1: cost = 300; break;
			case 2: cost = 800; break;
			case 3: cost = 1800; break;
			case 4: cost = bBLANK ? 3500 : 200; break;
			case 5: cost = 200; break;
			*/
		}

		let man = OldWorldManual(Target.FindInventory('OldWorldManualLevel'));
		if (man)
		{
			cost = int(cost * (1.0 - (man.Amount * man.Reduction) / 100.0));
		}
		return Target ? max(1, cost - (bSPECIALUPGRADE * 60 * EquipmentLevel)) : cost;
	}

	clearscope int GetRerollCost()
	{
		int cost = 250;
		let man = OldWorldManual(Target.FindInventory('OldWorldManualReroll'));
		if (man)
		{
			cost = int(cost * (1.0 - (man.Amount * man.Reduction) / 100.0));
		}
		return Target ? max(1, cost - (bBLANK * 30)) : cost;
	}

	// --------------------------------------------------
	// CONSTANTS/VARIABLES
	// --------------------------------------------------

	mixin DamageModifierType;

	// [Ace] These need to be the same size as Tier.

	static const int ScoreMin[] = { 0, 250000, 600000, 1500000, 3500000, 9999999999 };

	int EquipmentLevel;
	
	Array<class<BEEquipmentBonus> > RolledBonuses;

	BEPlayer LastOwner;

	meta int Type;
	property Type: Type;

	meta int Tier;
	property Tier: Tier;

	string EffectText;
	property EffectText: EffectText;

	meta string FlavorText;
	property FlavorText: FlavorText;

	meta int SellPrice;
	property SellPrice: SellPrice;

	private int EquipmentBehaviourFlags;

	flagdef EnemyDrop: EquipmentBehaviourFlags, 0; //Items that drop from specific enemies.
	flagdef EliteEquipment: EquipmentBehaviourFlags, 1;
	flagdef Blank: EquipmentBehaviourFlags, 2; //Alpha equipment items.
	flagdef ShieldEquipment: EquipmentBehaviourFlags, 3; //Arm shield equipment items. Goes over glove slot.
	flagdef SpecialDrop: EquipmentBehaviourFlags, 4; //Special drops that cannot be found from an enemy and are only in chests.
	flagdef SpecialUpgrade: EquipmentBehaviourFlags, 5; //Item upgrade cost is reduced.

	Default
	{
		Radius 15;
		Height 42;
		//PushFactor 0.18;
		+DONTGIB
		+INVENTORY.ALWAYSPICKUP
		+BRIGHT
		BEEquipmentItem.SellPrice 25;
		BEEquipmentItem.Tier 1;
		BEEquipmentItem.DamageBonus 1.0, 1.0;
	}

	States
	{
		Spawn:
			#### A 0 A_SpawnItemEx("EquipmentPowerupL"..invoker.EquipmentLevel.."FX2", flags: SXF_SETMASTER);
			#### A 35;
		SpawnLoop:
			#### A 3 A_SpawnItemEx("EquipmentPowerupL"..invoker.EquipmentLevel.."FX1", frandom(-8, 8), frandom(-8, 8), frandom(4, 10), flags: SXF_SETMASTER);
			#### A 0
			{
				if (bELITEEQUIPMENT)
				{
					A_SpawnItemEx("EquipmentPowerupElite", frandom(-12, 12), frandom(-12, 12), frandom(10, 15), flags: SXF_SETMASTER);
				}
			}
			Loop;
	}
}

mixin class DamageModifierType
{
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags)
	{
		if (!passive && damage > 0 && (BonusPrimary > 1.0 || BonusSecondary > 1.0) && (WeaponType > -1 || WeaponTechnicalInfo != 0))
		{
			let wpnCls = BEWeapon.DamageTypeToWeapon(damageType);
			if (wpnCls)
			{
				let def = GetDefaultByType(wpnCls);
				if ((WeaponType == -1 || def.Type == WeaponType) && (WeaponTechnicalInfo == 0 || def.TechnicalInfo & WeaponTechnicalInfo))
				{
					// [Ace] If execution gets this far, obviously one of these is > 1.0.
					if (BonusPrimary > 1.0)
					{
						newDamage = int(damage * (BonusPrimary + def.BonusPrimary));
					}
					else
					{
						newDamage = int(damage * (BonusSecondary + def.BonusSecondary));
					}
				}
			}
		}
	}

	meta int WeaponType;
	property WeaponType: WeaponType; // [Ace] Ignored if -1. At least WeaponTechnicalInfo should be specified in that case, otherwise nothing will happen.

	meta int WeaponTechnicalInfo;
	property WeaponTechnicalInfo : WeaponTechnicalInfo; // [Ace] Ignored if 0. Same as above but for WeaponType. Should have at least one of these specified.

	meta double BonusPrimary, BonusSecondary;
	property DamageBonus: BonusPrimary, BonusSecondary;
}
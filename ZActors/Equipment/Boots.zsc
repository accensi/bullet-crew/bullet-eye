//--------------------------------------------
// ALPHA SABATONS
//--------------------------------------------

class BootsAlphaSabatons : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Alpha Sabatons";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT00";
		Tag "\c[White]Alpha Sabatons\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Allows for 6 equipment bonuses. Reduced reroll cost.";
		BEEquipmentItem.FlavorText "A new type of armor in the alpha development stage that houses 6 slots for enhancements rather than the standard 3. As it is still being developed, the armor itself does not have any special properties, which should hopefully be remedied sooner rather than later. Once completed, a new era of combat should begin, as the technology could possibly be worked into weaponry.";
		+BEEQUIPMENTITEM.BLANK
		BEEquipmentItem.SellPrice 6;
		BEEquipmentItem.Tier 1;
	}
	States
	{
		Spawn:
			BT00 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// HASTY TREADS
//--------------------------------------------

class BootsHastyTreads : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Hasty Treads";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT01";
		Tag "\c[Green]Hasty Treads\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Reduces inventory item cooldowns by 20%.";
		BEEquipmentItem.FlavorText "One of the many types of inexpensive footwear tailored for mercenaries and hunters, but still no less useful. These boots reduce fatigue on the wearer, allowing them to conserve stamina for other functions, such as tossing explosives or quickly grabbing a healing solution from their belt. Rumor has it that the manufacturer is working on an upgraded model with improved specs.";
		BEEquipmentItem.SellPrice 16;
		BEEquipmentItem.Tier 2;
	}

	States
	{
		Spawn:
			BT01 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// SIEGEBREAKER COMBAT BOOTS
//--------------------------------------------

class SiegebreakerDebuff : PowerProtection
{
	override void AttachToOwner(Actor other)
	{
		other.A_SpawnItemEx("SiegebreakerIndicator", 0, 0, 6, flags: SXF_SETMASTER);
		Super.AttachToOwner(other);
	}

	Default
	{
		Powerup.Duration BE_INFINITEDURATION;
		DamageFactor "GrindHalberd", 1.4;
		DamageFactor "QuickStrike", 1.4;
		DamageFactor "GalaxySlicer", 1.4;
		DamageFactor "BloodletterMagnumMelee", 1.4;
		DamageFactor "GlacialLash", 1.4;
		DamageFactor "FrenzyRavagers", 1.4;
		DamageFactor "VampiricGauntlets", 1.4;
		DamageFactor "MetalMonstrosity", 1.4;
	}
}

class SiegebreakerIndicator : Actor
{
	Default
	{
		+NOINTERACTION
		+BRIGHT
		Renderstyle "Add";
		Scale 0.8;
	}
	
	States
	{
		Spawn:
			RVTI A 1
			{
				if (!master || Master.Health <= 0)
				{
					Destroy();
					return;
				}

				A_Warp(AAPTR_MASTER, 0, 0, 0, 0, WARPF_INTERPOLATE | WARPF_NOCHECKPOSITION, null, 1.5, 0, 0);
			}
			Loop;
	}
}

class BootsSiegebreakerCB : BEEquipmentItem
{
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags)
	{
		if (!passive && damageType == 'QuickMelee' && source is "BEEnemy" && source.Health > 0) 
		{
			source.A_GiveInventory("SiegebreakerDebuff");
		}

		Super.ModifyDamage(damage, damageType, newDamage, passive, inflictor, source, flags);
	}

	Default
	{
		Health 350;
		Inventory.PickupMessage "Siegebreaker Combat Boots";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT02";
		Tag "\c[White]Siegebreaker Combat Boots\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Your Quick Kick weakens an enemy's defenses, causing them to take increased damage from \c[GhostWhite]MELEE\c- weapons. +10% \c[GhostWhite]MELEE\c- weapon damage.";
		BEEquipmentItem.FlavorText "These boots are heavily reinforced on parts that would normally be most vulnerable in footwear. Due to the Solidinite metal used in such reinforcement, these boots are strong enough to break down doors with ease or even crack open some types of combat armor like an egg.";
		BEEquipmentItem.SellPrice 28;
		BEEquipmentItem.Tier 1;
		BEEquipmentItem.WeaponType WType_Melee;
		BEEquipmentItem.DamageBonus 1.10, 1.10;
	}

	States
	{
		Spawn:
			BT02 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// POWER STOMPERS
//--------------------------------------------

class BootsPowerStompers : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Power Stompers";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT03";
		Tag "\c[White]Power Stompers\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Increases knock back and recovery time of Quick Kick.";
		BEEquipmentItem.FlavorText "Oversized appearance on the outside, nice and secure fit on the inside. You'll surely have no trouble traversing harsh terrain such as snow or unstable rocky ground with these on. But you don't have to worry about that right now. They are also perfect for kicking - an aspect you should be focusing on.";
		BEEquipmentItem.SellPrice 22;
		BEEquipmentItem.Tier 1;
	}
	States
	{
		Spawn:
			BT03 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// HOVER JETS
//--------------------------------------------

class BootsHoverPower : PowerFlight
{
	override void DoEffect()
	{
		if (Owner && Owner.vel.z != 0)
		{
			Owner.vel.z = 0;
		}

		Super.DoEffect();
	}

	override void EndEffect()
	{
		if (Owner == NULL || Owner.Player == NULL)
		{
			return;
		}

		if (!(Owner.bFlyCheat))
		{
			Owner.bFly = false;
			Owner.bNoGravity = false;
		}
	}


	Default
	{
		Powerup.Duration -2;
	}
}

class BootsHoverJets : BEEquipmentItem
{
	override void DoEffect()
	{
		let plr = BEPlayer(Owner);
		bool PressedJump = plr.Player.cmd.buttons & BT_JUMP && !(plr.Player.oldbuttons & BT_JUMP);
		if (PressedJump && plr.JumpCount == 2 && !plr.Player.onground)
		{
			plr.A_GiveInventory("BootsHoverPower");
		}

		Super.DoEffect();
	}

	Default
	{
		Health 350;
		Inventory.PickupMessage "Hover Jets";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT04";
		Tag "\c[White]Hover Jets\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Jumping again after a double jump allows you to hover in place for a short time.";
		BEEquipmentItem.FlavorText "These boots use a new type of energy cell that powers the jets placed at the bottom of the boots. The cell only contains enough energy for the wearer to hover for a few seconds, but is able to recharge on its own with no user input and is compact so as not to add unnecessary bulkiness.";
		BEEquipmentItem.SellPrice 12;
		BEEquipmentItem.Tier 1;
	}

	States
	{
		Spawn:
			BT04 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// VENOM SPIKEFEET
//--------------------------------------------

class BootsVenomSpikefeet : BEEquipmentItem
{
	mixin VenomSpikefeetEffect;

	Default
	{
		Health 350;
		Inventory.PickupMessage "Venom Spikefeet";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT05";
		Tag "\c[Yellow]Venom Spikefeet\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Quick Kick gains the poison element.";
		BEEquipmentItem.FlavorText "Boots adorned with spikes, which in turn, are lined with a potent venom. A kick with these boots would allow the wearer to easily follow up with all sorts of attacks, considering the target will be in a weakened state. The boots were based on another model of footwear known as the Jasper Clawfeet, which see a limited window of release annually during the summer solstice.";
		BEEquipmentItem.SellPrice 22;
		BEEquipmentItem.Tier 3;
	}
	States
	{
		Spawn:
			BT05 A 0;
			Goto Super::Spawn;
	}
}

mixin class VenomSpikefeetEffect
{
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags)
	{
		if (!passive && damageType == 'QuickMelee' && source is "BEEnemy" && source.Health > 0 && !source.bFRIENDLY) 
		{
			source.A_GiveInventory("PoisonDebuff");
		}

		Super.ModifyDamage(damage, damageType, newDamage, passive, inflictor, source, flags);
	}
}

//--------------------------------------------
// MORALE MARCHERS
//--------------------------------------------

class BootsMoraleMarchers : BEEquipmentItem
{
	override void DrawHUDStuff(BulletEyeHUD hud, BEPlayer plr, int flags)
	{
		if (CVar.GetCVar("bulleteye_relevantequipmenttrackers", plr.Player).GetInt() && Charge >= 40)
		{
			hud.DrawImage("MORMARCN", (47, -94), flags);
			hud.DrawString(hud.mCounterFont, hud.FormatNumber(Charge, 2, 2, hud.FNF_FILLZEROS), (70, -108), flags, Font.CR_GOLD);
		}
		else if (!CVar.GetCVar("bulleteye_relevantequipmenttrackers", plr.Player).GetInt() || plr.InPanel)
		{
			hud.DrawImage("MORMARCN", (47, -94), flags);
			hud.DrawString(hud.mCounterFont, hud.FormatNumber(Charge, 2, 2, hud.FNF_FILLZEROS), (70, -108), flags, Font.CR_GOLD);
		}
	}

	override void OnEnemyKilled(BEEnemy enemy, BEPlayer plr, BEWeapon wpn, Name mod)
	{
		Charge++;
	}

	override void DoEffect()
	{
		if (Charge >= 50)
		{
			Owner.A_SpawnItemEx("MediGemSmallDropped", radius + 80, -30);
			Owner.A_SpawnItemEx("MediGemSmallDropped", radius + 80, 30);
			Owner.A_SpawnItemEx("ShieldCapsuleL", radius + 150, -30);
			Owner.A_SpawnItemEx("ShieldCapsuleL", radius + 150, 30);
			Charge -= 50;
		}

		Super.DoEffect();
	}

	override void DetachFromOwner()
	{
		Charge = 0;
		Super.DetachFromOwner();
	}

	private int Charge;

	Default
	{
		Health 350;
		Inventory.PickupMessage "Morale Marchers";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT06";
		Tag "\c[Yellow]Morale Marchers\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Creates health and shield capsules nearby for every 50 enemies slain.";
		BEEquipmentItem.FlavorText "Interestingly designed boots that use tech similar to the Victory Armor. However, rather than solely granting the wearer all of the benefits, the rewards are dropped in nearby. This allows the user to share much needed aid to allies, thereby keeping morale up in hard fought battles and more easily securing a victory.";
		BEEquipmentItem.SellPrice 22;
		BEEquipmentItem.Tier 3;
	}

	States
	{
		Spawn:
			BT06 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// ICEWRATH WAR BOOTS
//--------------------------------------------

class BootsIcewrathWarBoots : BEEquipmentItem
{
	override void DoEffect()
	{
		Super.DoEffect();

		if (Owner.Pos.z >= Owner.floorz + 140)
		{
			HeightCheckReached = 1;
		}
		
		if (HeightCheckReached >= 1 && Owner.Pos.z == Owner.floorz)
		{
			Owner.A_SpawnItemEx("IcewrathGlacialSpawner", 0, 0, 0, 12, 0, 0, 0, SXF_NOCHECKPOSITION);
			Owner.A_SpawnItemEx("IcewrathGlacialSpawner", 0, 0, 0, 12, 0, 0, 90, SXF_NOCHECKPOSITION);
			Owner.A_SpawnItemEx("IcewrathGlacialSpawner", 0, 0, 0, 12, 0, 0, 180, SXF_NOCHECKPOSITION);
			Owner.A_SpawnItemEx("IcewrathGlacialSpawner", 0, 0, 0, 12, 0, 0, 270, SXF_NOCHECKPOSITION);
			HeightCheckReached = 0;
		}
	}
	
	int HeightCheckReached;

	Default
	{
		Health 350;
		Inventory.PickupMessage "Icewrath War Boots";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT07";
		Tag "\c[Yellow]Icewrath War Boots\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Creates glacial spikes in a cross pattern after landing from a double jump or a great height.";
		BEEquipmentItem.FlavorText "Despite the ice and frost encrusting this pair of boots, the inside of them is very warm. The sub-zero energy cells within certain parts of the boots are of course the cause of said layer of ice. These cells have enough power to actually freeze hostiles should they be exposed to the biting cold temperatures long enough.";
		BEEquipmentItem.SellPrice 35;
		BEEquipmentItem.Tier 3;
	}
	States
	{
		Spawn:
			BT07 A 0;
			Goto Super::Spawn;
	}
}

class IcewrathGlacialSpawner : BEPlayerProjectile
{
	Default
	{
		Speed 25;
		Radius 3;
		Height 3;
		Projectile;
		+THRUACTORS
		+DONTBLAST
		+FLOORHUGGER
		ReactionTime 15;
	}

	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_SpawnItemEx("IcewrathGlacialSpike", flags: SXF_TRANSFERPOINTERS);
			TNT1 A 4;
			TNT1 A 0 A_Countdown;
			Loop;
		Death:
			TNT1 A 1;
			Stop;
	}
}

class IcewrathGlacialSpike : BEPlayerProjectile
{
	override void PostBeginPlay()
	{
		Super.PostBeginPlay();
		SetOrigin((pos.Xy, floorz), false);
	}

	Default
	{
		Scale 0.8;
		RenderStyle "Add";
		DamageType "Riskbreaker";
		DeathType "BEIce";
		Alpha 0.8;
		Projectile;
		+NOINTERACTION
		+NODAMAGETHRUST
		+THRUACTORS
		+BRIGHT
		+ROLLSPRITE
	}
   
	States
	{
		Spawn:
			IWSP A 0 NoDelay A_SetRoll(random(-10, 10));
			IWSP A 0
			{
				if (random(1, 100) <= 50)
				{
					bSPRITEFLIP = true;
				}
			}
			IWSP A 2;
			IWSP A 0 A_StartSound("weapons/glaciallashstrikeendpool", CHAN_BODY, volume: 0.3);
			IWSP B 2;
			IWSP C 1 A_Explode(15 + (BEGlobalStats.Get().ItemPower / 2), 100, 0, 1, 35);
			IWSP D 1;
			IWSP A 0 A_Stop;
			1GLF BCD 2;
			Stop;
	} 
}

//--------------------------------------------
// BOOTS OF DIRTY TRICKS
//--------------------------------------------

class BootsBootsOfDirtyTricks : BEEquipmentItem
{
	override void DoEffect()
	{
		if (Owner && --DirtyTricksCooldown <= 0)
		{
				Owner.A_RadiusGive("MovementSpeedSlowEffect", 300, RGF_MONSTERS | RGF_EXFILTER, 1, "BEPlayer");
				Owner.A_SpawnItemEx("DirtyTricksAuraFX", 0, 0, 15, 0, 0, 0);
				DirtyTricksCooldown = 175;
		}
		Super.DoEffect();
	}

	private int DirtyTricksCooldown;
	
	Default
	{
		Health 450;
		Inventory.PickupMessage "Boots of Dirty Tricks";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT08";
		Tag "\cl*\cd \c[TalayPeaks]Boots of Dirty Tricks\c-";
		BEEquipmentItem.Type EType_Boots;
		+BEEQUIPMENTITEM.ELITEEQUIPMENT
		BEEquipmentItem.EffectText "Every 5 seconds, nearby enemies have their movement speed slowed.";
		BEEquipmentItem.FlavorText "This fearsome looking footwear has all the necessary parts to strike fear into your enemies. Spikes, a terrifying face, and impenetrable metal. Any who are close enough look upon these features will be wary of approaching you further onward. This gives wearers an edge in battle, as demoralized enemies rarely put up a fight, which in turn causes them to be trampled over with ease.";
		BEEquipmentItem.SellPrice 400;
		BEEquipmentItem.Tier 6;
	}
	States
	{
		Spawn:
			BT08 A 0;
			Goto Super::Spawn;
	}
}

class DirtyTricksAuraFX : Actor
{
	Default
	{
		RenderStyle "Add";
		Alpha 0.1;
		+BRIGHT
		+FLATSPRITE
		+NOINTERACTION
		Scale 0.6;
		ReactionTime 8;
	}

	States
	{
		Spawn:
			RBSW A 1;
			RBSW B 1
			{
				A_SetScale(scale.x + 0.8);
				if (--ReactionTime <= 0)
				{
					SetStateLabel("Death");
				}
			}
			Loop;
		Death:
			RBSW A 1;
			RBSW B 1 A_FadeOut(0.2);
			Loop;
	} 
}

//--------------------------------------------
// BULLET STRIDERS
//--------------------------------------------

class BootsBulletStriders : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Bullet Striders";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT09";
		Tag "\c[Unterwasser]Bullet Striders\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Picking up any ammo has a chance to restore partial ammo directly into your current weapon's magazine.";
		BEEquipmentItem.FlavorText "Adorned with magnetized metal soles, these boots will allow ammo to stick directly to the sides, allowing for wearers to quickly load their firearms on the go. Specialized sensors deactivate the magnetic field while walking upon metal surfaces, but will otherwise reactivate for a split second to collect ammunition. Special modules can be added to strengthen the magnetic field, allowing wearers to even walk on metal walls!";
		BEEquipmentItem.SellPrice 35;
		BEEquipmentItem.Tier 4;
	}
	States
	{
		Spawn:
			BT09 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// SHATTERING STOMPERS
//--------------------------------------------

class BootsShatteringStompers : BEEquipmentItem
{
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags)
	{
		let plr = BEPlayer(Owner);
		if (!passive && damageType == 'QuickMelee' && source is 'BEEnemy' && source.Health > 0 && !Owner.FindInventory('ShatteringStompersKickedCheck')) 
		{
			Owner.A_GiveInventory("ShatteringStompersKickedCheck", 1);
			source.A_SpawnItemEx("LockedMoneyChestRed", flags: SXF_NOCHECKPOSITION);
			newDamage = max(1, damage * 6);
		}

		Super.ModifyDamage(damage, damageType, newDamage, passive, inflictor, source, flags);
	}

	Default
	{
		Health 350;
		Inventory.PickupMessage "Shattering Stompers";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT10";
		Tag "\c[Unterwasser]Shattering Stompers\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "The first Quick Kick used on an enemy in a level deals 500% damage and also causes them to drop a red chest.";
		BEEquipmentItem.FlavorText "Vibrant footwear embedded with high energy-consuming plasma in the soles, making it easy to combat tough opponents and cause them to drop their valuables. The plating on these boots are reinforced with a chiseled mineral as hard as steel. One may think such boots would be impractical to use due to their assumed weight, but the comfort, actual weight, and maneuverability rivals that of many top of the line models.";
		BEEquipmentItem.SellPrice 30;
		BEEquipmentItem.Tier 4;
	}
	States
	{
		Spawn:
			BT10 A 0;
			Goto Super::Spawn;
	}
}

class ShatteringStompersKickedCheck : Inventory
{
	Default
	{
		Inventory.MaxAmount 1;
		Inventory.InterHubAmount 0;
		Inventory.Icon "TNT1A0";
	}
}

//--------------------------------------------
// DRAIN KICK
//--------------------------------------------

class BootsDrainKick : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Drain Kick";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT11";
		Tag "\c[Red]Drain Kick\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Quick Kick drains 6 health from the enemy if you are below 20% health. If an Energy Screen is not available, adds 2 shields instead. Otherwise has a chance to drain 1-3 Bullet-Eye Energy.";
		BEEquipmentItem.FlavorText "There are actually devices that leech energy from objects such as generators, electronic devices, and anything else that requires power. The frontal plating that covers the bottom of the soles on these boots implements such tech. However, it was also developed in collaboration with the Einhander corporation, which in turn grants it the power to steal the very life force from your foes as well. Like anything else that comes from Einhander, the technology to make it function is a closely guarded secret.";
		BEEquipmentItem.SellPrice 40;
		BEEquipmentItem.Tier 5;
	}
	States
	{
		Spawn:
			BT11 A 0;
			Goto Super::Spawn;
	}
}

//--------------------------------------------
// FORTIFYING TREADS
//--------------------------------------------

class BootsFortifyingTreads : BEEquipmentItem
{
	Default
	{
		Health 350;
		Inventory.PickupMessage "Fortifying Treads";
		Inventory.PickupSound "powerup/helmetget";
		Inventory.Icon "BOOT12";
		Tag "\c[Green]Fortifying Treads\c-";
		BEEquipmentItem.Type EType_Boots;
		BEEquipmentItem.EffectText "Increases the window that Flash Guard may deflect projectiles.";
		BEEquipmentItem.FlavorText "These boots were developed by 'Strike', a leading corporation in the production of cutting-edge equipment and advanced weaponry. These boots are a staple of Strike's ground forces and are equipped with tech that enhances the capabilities of barrier systems. Specifically, the effect of these boots allows for a greater window in which projectiles can be deflected, providing a significant advantage to troops and giving them a crucial leg up on the battlefield. Older barriers will not be left out either, as rather than strengthening the deflection field, the system will gain the ability to deflect shots in the first place.";
		BEEquipmentItem.SellPrice 25;
		BEEquipmentItem.Tier 2;
	}
	States
	{
		Spawn:
			BT12 A 0;
			Goto Super::Spawn;
	}
}